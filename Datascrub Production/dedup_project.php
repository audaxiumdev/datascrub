<?php
session_start();
require_once("config/dbconnect.php");
require_once("classes/class.datascrub.php");
use google\appengine\api\taskqueue\PushTask;


require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();

$projectid=442;


$dropsql="DROP TABLE IF EXISTS `ax_job_".$projectid."_detail_results`";
//echo "<br>".$dropsql;
$conn->query($dropsql);
$sql="CREATE TABLE ax_job_".$projectid."_detail_results (
  			id int(11) unsigned NOT NULL AUTO_INCREMENT,
  			primeid int(11) DEFAULT NULL,
  			secondid int(11) DEFAULT NULL,
			col_7 int(11) DEFAULT NULL,
			col_8 int(11) DEFAULT NULL,
			col_10 int(11) DEFAULT NULL,
			col_12 int(11) DEFAULT NULL,
  			result decimal(10,2) DEFAULT NULL,
  			PRIMARY KEY (id),
			KEY result (result),
  			KEY primeid (primeid),
  			KEY secondid (secondid)
			)  CHARSET=latin1;";
//echo "<br>".$sql;
if($conn->query($sql))
{
	//echo "Results table created";
}
else
{
	echo $conn->error;
	error_log($conn->error);
	exit;
}



//GET PROCESSING VARIABLES FROM PROCESS TABLE
$columns=array();
//Check Includes and datatypes
$sql="SELECT * from ax_job_".$projectid."_process";
//echo $sql;

$result = $conn->query($sql);
//echo "hhh";
while($row = $result->fetch_assoc())
{
	//echo "===".$row['colid']."===".$row['type'];
	$columns[]=$row;
}
//print_r($columns);

$colist="";
$co2list="";

$x=0;
foreach($columns as $col){

	$colist.="`col_".$col['colid']."`,";
	if($col['col2id']){
		$co2list.="`col_".$col['col2id']."`,";
	}

}


$secondfile=false;
$sql="select * from ax_project_files where project_id=".$projectid;
echo $sql;
$projectfileexist = $conn->query($sql);

if ($projectfileexist->num_rows > 0) {
	$sql_file2_origID ="Select file_2 from ax_project_files where project_id=".$projectid;
	$result = $conn->query($sql_file2_origID);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$file2_origID=$row['file_2'];
			if($file2_origID != '' && $file2_origID != null){
				$secondfile=true;
			}
		}
	}
}
$tabletype="primary";
$primary_table = 'ax_job_'.$projectid.'_primary';
//echo "---".$primary_table;
//echo "SELECT count(*) as cnt from ax_job_".$projectid."_primary";
$TotalPrimaryQuery = $conn->query("SELECT count(*) as cnt from ax_job_".$projectid."_primary");
while($TotalPrimaryResult =$TotalPrimaryQuery->fetch_assoc())
{
	$TotalPrimaryRecords=$TotalPrimaryResult['cnt'];
}


echo "TotalPrimaryRecords===".$TotalPrimaryRecords;

$memcache = new Memcache;
$key="dedup_counter_".$projectid;
$setprimcache= $memcache->set($key, $TotalPrimaryRecords);
/*if($TotalPrimaryRecords>100000)
{
	$queue_count=20;
}
elseif($TotalPrimaryRecords>20000 && $TotalPrimaryRecords<=100000)
{
	$queue_count=10;
}
else
{
	$queue_count=5;
}*/
$queue_count=20;
$queuearray=[];
for($q=0;$q<$queue_count;$q++)
{
	$t=$q+1;
	$queuearray[$q]="dedup-queue".$t;
}


$stepprimary=$queue_count-1;

$div=intval($TotalPrimaryRecords/$stepprimary);

echo "queuecount=".$queue_count."==stepprimary==".$stepprimary."==div==".$div;

$start=0;
$end=0;
$k=1;
$q=0;
while($start<$TotalPrimaryRecords)
{

	$end=$start+$div;
	if($end>$TotalPrimaryRecords)
	{
		$end=$TotalPrimaryRecords;
	}
	$queue_name=$queuearray[$q];
	echo "<br>queuename==".$queue_name."===".$start."==".$end."==".$div;
	error_log("queuename==".$queue_name."===".$start."==".$end."==".$div);
	//$jobid = Queue::pushon($queue_name,new queueAnalyze($projectid,$secondfile,$colist,$co2list,$start,$div));
	$task = new PushTask(
			'/queuededup.php',
			['projectid' => $projectid, 'tabletype' => $tabletype,'secondfile' => $secondfile,'colist' => $colist,'co2list' => $co2list,'start' => $start,'div' => $div]);
	$task_name = $task->add($queue_name);
	
	if($start==$TotalPrimaryRecords)
	{
		break;
	}
	$start=$end;
	$k++;
	$q++;
}

