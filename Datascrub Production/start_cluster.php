<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}


$ml_id="";
$status="";
$sql_chk_ml_project="Select * from ax_ml where project_id=".$current_projectid;
//echo $sql_chk_ml_project;
if($res_chk_ml_project=$conn->query($sql_chk_ml_project))
{
	if($res_chk_ml_project->num_rows>0)
	{
		
		while($row_chk_ml_project=$res_chk_ml_project->fetch_assoc())
		{
			$ml_id=$row_chk_ml_project['id'];
			//echo $ml_id;
			$status=$row_chk_ml_project['status'];
		}
	}
}


$conn->query("Update ax_projects set status=7 where id=".$current_projectid);
$service_url = "http://ml.datascrub-152522.appspot.com/portal/ml/".$ml_id."/cluster";
$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
//echo "curl---response=".$curl_response;
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);
	require_once("templates/header.html");
	require_once("templates/nav.html");
	echo "<h2 style='margin-left:300px;margin-top:200px;'>There was some problem  with Active Learning .<br><br>So, you are getting redirected to Field Select page.</h2>";

	?>
							<meta http-equiv="refresh" content="5;url=/mapfields/<?=$current_projectid;?>"> 
	<?php	
						die('error occured during curl exec. Additional info: ' . var_export($info));
	require_once("templates/footer.html");
}
else
{
	curl_close($curl);
header("Location:/clustering/".$current_projectid);
}	
	
