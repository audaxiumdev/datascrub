<?php
function checkSession()
{
	if(isset($_SESSION['userid']) && isset($_SESSION['username']))
	{
		
	}
	else
	{
		echo "<h1 style='margin:200px;'>Your Session has been expired and You are getting redirected to login page.</h1>";
		
		?>
			<meta http-equiv="refresh" content="5;url=/login"> 
		<?php
		exit;
	}
	
}

function checkFilesUploaded($current_projectid,$conn)
{
	$sql="select * from ax_project_files where project_id=".$current_projectid;
	//echo $sql;
	$res = $conn->query($sql);
	if($res->num_rows<=0)
	{
		echo "<h2 style='margin-left:300px;margin-top:200px;'>No file has been uploaded yet.<br><br>So, you are getting redirected to Upload Files page.</h2>";
		
		?>
					<meta http-equiv="refresh" content="5;url=/upload11/<?=$current_projectid;?>"> 
				<?php
				exit;
	}
}
function checkProjectCompleted($current_projectid,$conn)
{
	$sql="select status from ax_projects where id=".$current_projectid;
	$res = $conn->query($sql);
	
	while($row=$res->fetch_assoc())
	{
		$status=$row['status'];
		 
		
		if($status!=8)
		{
			
			if($status==1)
			{
				$message="No file has been uploaded yet.<br><br>So, you are getting redirected to Upload Files page.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/upload11/<?=$current_projectid;?>"> 
				<?php
			}
			else if($status==2)
			{
				$message="The project is ready for Initial Processing/Analysis.<br><br>So, you are getting redirected to Initialize that.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/mapfields/<?=$current_projectid;?>"> 
				<?php
			}
			else if($status==4)
			{
				$message="The project is ready to start Learning.<br><br>So, you are getting redirected to Initialize Learning.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/export/<?=$current_projectid;?>"> 
				<?php
			}
			else if($status==5)
			{
				$message="The project is still Learning.<br><br>So, you are getting redirected to check the progress.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/projects"> 
				<?php
			}
			else if($status==6)
			{
				$message="The project is ready to find Matches.<br><br>So, you are getting redirected to Initialize the process.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/projects"> 
				<?php
			}
			else if($status==7)
			{
				$message="The project is finding Matches.<br><br>So, you are getting redirected to check the process.";
				echo "<h2 style='margin-left:300px;margin-top:200px;'>".$message.$url."</h2>";
				?>
					<meta http-equiv="refresh" content="5;url=/projects"> 
				<?php
			}
			
			
			
			
			
			
			
			
			
			
			
								
							
							exit;
		}
	}
}

function curPageURL() {
 $pageURL = 'http';
 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


function get_filename()
{
	$url=curPageURL();
	$path= parse_url($url, PHP_URL_PATH);
	$pathComponents = explode("/", trim($path, "/")); // trim to prevent
                                                  // empty array elements
	$filename= $pathComponents[0]."php"; 
	return $filename;
}

function get_id()
{
	$url=curPageURL();
	$path= parse_url($url, PHP_URL_PATH);
	$pathComponents = explode("/", trim($path, "/")); // trim to prevent
                                                  // empty array elements
	$id= $pathComponents[1]; 
	if (!preg_match('/^[1-9][0-9]{0,15}$/', $id)) {   //This Pregmatch would allow the string to be between 1 and 16 digits long
	return "error";												  // (ie the first digit plus 0-15 further digits). 
	}												  //Feel free to adjust the numbers in the curly braces to suit your own needs. 
	else												  //If you want a fixed length string, then you only need to specify one number 
	{												  //in the braces.
	return $id;
	}
}

function get_projectid()
{
	$url=curPageURL();
	$path= parse_url($url, PHP_URL_PATH);
	$pathComponents = explode("/", trim($path, "/")); // trim to prevent
	// empty array elements
	$id= $pathComponents[2];
	if (!preg_match('/^[1-9][0-9]{0,15}$/', $id)) {   //This Pregmatch would allow the string to be between 1 and 16 digits long
		return "error";												  // (ie the first digit plus 0-15 further digits).
	}												  //Feel free to adjust the numbers in the curly braces to suit your own needs.
	else												  //If you want a fixed length string, then you only need to specify one number
	{												  //in the braces.
		return $id;
	}
}

function redirectTohttps() {
    if ($_SERVER['HTTPS']!="on") {
        $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        header("Location:$redirect"); 
    } 
}

function createToken($data)
{
	/* Create a part of token using secretKey and other stuff */
	$tokenGeneric = SECRET_KEY.$_SERVER["SERVER_NAME"]; // It can be 'stronger' of course

	/* Encoding token */
	$token = hash('sha256', $tokenGeneric.$data);

	return array('token' => $token, 'userData' => $data);
}

function authUser($login, $password,$conn)
{
	$ret="";
	if($login!="" && $password!="")
	{
		$tbl='ax_users';
		$sql="Select id,password,email from $tbl where email='".$login."'";
		
		$result=$conn->query($sql);
		
		if ($result->num_rows > 0)
		{
			$row=mysqli_fetch_array($result);
		
			if (password_verify($password, $row['password'])) {
				//$login_errors[]= 'Password is valid!';
				$_SESSION['username']=$login;
				$_SESSION['userid']=$row['id'];
				
				$ret= 1;
			} else {
				$ret= 'Your password is incorrect.Please try again.';
				
			}
				
		}
		else
		{
			$ret= "Your Email Address is incorrect. Please try again.";
			
				
		}
			
	}
	else
	{
		$ret="Login and password are required to authenticate";
	}
	return $ret;
}

function auth($login, $password, $userID, $userRole)
{
	// we check user. For instance, it's ok, and we get his ID and his role.
	//$userID = 1;
	//$userRole = "admin";

	// Concatenating data with TIME
	$data = time()."_".$userID."-".$userRole;
	$token = createToken($data);
	return  json_encode($token);
}
function generateToken($login, $userID )
{
	// we check user. For instance, it's ok, and we get his ID and his role.
	//$userID = 1;
	//$userRole = "admin";

	// Concatenating data with TIME
	$data = time()."_".$userID."-".$login;
	$token = createToken($data);
	return  json_encode($token);
}

function checkToken($receivedToken, $receivedData)
{
	
	
	/* Recreate the generic part of token using secretKey and other stuff */
	$tokenGeneric = SECRET_KEY.$_SERVER["SERVER_NAME"];

	// We create a token which should match
	$token = hash('sha256', $tokenGeneric.$receivedData);

$token = hash('sha256', $tokenGeneric.$receivedData);

	// We check if token is ok !
	if ($receivedToken != $token)
	{
		echo 'wrong Token !';
		return false;
	}
	

	list($tokenDate, $userData) = explode("_", $receivedData);
	// here we compare tokenDate with current time using VALIDITY_TIME to check if the token is expired
	// if token expired we return false
	
	//echo "datae====".date("Y-m-d",$tokenDate);
	

	// otherwise it's ok and we return a new token
	return createToken(time()."#".$userData);
}

function getProjectName($projectid, $userid,$conn)
{
	$role_id=getUserRole($userid,$conn);
	//echo "-=-=-=".$role_id;
	if($role_id==1)
	{
		$query_project="select project_name from ax_projects where deleted_at IS NULL and deleted_by IS NULL and id=".$projectid;
	}
	elseif($role_id=="wrong_user")
	{
		return "wrong_user";
	}
	else {
		$query_project="select project_name from ax_projects where deleted_at IS NULL and deleted_by IS NULL and created_by=".$userid." and id=".$projectid;
		
	}
	//echo $query_project;
	$result=$conn->query($query_project);
	
	if ($result->num_rows > 0)
	{
		while($row=$result->fetch_assoc())
		{
			return $row['project_name'];
		}
	}
	else 
	{
		return "perm_deny";
	}
	

}

function getUserRole($userid,$conn)
{
	//echo $userid;
	$query_user_role="select role_id from ax_user_roles where user_id=".$userid;
	$result_user_role=$conn->query($query_user_role);
	//echo "---".$result_user_role->num_rows;
	if ($result_user_role->num_rows > 0)
	{
		//echo "====sadsds".$userid;
		while($row_user_role=$result_user_role->fetch_assoc())
		{
			$role_type=$row_user_role['role_id'];
		
		}
		return $role_type;
	}
	else
	{
	return 	"wrong_user";
	}
	
}
/*
 * $check = checkToken($_GET['token'], $_GET['data']);
if ($check !== false)
    echo json_encode(array("secureData" => "Oo")); // And we add the new token for the next request
 * /
 */
?>