<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();
$current_projectid=get_id();

//echo "----------------".$current_projectid;
if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}
$memcache = new Memcache;
//$key_prim="addprimefile_counter_".$_SESSION['project_id'];
$key_prim="addprimefile_counter_".$current_projectid;
$setprimcache= $memcache->get($key_prim);

//$key_sec="addsecfile_counter_".$_SESSION['project_id'];
$key_sec="addsecfile_counter_".$current_projectid;
$setseccache= $memcache->get($key_sec);

if ($setprimcache==0 && $setseccache==0)
{
	require_once("templates/header.html");
	require_once("templates/nav.html");
	require_once("templates/mapfields.php");
	require_once("templates/footer.html");
}
else
{
	echo '<h2>Still processing.....</h2>';
	
	?>
	<script>
window.onload = function() {
	setTimeout(function () {
		location.reload()
	}, 10000);
};
</script>
	<?php 
}

//echo "keys==".$setprimcache."==".$setseccache;

//print_r($_POST);
?>

