<?php
session_start();




//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}


$ml_id="";
$status="";
$sql_chk_ml_project="Select * from ax_ml where project_id=".$current_projectid;

if($res_chk_ml_project=$conn->query($sql_chk_ml_project))
{
	if($res_chk_ml_project->num_rows>0)
	{
		
		while($row_chk_ml_project=$res_chk_ml_project->fetch_assoc())
		{
			$ml_id=$row_chk_ml_project['id'];
			//echo $ml_id;
			$status=$row_chk_ml_project['status'];
		}
	}
}


if(isset($_POST) && isset($_POST['userSelection']) && isset($_POST['trainingId']) && $_POST['userSelection']!="" && $_POST['trainingId']!="")
{
	if($_POST['userSelection']=="Done")
	{
		
		
		header("Location:/start_cluster/".$current_projectid);
				exit;
	}
	elseif($_POST['userSelection']=="Yes")
	{
		$label="1";
	}
	elseif($_POST['userSelection']=="No")
	{
		$label="0";
	}
	
	$sql_update="update ax_ml_".$ml_id."_training set label=".$label." where id=".$_POST['trainingId'];
	//echo $sql_update;
	$row_update=$conn->query($sql_update );
	if($row_update)
	{
		
	}
	else {
		echo $conn->error;
	}
}

$total_ques_answered=0;
$total_yes=0;
$total_no=0;
$sql_cnt_ques_answered="Select count(*) as cnt from ax_ml_".$ml_id."_training where label in(0,1) and primary_id<>-1";

if($res_cnt_ques_answered=$conn->query($sql_cnt_ques_answered))
{
	if($res_cnt_ques_answered->num_rows>0)
	{

		while($row_cnt_ques_answered=$res_cnt_ques_answered->fetch_assoc())
		{
			$total_ques_answered=$row_cnt_ques_answered['cnt'];
			//echo $ml_id;
				
		}
	}
}


$sql_cnt_ques_yes="Select count(*) as cnt from ax_ml_".$ml_id."_training where label=1 and primary_id<>-1";
if($res_cnt_ques_yes=$conn->query($sql_cnt_ques_yes))
{
	if($res_cnt_ques_yes->num_rows>0)
	{

		while($row_cnt_ques_yes=$res_cnt_ques_yes->fetch_assoc())
		{
			$total_ques_yes=$row_cnt_ques_yes['cnt'];
			//echo $ml_id;

		}
	}
}

$sql_cnt_ques_no="Select count(*) as cnt from ax_ml_".$ml_id."_training where label=0 and primary_id<>-1";
if($res_cnt_ques_no=$conn->query($sql_cnt_ques_no))
{
	if($res_cnt_ques_no->num_rows>0)
	{

		while($row_cnt_ques_no=$res_cnt_ques_no->fetch_assoc())
		{
			$total_ques_no=$row_cnt_ques_no['cnt'];
			//echo $ml_id;

		}
	}
}
require_once("templates/header.html");
require_once("templates/nav.html");

$service_url = "http://ml.datascrub-152522.appspot.com/portal/ml/".$ml_id."/al";
$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
//echo "curl---response=".$curl_response;19687
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);

	echo "<h2 style='margin-left:300px;margin-top:200px;'>There was some problem  with Active Learning .<br><br>So, you are getting redirected to Field Select page.</h2>";

	?>
							<meta http-equiv="refresh" content="5;url=/mapfields/<?=$current_projectid;?>"> 
						<?php	
						die('error occured during curl exec. Additional info: ' . var_export($info));
}
else {
	$training_id=$curl_response;
	curl_close($curl);
	$sql_training="Select primary_id,secondary_id from ax_ml_".$ml_id."_training where id=".$training_id;
	//echo $sql_training;
	$primary_id="";
	$secondary_id="";
	if($res_training=$conn->query($sql_training))
	{
		if($res_training->num_rows>0)
		{
	
			while($row_training=$res_training->fetch_assoc())
			{
				$primary_id=$row_training['primary_id'];
				//echo $ml_id;
				$secondary_id=$row_training['secondary_id'];
			}
		}
	}
	
	//echo "<br>".$primary_id."======".$secondary_id;
	
	//CHECK IF SINGLE OR DOUBLE FILE PROJECT
	$secondfile=false;
	$sql="select * from ax_project_files where project_id=".$current_projectid;
	//echo $sql;
	$projectfileexist = $conn->query($sql);
	
	if ($projectfileexist->num_rows > 0) {
		$sql_file2_origID ="Select file_2 from ax_project_files where project_id=".$current_projectid;
		$result = $conn->query($sql_file2_origID);
	
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$file2_origID=$row['file_2'];
				if($file2_origID != '' && $file2_origID != null){
					$secondfile=true;
				}
			}
		}
	}	
	
	
	
	//GET PROCESSING VARIABLES FROM PROCESS TABLE
	$columns=array();
	//Check Includes and datatypes
	$sql="SELECT * from ax_job_".$current_projectid."_process";
	//echo $sql;
	
	$result = $conn->query($sql);
	//echo "hhh";
	while($row = $result->fetch_assoc())
	{
		//echo "===".$row['colid']."===".$row['type'];
		$columns[]=$row;
	}
	//print_r($columns);
	
	$colist="";
	$co2list="";
	
	$x=0;
	foreach($columns as $col){
	
		$colist.="`col_".$col['colid']."`,";
		if($col['col2id']){
			$co2list.="`col_".$col['col2id']."`,";
		}
	
	}
	
	//echo $colist;
	//echo "<br>";
	//echo $co2list;
	
	$colist_array=explode(",",substr($colist,0,strlen($colist)-1));
	$co2list_array=explode(",",substr($co2list,0,strlen($co2list)-1));
	?>
	 <div class="block analysis-wrap table-responsive">
	 
	 <div style="margin-left:100px;">
	 Total Questions Answered    :<?=$total_ques_answered;?><br>
	 Total Questions Answered YES:<?=$total_ques_yes;?><br>
	 Total Questions Answered  NO:<?=$total_ques_no;?><br>
	 </div>
	<?php
	$table='<form id="frmUserSelection" method="POST"><table class="table table-striped table-bordered " ><tr>';
	$tbheaders = array();
	
	$tbheaders="<th>id</th>";
	foreach($colist_array as $coheader)
	{
		$coheader=str_replace("col_","",$coheader);
	
		//$colname_map = DB::table('job_'.$id.'_name_mapping_primary')->where('colid', '=', $coheader)->pluck('colname');
		$coheader=str_replace("`","",$coheader);
		$colmap_sql="Select colname from ax_job_".$current_projectid."_name_mapping_primary where colid=".$coheader;
	
		//echo "<br>".$colmap_sql;
		$res_colmap = $conn->query($colmap_sql);
		if($res_colmap->num_rows>0)
		{
			while($row_colmap = $res_colmap->fetch_object())
			{
				$colname_map=$row_colmap->colname;
			}
		}
		if($colname_map!="")
			$tbheaders.= "<th>".$colname_map."</th>";
	}
	
	
	$table.=$tbheaders."</tr><tr>";
	
	$prim_ques="";
	$sec_ques="";
	$sql_prim_ques="select ".$colist."id  from ax_job_".$current_projectid."_primary where id=".$primary_id;
	$result_prim_ques = $conn->query($sql_prim_ques);
	//echo "hhh";
	while($row_prim_ques = $result_prim_ques->fetch_assoc())
	{
		//echo "===".$row['colid']."===".$row['type'];
		$prim_ques.="<td>".$row_prim_ques['id']."</td>";
		
		foreach($colist_array as $list)
		{
		$list=str_replace("`","",$list);
			//echo "<br>".$list."------------".$row_prim_ques['col_2'];
			$prim_ques.="<td>".$row_prim_ques[$list]."</td>";
		}
	}
	$table.=$prim_ques."</tr><tr>";
	
	
	if($secondfile)
	{
		$sql_sec_ques="select ".$co2list."id  from ax_job_".$current_projectid."_secondary where id=".$secondary_id;
	}
	else {
		$sql_sec_ques="select ".$co2list."id  from ax_job_".$current_projectid."_primary where id=".$secondary_id;
	}
	
	$result_sec_ques = $conn->query($sql_sec_ques);
	//echo "hhh";
	while($row_sec_ques = $result_sec_ques->fetch_assoc())
	{
	$sec_ques.="<td>".$row_sec_ques['id']."</td>";
		
		foreach($co2list_array as $list)
		{
			$list=str_replace("`","",$list);
			//echo "<br>".$list;
			$sec_ques.="<td>".$row_sec_ques[$list]."</td>";
		}
	}
	$table.=$sec_ques."</tr></table>";
	echo "<br><h2>Are these Two Matches?</h2>";
	
	echo "<br><br>";
	
	
	echo $table;
	
	?>
	<tr><td></td><td colspan="3">
	<input type="hidden" name="userSelection" id="userSelection"/>
	<input type="hidden" name="trainingId" id="trainingId"/>
	 <button id="Yes<?=$current_projectid;?>"  class="submitbtn yesButton btn btn-primary" role="button" onClick="putValue('Yes',<?=$training_id;?>);">Yes</button>
	 <button id="No<?=$current_projectid;?>"  class="submitbtn noButton btn btn-primary" role="button" onClick="putValue('No',<?=$training_id;?>);">No</button>
	 <button id="Done<?=$current_projectid;?>"  class="submitbtn doneButton btn btn-primary" role="button" onClick="putValue('Done',<?=$training_id;?>);">Done</button>
	 </td></tr></div>
	 
	 </form>
	 <script>
	 function putValue(type,trainingId)
	 {
		 document.getElementById("userSelection").value=type;
		 document.getElementById("trainingId").value=trainingId;
		 document.frmUserSelection.submit();
	 }
	 </script>
	<?php 
	/*echo "<b>".$tbheaders."</b><br>";
	echo $prim_ques;
	echo "<br>";
	echo $sec_ques;*/
	//header("Location:/initializeAL/".$current_projectid);
	exit;
}
require_once("templates/footer.html");
