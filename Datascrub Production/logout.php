<?php

session_start();
unset($_SESSION['username']);
unset($_SESSION['userid']);
unset($_SESSION['project_id']);
unset($_SESSION['project_name']);
header("Location:/");