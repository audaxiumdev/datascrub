<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();

$current_projectid=443;

$dropsql="DROP TABLE IF EXISTS `ax_job_".$current_projectid."_detail_results`";
//echo "<br>".$dropsql;
$conn->query($dropsql);
$sql="CREATE TABLE ax_job_".$current_projectid."_detail_results (
  			id int(11) unsigned NOT NULL AUTO_INCREMENT,
  			primeid int(11) DEFAULT NULL,
  			secondid int(11) DEFAULT NULL,
			col_7 int(11) DEFAULT NULL,
			col_8 int(11) DEFAULT NULL,
			col_10 int(11) DEFAULT NULL,
			col_12 int(11) DEFAULT NULL,
  			result decimal(10,2) DEFAULT NULL,
  			PRIMARY KEY (id),
			KEY result (result),
  			KEY primeid (primeid),
  			KEY secondid (secondid)
			)  CHARSET=latin1;";
//echo "<br>".$sql;
if($conn->query($sql))
{
	//echo "Results table created";
}
else
{
	echo $conn->error;
	error_log($conn->error);
	exit;
}



$field=strtolower($field);
	//$search=array("+","(",")",",","?","#","%","!","_","�","'","\\","<",">","*","\/","`","[","]","none","n/a","'","\'","not available","unknown");
	$field =trim($field);
	$field = str_replace($search,"",$field);
	$field=strtolower($field);

$sql="select  id,col_7,col_10,col_12,col_8 from ax_job_".$current_projectid."_primary ";
$res=$conn->query($sql);

if( $res->num_rows>0)
{
	//$total=$res->num_rows;
	$id1="";
	$phone1="";
	$zip1="";
	$emaildomain1="";
	$webdomain1="";
	while($row = $res->fetch_assoc())
	{
		$id1=$row["id"];
		$phone1=$row["col_7"];
		$zip1=$row["col_8"];
		$emaildomain1=$row["col_10"];
		$webdomain1=$row["col_12"];
		
		$sql2="select  id,col_7,col_10,col_12,col_8 from ax_job_".$current_projectid."_primary where id!=".$id1;
		$res2=$conn->query($sql2);
		
		if( $res2->num_rows>0)
		{
			$id2="";
			$phone2="";
			$zip2="";
			$emaildomain2="";
			$webdomain2="";
			
			
				while($row2 = $res2->fetch_assoc())
				{
				
				
					$id2=$row2["id"];
					$phone2=$row2["col_7"];
					$zip2=$row2["col_8"];
					$emaildomain2=$row2["col_10"];
					$webdomain2=$row2["col_12"];
					
					
					
					$phone_match="";
					$zip_match="";
					$emaildomain_match="";
					$webdomain_match="";
					
					
					$search=array("+","(",")",",","?","#","%","!"," ","-");
					$phone1 =trim($phone1);
					$phone1 = str_replace($search,"",$phone1);
					$phone2 =trim($phone2);
					$phone2 = str_replace($search,"",$phone2);
					
					$zip1 =trim($zip1);
					$zip1 = str_replace($search,"",$zip1);
					$zip2 =trim($zip2);
					$zip2 = str_replace($search,"",$zip2);
					
					$emaildomain1=strtolower(trim($emaildomain1));
					$emaildomain2=strtolower(trim($emaildomain2));
					
					$webdomain1=strtolower(trim($webdomain1));
					$webdomain2=strtolower(trim($webdomain2));
					$nooffields=0;
					//echo $id1."==".$id2."==".$phone1."==".$phone2."==".$zip1."==".$zip2."==".$emaildomain1."==".$emaildomain2."==".$webdomain1."==".$webdomain2;
							if($phone1==$phone2 && $phone1!="" && $phone2!="")
							{
								$phone_match=1;
								$nooffields++;
							}
							elseif( $phone1==$phone2 && $phone1=="" && $phone2=="")
							{
								$phone_match=0;
							}
							else{
								$phone_match=0;
								$nooffields++;
							}
						
							if($zip1==$zip2 && $zip1!="" && $zip2!="")
							{
								$zip_match=1;
								$nooffields++;
							}
							elseif($zip1==$zip2 && $zip1=="" && $zip2=="")
							{
								$zip_match=0;
							}
							else{
								$zip_match=0;
								$nooffields++;
							}
							
							if($emaildomain1==$emaildomain2 && $emaildomain1!="" && $emaildomain2!="")
							{
								$emaildomain_match=1;
								$nooffields++;
							}
							elseif($emaildomain1==$emaildomain2 && $emaildomain1=="" && $emaildomain2=="")
							{
								$emaildomain_match=0;
							}
							else{
								$emaildomain_match=0;
								$nooffields++;
							}
							
							if($webdomain1==$webdomain2 && $webdomain1!="" && $webdomain2!="")
							{
								$webdomain_match=1;
								$nooffields++;
							}
							elseif($webdomain1==$webdomain2 && $webdomain1=="" && $webdomain2=="")
							{
								$webdomain_match=0;
							}
							else{
								$webdomain_match=0;
								$nooffields++;
							}
							//echo "\n Match==".$phone_match."==".$zip_match."==".$emaildomain_match."==".$webdomain_match;
							$result=(($phone_match+$zip_match+$emaildomain_match+$webdomain_match)/4)*100;
							
							if($result==25)
							{
								if($phone1==$phone2 && $phone1!="" && $phone2!="" && $zip1=="" && $zip2=="" && $emaildomain1=="" && $emaildomain2=="" && $webdomain1=="" && $webdomain2=="")
								{
									$result=70;
								}
							}
							if($result==50)
							{
								if($phone1==$phone2 && $phone1!="" && $phone2!="")
								{
									$result=73;
								}
								if($zip1==$zip2 && $zip1!="" && $zip2!="")
								{
									$result=72;
								}
								if($phone1==$phone2 && $phone1!="" && $phone2!="" && $zip1==$zip2 && $zip1!="" && $zip2!="")
									{
										$result=74;
									}
							
							}
							
							if($result==75  && $nooffields==3)
							{
								$result=100;
							}
							if($result==74  && $nooffields==2 )
							{
								$result=100;
							}
							if($result==73  && $nooffields==2 )
							{
								$result=100;
							}
							if($result==72  && $nooffields==2 )
							{
								$result=100;
							}
							if($result==50  && $nooffields==2 )
							{
								$result=100;
							}
							if($result==70  && $nooffields==1 )
							{
								$result=100;
							}
							
							
							
							echo "\n".$result."\n\n";	
					if($result>0)
					{
						$sql_insert="INSERT INTO  ax_job_".$current_projectid."_detail_results(primeid,secondid,col_7,col_8,col_10,col_12,result) VALUES
								(".$id1.",".$id2.",".$phone_match.",".$zip_match.",".$emaildomain_match.",".$webdomain_match.",".$result.")";
						echo $sql_insert;
						if($conn->query($sql_insert))
						{
						
						}
						else
						{
							echo $conn->error;
							error_log($conn->error);
							exit;
						}
						
					}
					
					
				}
		
		}
		
		
	}

}
