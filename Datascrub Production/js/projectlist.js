function LoadProjList()
{
    $(function() {
        $('.project-wrap').load('/projects', function() {
            $(".project-wrap .table-list").tablesorter({
                widgets: ["zebra", "filter"]
            });
        });

    });
}


setInterval( LoadProjList, 10000 );
