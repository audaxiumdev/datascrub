jQuery(document).ready(function(){
    $.confirm = function (message, title, callback) {
    	confirmModal = $('#confirmationModal');
        confirmModal.find('.modal-title').text(title);
        confirmModal.find('p').text(message);

        $('.uibtn-delete').entwine({
            onclick: function (){ callback(); return false; }
        });

        return confirmModal;
    }

    $.confirmClose = function (){
    	
        $('#confirmationModal').modal('hide');
    }

    $.showLoader = function () {
        $.blockUI({
            message: '<img src="/images/loader.gif" alt=""/>',
            css: {
                width: '50px',
                padding: '10px',
                border: 'none',
                borderRadius: '10px',
                left: '48%'
            }
        });
    }

    $.showElementLoader = function (element) {
        $(element).block({
            message: '<img src="/images/loader.gif" alt=""/>',
            css: {
                width: '50px',
                padding: '10px',
                border: 'none',
                borderRadius: '0',
                left: '48%',
            },
            overlayCSS: {
                '-ms-filter': "progid:DXImageTransform.Microsoft.Alpha(Opacity=1)",
                filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=1)",
                '-moz-opacity': "1",
                opacity: "1",
                'background-color': "#fff"
            },
            baseZ: 1
        });
    }

    $.isAllowed = function (response) {
        if (response.isAllowed !== undefined && response.isAllowed == false) {
            return false;
        } else {
            return true;
        }
    }

    $.refreshTable = function () {
        $(".table-list").trigger("update");
    }

    $(".table-list").tablesorter({
        widgets: ["zebra", "filter"]
    });

    $('.select').chosen();
    $('.modal-select').chosen({width: "100% !important"});

    /*$( "#sortable1" ).sortable();
    $( "#sortable1" ).disableSelection();
    $( "#sortable2" ).sortable();
    $( "#sortable2" ).sortable( "disable" );*/

    // CSRF protection
    $.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });

    var url = window.location;
    // Will only work if string in href matches with location
    $('ul.nav a[href="'+ url +'"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');

    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3
        $(this).parent().fadeOut(function () {
            $(this).hide();
        });
    });

});