
jQuery(document).ready(function($){
	
    $('.viewResults').entwine({
        onclick: function(){
            var thismodal = $('#ResultsModal').modal('show');
            thismodal.find('#proceed_results').attr('href', $(this).attr('data-proceed'));
            thismodal.find('#rerun_analysis').attr('data-projid', $(this).attr('data-projectid'));
        }
    });

    $('#rerun_analysis').entwine({
        onclick: function(){
            var projid = $(this).attr('data-projid'); //console.log(projid);
            $('.close:visible').trigger('click');
            var rerunmodal = $('#RerunResultsModal').modal('show');

            //rerunmodal.find('#same_fields').attr('href', '/project/samefields/'+projid);
            rerunmodal.find('#same_fields').attr('data-id', projid);
            rerunmodal.find('#same_fields').addClass('process_samefields');
            rerunmodal.find('#new_fields').attr('href', '/project/fieldselection/'+projid);
            //alert('re run analysis');
        }
    });

    $('.ecm-add-user').entwine({
        onclick: function (e) {
            e.preventDefault();
            $('#UserModal').modal('show');
            modal = $('#UserModal');
            modal.find('.user-form').trigger('reset');
            modal.find('.modal-select').val('').trigger('chosen:updated');
            modal.find('.ecm-save-user').data('action', 'create');
            modal.find('#gridSystemModalLabel').html('<i class="fa fa-user"></i> Create User');
        }
    });

    $('.ecm-edit-user').entwine({
        onclick: function (e) {
            e.preventDefault();
            $('.ecm-save-user').data('id', this.data('id'));
            this.showForm();
        },
        showForm: function () {
            dis = this;
            $.ajax({
                url: '/user/getuser',
                type: 'get',
                data:{"id": dis.data('id')},
                beforeSend: function () {
                    $.showLoader();
                },
                success: function(data){
                    $('#UserModal').modal('show');
                    modal = $('#UserModal');
                    modal.find('.user-form').trigger('reset');

                    modal.find('.ecm-save-user').data('action', 'update');
                    modal.find('#gridSystemModalLabel').html('<i class="fa fa-user"></i> Edit User');
                    $.unblockUI();
                    $('.user-form').populateForm(data);
                }
            });
        }
    });

    $('.user-form').entwine({
        populateForm: function(data) {
            modal = $('#UserModal');
            modal.find('input[name=id]').val(data.id);
            modal.find('input[name=first_name]').val(data.first_name);
            modal.find('input[name=last_name]').val(data.last_name);
            modal.find('input[name=email]').val(data.email);
            modal.find('input[name=company]').val(data.company);
            modal.find('input[name=address]').val(data.address);
            modal.find('input[name=phone]').val(data.phone);
            modal.find('input[name=password]').val(data.password);

            modal.find('.modal-select').val('').trigger('chosen:updated');
            modal.find('select[name=company] option').each(function (){
                if($(this).val() == data.company){
                    $(this).prop('selected', true).trigger('chosen:updated');
                }
            });
            modal.find('select[name=role] option').each(function (){
                if($(this).val() == data.role){
                    $(this).prop('selected', true).trigger('chosen:updated');
                }
            });

            $('.modal-select').chosen();
        },
        saveUser: function () {
            this.validateForm();
            this.submit();
            return false;
        },
        validateForm: function() {
            actionBtn = $('.ecm-save-user');
            action = actionBtn.data('action');

            var dis = this;

            validator = this.validate({
                ignore: [],
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function ()
                {
                    dis.ajaxSubmit({
                        url: '/user/'+action,
                        method: 'post',
                        type: 'json',
                        beforeSend: function (){
                            $.showLoader();
                            $('.close:visible').trigger('click');
                        },
                        success: function (data) {
                            $.unblockUI();
                            if(action == 'create'){
                                $('#userCreateAlert').addClass('in');
                                $('#userCreateAlert').show();
                            }else{
                                $('#userSaveAlert').addClass('in');
                                $('#userSaveAlert').show();
                            }

                            $('.user-wrap').load('/user/loadview', function() {
                                $(".user-wrap .table-list").tablesorter({
                                    widgets: ["zebra", "filter"]
                                });
                            });

                        }
                    });
                }
            });

            updateRules = {
                email: {
                    email: true,
                    required: true
                },
                first_name: "required",
                last_name: "required",
                confirm_password: {
                    equalTo: '#password'
                }
            };

            //validator extension
            var validatorSettings = validator.settings;
            $.extend(validatorSettings, {
                rules: updateRules
            });

        }
    });

    $('.ecm-save-user').entwine({
        onclick: function() {
            $('.user-form').saveUser();
        }
    });

    $('.ecm-delete-user').entwine({
        onclick: function() {
            var elemobj = this;
            var callback = function () {
                elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});
                $.confirmClose();
            }
            $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, callback);
            return false;
        },

        delete: function (element, data) {
            $.ajax({
                url: '/user/delete',
                type: 'post',
                data: data,
                beforeSend: function() {
                    $.showLoader();
                },
                success: function (data) {
                    $('#userAlert').addClass('in');
                    $('#userAlert').show();
                    element.parent().parent().parent().fadeOut(function () {
                        $(this).remove();
                    });
                    $.unblockUI();
                }
            });
        }
    });

    $('.ecm-status-user').entwine({
        onclick: function() {

            var elemobj = this;
            var callback = function () {
                elemobj.status(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id'),"status": elemobj.data('status')});
                $.confirmClose();
            }

            $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, callback);
            return false;
        },

        status: function (element, data) {

            $.ajax({
                url: '/user/changestatus',
                type: 'post',
                data: data,
                beforeSend: function() {
                    $.showLoader();
                },
                success: function (ret) {

                    if(ret == 1){
                        $('#activateAlert').addClass('in');
                        $('#activateAlert').show();
                    }else if(ret == 0){
                        $('#deactivateAlert').addClass('in');
                        $('#deactivateAlert').show();
                    }else{
                        $('#errorAlert').addClass('in');
                        $('#errorAlert').show();
                    }

                    $('.user-wrap').load('/user/loadview', function() {
                        $(".user-wrap .table-list").tablesorter({
                            widgets: ["zebra", "filter"]
                        });
                    });

                    $.unblockUI();

                }
            });
        }
    });


    $('.ecm-delete-company').entwine({
        onclick: function() {
            var elemobj = this;
            var callback = function () {
                elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});
                $.confirmClose();
            }
            $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, callback);
            return false;
        },

        delete: function (element, data) {
            $.ajax({
                url: '/company/delete',
                type: 'post',
                data: data,
                beforeSend: function() {
                    $.showLoader();
                },
                success: function (data) {
                    $('#companyDeleteAlert').addClass('in');
                    $('#companyDeleteAlert').show();
                    element.parent().parent().parent().fadeOut(function () {
                        $(this).remove();
                    });
                    $.unblockUI();
                }
            });
        }
    });

    $('.ecm-add-company').entwine({
        onclick: function (e) {
            e.preventDefault();
            $('#CompanyModal').modal('show');
            modal = $('#CompanyModal');
            modal.find('.company-form').trigger('reset');
            modal.find('.modal-select').val('').trigger('chosen:updated');
            modal.find('.ecm-save-company').data('action', 'create');
            modal.find('#gridSystemModalLabel').html('<i class="fa fa-folder"></i> Add Company');
        }
    });


    $('.ecm-edit-company').entwine({
        onclick: function (e) {
            e.preventDefault();
            $('.ecm-save-company').data('id', this.data('id'));
            this.showForm();
        },
        showForm: function () {
            dis = this;
            $.ajax({
                url: '/company/getcompany',
                type: 'get',
                data:{"id": dis.data('id')},
                beforeSend: function () {
                    $.showLoader();
                },
                success: function(data){
                    $('#CompanyModal').modal('show');
                    modal = $('#CompanyModal');
                    modal.find('.company-form').trigger('reset');

                    modal.find('.ecm-save-company').data('action', 'update');
                    modal.find('#gridSystemModalLabel').html('<i class="fa fa-user"></i> Edit Company');
                    $.unblockUI();
                    $('.company-form').populateForm(data);
                }
            });
        }
    });

    $('.company-form').entwine({
        populateForm: function(data) {
            modal = $('#CompanyModal');
            modal.find('input[name=id]').val(data.id);
            modal.find('input[name=company_name]').val(data.company_name);
            modal.find('textarea[name=description]').val(data.description);
            modal.find('input[name=email]').val(data.email);
        },
        saveCompany: function () {
            this.validateForm();
            this.submit();
            return false;
        },
        validateForm: function() {
            actionBtn = $('.ecm-save-company');
            action = actionBtn.data('action');

            var dis = this;

            validator = this.validate({
                ignore: [],
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function ()
                {
                    dis.ajaxSubmit({
                        url: '/company/'+action,
                        method: 'post',
                        type: 'json',
                        beforeSend: function (){
                            $.showLoader();
                            $('.close:visible').trigger('click');
                        },
                        success: function (data) { console.log(data);
                            $.unblockUI();
                            if(data != ''){
                                //$('#create_proj_btn').hide();
                                $('#companyCreateAlert').addClass('in');
                                $('#companyCreateAlert').show();
                                //$('#upload_files').show();
                                //$('.uploadfiles-form').find('#project_id').val(data);
                                $('#emptytable').hide();
                                $('#showtable').show();
                                $('.company-wrap').load('/company/companyloadview', function() {
                                    $(".company-wrap .table-list").tablesorter({
                                        widgets: ["zebra", "filter"]
                                    });
                                });
                            }
                        }
                    });
                }
            });

            updateRules = {
                project_name: "required",
                user_id: "required"
            };

            //validator extension
            var validatorSettings = validator.settings;
            $.extend(validatorSettings, {
                rules: updateRules
            });

        }
    });

    $('.ecm-save-company').entwine({
        onclick: function() {
            $('.company-form').saveCompany();
        }
    });

    $('.ecm-add-project').entwine({
        onclick: function (e) {
            e.preventDefault();
            $('#ProjectModal').modal('show');
            //$('input[name="project_name"]').focus();
            modal = $('#ProjectModal');
            modal.find('.project-form').trigger('reset');
            modal.find('.modal-select').val('').trigger('chosen:updated');
            modal.find('.ecm-save-project').data('action', 'create');
            modal.find('#gridSystemModalLabel').html('<i class="fa fa-folder"></i> Create Project');
            modal.find('input[name="project_name"]').focus();
        }
    });

    $('#ProjectModal').on('shown.bs.modal', function () {
        $('input[name="project_name"]').focus();
    })

    $('.project-form').entwine({
        saveProject: function () {
            this.validateForm();
            this.submit();
            return false;
        },
        validateForm: function() {
            actionBtn = $('.ecm-save-project');
            action = actionBtn.data('action');

            var dis = this;

            validator = this.validate({
                ignore: [],
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function ()
                {
                    dis.ajaxSubmit({
                        url: '/project/'+action,
                        method: 'post',
                        type: 'json',
                        beforeSend: function (){
                            $.showLoader();
                            $('.close:visible').trigger('click');
                        },
                        success: function (data) { console.log(data);
                            $.unblockUI();
                            if(data != ''){
                                //$('#create_proj_btn').hide();
                                $('#projectCreateAlert').addClass('in');
                                $('#projectCreateAlert').show();
                                //$('#upload_files').show();
                                //$('.uploadfiles-form').find('#project_id').val(data);
                                /*$('.project-wrap').load('/projects/projectloadview', function() {
                                    $(".project-wrap .table-list").tablesorter({
                                        widgets: ["zebra", "filter"]
                                    });
                                });*/
                                window.location = '/project/view/'+data;
                            }
                        }
                    });
                }
            });

            updateRules = {
                project_name: "required",
                user_id: "required"
            };

            //validator extension
            var validatorSettings = validator.settings;
            $.extend(validatorSettings, {
                rules: updateRules
            });

        }
    });

    $('.ecm-save-project').entwine({
        onclick: function() {
            $('.project-form').saveProject();
        }
    });

    $('.ecm-confirm-files').entwine({
        onclick: function() {
            window.location='/project/fieldselection/'+$(this).attr('id');
        }
    });

    
    

    
    
    $('.ecm-delete-project').entwine({
        onclick: function() {
        	
        	var elemobj = this;
            var callback = function () {
                elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});
                $.confirmClose();
            }
            $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, callback);
            return false;
        },

        delete: function (element, data) {
        	   $.ajax({
                url: '/project/delete.php',
                type: 'post',
                data: data,
                beforeSend: function() {
                    $.showLoader();
                },
                success: function (data) {
                	$('#projectDeleteAlert').addClass('in');
                    $('#projectDeleteAlert').show();
                    element.parent().parent().parent().fadeOut(function () {
                        $(this).remove();
                    });
                    $.unblockUI();
                }
            });
        }
    });

    $('.ecm-field-selection.active').entwine({
        onclick: function(){
            if($(this).hasClass('viewResults')){
                var thismodal = $('#ResultsModal').modal('show');
                thismodal.find('#proceed_results').attr('href', $(this).attr('data-proceed'));
                thismodal.find('#rerun_analysis').attr('data-projid', $(this).attr('data-projectid'));
            }else{
            	//alert(this);
                var elemobj = this;
                var sortable1 = $("#to_file1").sortable("toArray");
                var sortable2 = '';

                if($("#to_file2").length > 0){
                    sortable2 = $("#to_file2").sortable("toArray");
                }else if($("#from_file2").length > 0){
                    sortable2 = $("#from_file2").sortable("toArray");
                }

                /*var priority = $('select[name="field_priority[]"]').map(function(){
                                   return $(this).val();
                               }).get();*/
                var type = $('select[name="field_type[]"]').map(function(){
                                    return $(this).val();
                                }).get();
               /* var exact = [];
                $('input[name="exact"]').each(function() {
                    //exact.push($(this).val());
                    exactval = $(this).is(':checked');
                    if(exactval){
                        exact.push(1);
                    }else{
                        exact.push(0);
                    }
                });*/

               /* var checkprio = 'true';
                var i = 0;
                for (i; i < priority.length; i++) {
                	if (priority[i].trim().length != 0) {
                        checkprio = 'false';
                    }
                }*/
                var checkprio = 'false';
               /* var i = 0;
                for (i; i < priority.length; i++) {
                	if (priority[i].trim().length == 0) {
                        checkprio = 'true';
                    }
                }*/

                if(sortable1.length == 0){
                    $('#errorAlert').addClass('in');
                    $('#errorAlert').show();
                }else if((sortable2 != '') && (sortable1.length > sortable2.length)){
                    $('#errorAlert').addClass('in');
                    $('#errorAlert').show();
                }else if(checkprio == 'true'){
                    //console.log(priority);
                    $('#prioAlert').addClass('in');
                    $('#prioAlert').show();
                }else{ //console.log(checkprio);
                   // var data = {"_token": elemobj.data('csrftoken'),"id": elemobj.attr('id'), "fields1": sortable1,  "fields2": sortable2,  "priority": priority, "type": type, "exact": exact };
                    var data = {"_token": elemobj.data('csrftoken'),"id": elemobj.attr('id'), "fields1": sortable1,  "fields2": sortable2, "type": type };
                    //alert(sortable1+sortable2+type);
                    //alert("sds"+data);
                    //console.log('success');
                    $.ajax({
                        url: '/savefields.php',
                        type: 'POST',
                        data: {data:data},
                        beforeSend: function() {
                               $.showLoader();
                           },
                        error: function(xhr){
                            alert("An error occured: " + xhr.status + " " + xhr.statusText);
                        },

                        
                      
                        //data: data,
                       // beforeSend: function() {
                         //   $.showLoader();
                       // },
                        success: function (data) {
                        	
                           if(data != ''){
                        	     window.location.href = '/showing-progress.php?id='+elemobj.attr('id');
                        	   
                            }
                           
                            //console.log(data);
                            $.unblockUI();
                        }
                    });
                }
            }
        }
    });

    $( "#from_file1" ).sortable({
        connectWith: ".fields1",
        appendTo: 'body',
        tolerance: 'pointer',
        revert: 'invalid',
        forceHelperSize: true,
        helper: 'original',
        scroll: true,
        placeholder: {
            element: function(clone, ui) {
                //console.log(clone[0].innerHTML);
                return $('<li class="placeholder_highlight ui-state-default col-md-12 draggable1 ui-sortable-handle">'+clone[0].innerHTML+'</li>');
            },
            update: function() {
                return;
            }
        },
        start: function( event, ui ) {
            clone = $(ui.item[0].outerHTML).clone();
        }
    }).disableSelection();

    $( "#to_file1" ).sortable({
        connectWith: ".fields1",
        placeholder: {
            element: function(clone, ui) {
                //console.log(clone[0].innerHTML);
                return $('<li class="placeholder_highlight ui-state-default col-md-12 draggable1 ui-sortable-handle">'+clone[0].innerHTML+'</li>');
            },
            update: function() {
                return;
            }
        },
        receive: function( event, ui ) {
            var totals1 = $(this).children().length;
            var totals2 = '';
            //console.log($("#to_file2").children().length);
            if($("#to_file2").children().length > 0){
                totals2 = $("#to_file2").children().length;
            }else if($("#from_file2").children().length > 0){
                totals2 = $("#from_file2").children().length;
            }else{
                totals2 = totals1;
            }

            //console.log(totals1);
            //console.log(totals2);

            if(totals1 == totals2){
                $('.ecm-field-selection').addClass('active');
                $('.ecm-field-selection').removeClass('viewResults');
            }else{
                $('.ecm-field-selection').removeClass('active');
            }

            var ind = ui.item.index();
            //console.log($('#priority').children().length);
            //var condition_html = '<div class="condition"><div class="col-md-8"><select name="field_priority[]" class="select field_priority"><option value="">- Priority -</option><option value="1" selected>Normal</option><option value="2">High</option><option value="3">Low</option></select></div><div class="col-md-4"><input class="checkbox" type="checkbox" value="" name="exact" /><label class="control-label">Exact</label></div><div id="field_type" class="col-md-8"><select name="field_type[]" class="select field_type"><option value="">- Data Type -</option><option value="0">Text</option><option value="1">Number</option><option value="2">Email</option><option value="3">Postal</option><option value="4">Phone</option><option value="5">Company</option><option value="6">Address</option><option value="7">First Name</option><option value="8">Last Name</option><option value="9">City</option><option value="10">State</option><option value="11">Country</option><option value="12">Web Address</option><option value="13">Date</option></select></div></div>';
            
            var condition_html = '<div class="condition"><div class="col-md-8" style="width:100px;height:40px;"></div><div id="field_type" class="col-md-8"><select name="field_type[]" class="select field_type"><option value="">- Data Type -</option><option value="0">Text</option><option value="1">Number</option><option value="2">Email</option><option value="3">Postal</option><option value="4">Phone</option><option value="5">Company</option><option value="6">Address</option><option value="7">First Name</option><option value="8">Last Name</option><option value="9">City</option><option value="10">State</option><option value="11">Country</option><option value="12">Web Address</option><option value="13">Date</option></select></div></div>';
            //$('#priority').append('<div cl ass="col-md-12 prio"><select name="field_priority[]" class="select field_priority"><option value="0">- Select Search Priority -</option><option value="1">Normal</option><option value="2">High</option><option value="3">Low</option></select></div>');
            if ($('#priority').children().length > 0) {
                // It has at least one
                $("#priority div.condition").eq(ind-1).after(condition_html);
            }else{
                $('#priority').append(condition_html);
            }

            $('.select').chosen();

        },
        start: function(event, ui) {
            ui.item.startPos = ui.item.index();
            clone = $(ui.item[0].outerHTML).clone();
        },
        remove: function( event, ui ) {
            var ind = ui.item.startPos; //ui.item.index();

            $('.ecm-field-selection').removeClass('viewResults');
            $('#priority div.condition').eq(ind).remove();
        }
    }).disableSelection();

    $( "#from_file2" ).sortable({
        connectWith: ".fields2",
        appendTo: 'body',
        tolerance: 'pointer',
        revert: 'invalid',
        forceHelperSize: true,
        helper: 'original',
        scroll: true,
        placeholder: {
            element: function(clone, ui) {
                //console.log(clone[0].innerHTML);
                return $('<li class="placeholder_highlight ui-state-default col-md-12 ui-sortable-handle">'+clone[0].innerHTML+'</li>');
            },
            update: function() {
                return;
            }
        },
        start: function( event, ui ) {
            clone = $(ui.item[0].outerHTML).clone();
        }
    }).disableSelection();

    $( "#to_file2" ).sortable({
        connectWith: ".fields2",
        placeholder: {
            element: function(clone, ui) {
                //console.log(clone[0].innerHTML);
                return $('<li class="placeholder_highlight ui-state-default col-md-12 draggable1 ui-sortable-handle">'+clone[0].innerHTML+'</li>');
            },
            update: function() {
                return;
            }
        },
        receive: function(event, ui){
            var totals2 = $(this).children().length;
            var totals1 = $("#to_file1").children().length;

            if(totals1 == totals2){
                $('.ecm-field-selection').addClass('active');
                $('.ecm-field-selection').removeClass('viewResults');
            }else{
                $('.ecm-field-selection').removeClass('active');
            }
        },
        start: function(event, ui) {
            ui.item.startPos = ui.item.index();
            clone = $(ui.item[0].outerHTML).clone();
        },
        remove: function( event, ui ) {
            $('.ecm-field-selection').removeClass('viewResults');
        }
    }).disableSelection();

    $('#box_title_btm').entwine({
        onclick: function(){
            //$('#notif_content').slideUp();
            $('#notif_content').show("slide", { direction: "up" }, 200);
        }
    });

    $('#box_title_top').entwine({
        onclick: function(){
            //$('#notif_content').slideDown();
            $('#notif_content').hide("slide", { direction: "down" }, 200);
        }
    });

    var s1 = $("#to_file1").children().length;
    var s2 = '';

    if($("#to_file2").children().length > 0){
        s2 = $("#to_file2").children().length;
    }else if($("#from_file2").children().length > 0){
        s2 = $("#from_file2").children().length;
    }else{
        s2 = s1;
    }

    if((s1 > 0) && (s1 == s2)){
        $('.ecm-field-selection').addClass('active');
    }else{
        $('.ecm-field-selection').removeClass('active');
    }

    $('.process_samefields').entwine({
        onclick: function() {
            var elemobj = this;
            var data = {"_token": elemobj.attr('data-csrftoken'),"id": elemobj.attr('data-id')};
            //console.log(data);
            $.ajax({
                url: '/project/processsamefields',
                type: 'post',
                data: data,
                beforeSend: function() {
                    $.showLoader();
                },
                success: function (data) {
                  if(data != ''){
                        window.location.href = data;
                  }
                  $.unblockUI();
                }
            });
        }
    });

    $('.ecm-export-results').entwine({
        onclick: function() {
        	//alert("export results");
           /* $('#ExportModal').modal('show');
            $('#ExportModal').find('#errors').html('');
            $('#ExportModal').find('#export_option').val('').trigger('chosen:updated');*/

            $('#export_options').css('top', $(this).offset().top+35);
            $('#export_options').css('left', $(this).offset().left);
            $('#export_options').toggle();
            var alloptions = $('#export_options').children('li');
            alloptions.removeClass('selected');
        }
    });

    $('#export_select li').entwine({
        onclick: function(){
        	//alert("clicked");
            var alloptions = $('#export_select').children('li');
            alloptions.removeClass('selected');
            $(this).addClass('selected');

            var elemobj = $('.ecm-export-results');

            var range_from = $( ".fuzzlogic" ).slider( "values", 0 );
            var range_to = $( ".fuzzlogic" ).slider( "values", 1 );

            var queryvalue = $(this).data("value");
//alert(queryvalue);
            if(queryvalue == ''){
                alert('Please select an option to export.');
            }else{

                var export_file = '/export_results.php?id=' + elemobj.attr('id') + '&from=' + range_from + '&to=' + range_to + '&query=' + queryvalue + '&_token=' + elemobj.data('csrftoken');

                if(queryvalue == 'all'){
                    export_file = '/exportall_results.php?id=' + elemobj.attr('id') + '&from=' + range_from + '&to=' + range_to + '&query=' + queryvalue + '&_token=' + elemobj.data('csrftoken');
                }

                if(export_file == 'error'){
                    $('#exportfileAlert').addClass('in');
                    $('#exportfileAlert').show();
                }else{
                    window.location = export_file;
                }

                alloptions.removeClass('selected');
                $('#export_options').toggle();
            }
        }
    });

    $("span.question").entwine({
        onclick: function(){
            $.unblockUI();
            /*$('div#pop-up').css('top', $(this).offset().top+25);
            $('div#pop-up').html($(this).attr('data-help'));
            $('div#pop-up').toggle();*/
            var elemobj = this;
            helpmodal = $('#HelpModal').modal('show');
            helpmodal.find('.modal-header').show();
            helpmodal.find('#popup').html($(elemobj).attr('data-help'));
        }
    });

    $( "#tabs" ).tabs();

    $('#fallback_form1').entwine({
        submitFile: function () {
        	
            this.validateForm();
            this.submit();
            return false;
        },
        validateForm: function() {
            var dis = this;

            validator = this.validate({
                ignore: [],
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function ()
                {
                	 // url: '/project/uploadfiles.php',
                	dis.ajaxSubmit({
                      	url: document.getElementById("uploadUrl11").innerHTML,
                        method: 'post',
                        type: 'json',
                        beforeSend: function (){
                        	
                            $.showLoader();
                            $('.close:visible').trigger('click');
                            
                        },
                        
                        success: function (data) { console.log(data);
                        //alert(data);
                            $.unblockUI();
                            if(data != ''){
                                $('.ecm-upload-files').addClass('proceed');
                            }
                        }
                    });
                }
            });

            updateRules = {
                file_1: "required"
            };

            //validator extension
            var validatorSettings = validator.settings;
            $.extend(validatorSettings, {
                rules: updateRules
            });

        }
    });

    $('#fallback_upload1').entwine({
        onclick: function(){ //console.log('here');
            $('#fallback_form1').submitFile();
        }
    });

    $('#fallback_form2').entwine({
        submitFile: function () {
            this.validateForm();
            this.submit();
            return false;
        },
        validateForm: function() {
            var dis = this;

            validator = this.validate({
                ignore: [],
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function ()
                {
                	//url: '/project/uploadfiles.php',
                	
                	dis.ajaxSubmit({
                        url: document.getElementById("uploadUrl11").innerHTML,
                        method: 'post',
                        type: 'json',
                        beforeSend: function (){
                            $.showLoader();
                            $('.close:visible').trigger('click');
                        },
                        success: function (data) { console.log(data);
                       // alert(data);
                            $.unblockUI();
                            if($('#old_file1').length > 0){
                                $('.ecm-upload-files').addClass('proceed');
                            }
                        }
                    });
                }
            });

            updateRules = {
                file_2: "required"
            };

            //validator extension
            var validatorSettings = validator.settings;
            $.extend(validatorSettings, {
                rules: updateRules
            });
        }
    });

    $('#fallback_upload2').entwine({
        onclick: function(){ //console.log('here');
            $('#fallback_form2').submitFile();
        }
    });
});

