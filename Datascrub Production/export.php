<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}

//checkProjectCompleted($current_projectid,$conn);

$sql="select * from ax_project_files where project_id='".$current_projectid."'";
//echo $sql;
$res = $conn->query($sql);
$file1="";
$file2="";

while($row=$res->fetch_assoc())
{

	$file1=$row['file_1'];
	$file2=$row['file_2'];
}
$memcache = new Memcache;
$key="prime_counter_".$current_projectid;
$primcounter=$memcache->get($key);

if($file2!="")
{
$memcache = new Memcache;
$key="sec_counter_".$current_projectid;
$seccounter=$memcache->get($key);
}
else
{
	$seccounter=0;
}
//echo "primcounter===".$primcounter."==seccountr==".$seccounter;
//echo $primcounter."/".$seccounter;



$TotalPrimaryQuery = $conn->query("SELECT count(*) as cnt from ax_job_".$current_projectid."_primary");
while($TotalPrimaryResult =$TotalPrimaryQuery->fetch_assoc())
{
	$TotalPrimaryRecords=$TotalPrimaryResult['cnt'];
}

$TotalPrimaryAnalysisQuery = $conn->query("SELECT count(*) as cnt from ax_job_".$current_projectid."_analysis_primary");
while($TotalPrimaryAnalysisResult =$TotalPrimaryAnalysisQuery->fetch_assoc())
{
	$TotalPrimaryAnalysisRecords=$TotalPrimaryAnalysisResult['cnt'];
}

if($file2!="")
{

	$TotalSecondaryQuery = $conn->query("SELECT count(*) as cnt from `ax_job_".$current_projectid."_secondary`");
	
	while($TotalSecondaryRows = $TotalSecondaryQuery->fetch_assoc())
	{
		$TotalSecondaryRecords = $TotalSecondaryRows['cnt'];
	}
	
	$TotalSecondaryAnalysisQuery = $conn->query("SELECT count(*) as cnt from `ax_job_".$current_projectid."_analysis_secondary`");
	
	while($TotalSecondaryAnalysisRows = $TotalSecondaryAnalysisQuery->fetch_assoc())
	{
		$TotalSecondaryAnalysisRecords = $TotalSecondaryAnalysisRows['cnt'];
	}
}


if($seccounter==0)
{
$chkcondition=(($TotalPrimaryRecords==$TotalPrimaryAnalysisRecords)  || ($primcounter==0 && $seccounter==0));

}
else 
{
$chkcondition=((($TotalPrimaryRecords==$TotalPrimaryAnalysisRecords) && ($TotalSecondaryRecords==$TotalSecondaryAnalysisRecords)) || ($primcounter==0 && $seccounter==0));
		
}


if($chkcondition)
{
	$memcache = new Memcache;
	$key="prime_counter_".$current_projectid;
	$memcache->delete($key);
	$memcache = new Memcache;
	$key="prime_counter_".$current_projectid;	
	$memcache->delete($key);

	//header("Location:http://ml.datascrub-152522.appspot.com/".$current_projectid);
	
	//exit;

		header("Location:/sendinit/".$current_projectid);
		exit;
/*
$service_url = "http://ml.datascrub-152522.appspot.com/portal/ml/".$ml_id."/init";
$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
//echo "curl---response=".$curl_response;
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);
	
	echo "<h2 style='margin-left:300px;margin-top:200px;'>There was some problem initializing the Learning .<br><br>So, you are getting redirected to Field Select page.</h2>";
	
	?>
							<meta http-equiv="refresh" content="5;url=/mapfields/<?=$current_projectid;?>"> 
						<?php	
						die('error occured during curl exec. Additional info: ' . var_export($info));
						exit;
}
else {
	curl_close($curl);
	
	header("Location:/initializeAL/".$current_projectid);
	exit;
}
*/
//$decoded = json_decode($curl_response);
//echo "<br>decoded===";
//print_r($decoded);
//echo $decoded."===".$ml_id;
/*if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
 die('error occured: ' . $decoded->response->errormessage);
 }*/

//echo 'response ok!';
//var_export($decoded->response);










}
else {
	//header("Location:/showing-progress.php?id=".$current_projectid);
	?>
	
	<h1>Still processing the files...</h1>
	
	
	<?php
	
}
require_once("templates/header.html");
if($chkcondition)
{
	?>

<div style="width:80%;border:0px solid red;margin-left:200px;">
<!--  <a href="exportcsv.php?src=primary&id=<?=$current_projectid?>">Export Primary File</a>-->
<?php
if($file2!=""){
	?>
<br/>
<!--  <a href="exportcsv.php?src=secondary&id=<?=$current_projectid?>" >Export Secondary File</a>-->
<?php } ?>
<br/>
<a href="http://ml.datascrub-152522.appspot.com/<?=$current_projectid?>" >Link to Machine Learning</a>
<!--  

<button id="init_ml">Initialize Learning</button>-->
</div>
<br/>
<?php }
require_once("templates/nav.html");
require_once("templates/footer.html");
 ?>
 <script>
    window.onload = function() {
        setTimeout(function () {
            location.reload()
        }, 20000);
     };
</script>
<?php 
/*echo "sending request";
$service_url = "http://ml.datascrub-152522.appspot.com/portal/ml/".$current_projectid;
$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
echo "curl---response=".$curl_response;
if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
}
curl_close($curl);
$decoded = json_decode($curl_response);
echo "<br>decoded===";
print_r($decoded);
/*if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
    die('error occured: ' . $decoded->response->errormessage);
}*/

/*echo 'response ok!';
//var_export($decoded->response);
 */
?>