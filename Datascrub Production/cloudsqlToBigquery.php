<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();
# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\BigQuery\BigQueryClient;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Core\ExponentialBackoff;
/*

$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}


$ml_id="";
$status="";
$sql_chk_ml_project="Select * from ax_ml where project_id=".$current_projectid;
//echo $sql_chk_ml_project;
if($res_chk_ml_project=$conn->query($sql_chk_ml_project))
{
	if($res_chk_ml_project->num_rows>0)
	{

		while($row_chk_ml_project=$res_chk_ml_project->fetch_assoc())
		{
			$ml_id=$row_chk_ml_project['id'];
			//echo $ml_id;
			$status=$row_chk_ml_project['status'];
		}
	}
}
$sql_comparison_count=0;
if($status=="Clustered")
{
$sql_fetch="select count(*) as cnt from ax_ml_".$ml_id."_comparisons";
if($res_fetch=$conn->query($sql_fetch))
{
	if($res_fetch->num_rows>0)
	{

		while($row_fetch=$res_fetch->fetch_assoc())
		{
			$sql_comparison_count=$row_fetch['cnt'];
			
		}
	}
}
}

echo "SQL COMPARISON COUNT =".$sql_comparison_count."\n";
*/

/**
 * @param string $projectId  The Google project ID.
 * @param string $datasetId  The BigQuery dataset ID.
 * @param string $tableId    The BigQuery table ID.
 * @param string $bucketName The Cloud Storage bucket Name.
 * @param string $objectName The Cloud Storage object Name.
 */
//gs://datascrub-bucket/Cloud_SQL_Export11.csv
//$projectId=$current_projectid;
//ax_ml_12958_comparisons
$projectId="datascrub-152522";
$datasetId="ax_datascrub";
$tableId="test1";
$bucketName="datascrub-bucket";
//$objectName="Cloud_SQL_Export_pid419_ax_ml_12958_comparisons.csv";
$objectName="Cloud_SQL_Export11.csv";
echo "\n".$projectId."==".$datasetId."==".$tableId."==".$bucketName."==".$objectName."\n";

//import_from_storage($projectId, $datasetId, $tableId, $bucketName, $objectName);
function import_from_storage($projectId, $datasetId, $tableId, $bucketName, $objectName)
{
	
	echo "inside function";
	
	// determine the import options from the object name
	$options = [];
	/*if ('.backup_info' === substr($objectName, -12)) {
		$options['jobConfig'] = ['sourceFormat' => 'DATASTORE_BACKUP'];
	} elseif ('.json' === substr($objectName, -5)) {
		$options['jobConfig'] = ['sourceFormat' => 'NEWLINE_DELIMITED_JSON'];
	}*/
	$options['jobConfig'] = [ 'sourceFormat' => 'CSV', 'skipLeadingRows' => 1 ];
	print_r($options);
	echo "gfgfhg".$projectId;
	// instantiate the bigquery table service
	$bigQuery = new BigQueryClient([
			'projectId' => $projectId,
	]);
	
	echo "dataset setxss";
	$dataset = $bigQuery->dataset($datasetId);
	
	echo "dataset set";
	$table = $dataset->table($tableId);
	// load the storage object
	$storage = new StorageClient([
			'projectId' => $projectId,
	]);
	echo "table  set";
	$object = $storage->bucket($bucketName)->object($objectName);
	//echo "=====ob==".$object;
	//print_r($object);
	
	// create the import job
	$job = $table->loadFromStorage($object, $options);
	echo "job set";
	// poll the job until it is complete
	$backoff = new ExponentialBackoff(10);
	$backoff->execute(function () use ($job) {
		echo 'Waiting for job to complete' . PHP_EOL;
		$job->reload();
		if (!$job->isComplete()) {
			throw new Exception('Job has not yet completed', 500);
		}
	});
		// check if the job has errors
		if (isset($job->info()['status']['errorResult'])) {
			$error = $job->info()['status']['errorResult']['message'];
			echo 'Error running job: ' . PHP_EOL." ". $error;
		} else {
			echo 'Data imported successfully' . PHP_EOL;
		}
}




/*$total_comparisons=0;
while($total_comparisons<$sql_comparison_count)
{
	$total_comparisons=$total_comparisons+10000;
}*/
/**
 * Example:
 * ```
 * $fields = [
 *     [
 *         'name' => 'field1',
 *         'type' => 'string',
 *         'mode' => 'required'
 *     ],
 *     [
 *         'name' => 'field2',
 *         'type' => 'integer'
 *     ],
 * ];
 * $schema = ['fields' => $fields];
 * create_table($projectId, $datasetId, $tableId, $schema);
 * ```
 * @param string $projectId The Google project ID.
 * @param string $datasetId The BigQuery dataset ID.
 * @param string $tableId   The BigQuery table ID.
 * @param array  $schema    The BigQuery table schema.
 */
$fields = [
		     		[
				         'name' => 'id',
				        'type' => 'integer',
				        'mode' => 'required'
				    ],
		     		[
				         'name' => 'primeid',
				         'type' => 'integer',
		     			 'mode' => 'required'
				    ],
					[
						 'name' => 'secondid',
						 'type' => 'integer',
						 'mode' => 'required'
					],
					[
						 'name' => 'result',
						 'type' => 'float'
						 
					],
		 ];
 
 
$projectId="datascrub-152522";
$datasetId="ax_datascrub";
$tableId="results1";
$schema = ['fields' => $fields];
//echo "calling create table";
//echo create_table($projectId, $datasetId, $tableId, $schema);
function create_table($projectId, $datasetId, $tableId, $schema)
{
    echo "<br>inside create table<br>";
	$bigQuery = new BigQueryClient([
        'projectId' => $projectId,
    ]);
    $dataset = $bigQuery->dataset($datasetId);
    $options = ['schema' => $schema];
    $table = $dataset->createTable($tableId, $options);
    return $table;
}


/* INSERT INTO RESULTS TABLE
 * 
#standardSQL
INSERT INTO  `datascrub-152522.ax_datascrub.results` (id,primeid,secondid,result)
SELECT  ROW_NUMBER() OVER() as id,a.primary_id,MAX(a.secondary_id) as secondid,MAX(a.matching_value) as result FROM `datascrub-152522.ax_datascrub.Cloud11` a 
INNER JOIN (SELECT primary_id, MAX(matching_value) as matching_value FROM `datascrub-152522.ax_datascrub.Cloud11` GROUP BY primary_id) b ON a.primary_id = b.primary_id
AND a.matching_value = b.matching_value  GROUP BY a.primary_id;
				
 * 
 */
/*
 * Run a BigQuery query as a job.
 * Example:
 * ```
 * $query = 'SELECT TOP(corpus, 10) as title, COUNT(*) as unique_words ' .
 *          'FROM [publicdata:samples.shakespeare]';
 * run_query_as_job($projectId, $query, true);
 * ```.
 *
 * @param string $projectId The Google project ID.
 * @param string $query     A SQL query to run. *
 * @param bool $useLegacySql Specifies whether to use BigQuery's legacy SQL
 *        syntax or standard SQL syntax for this query.
 */
echo "calling query to run";
$projectId="datascrub-152522";
$query='INSERT INTO  `datascrub-152522.ax_datascrub.results1` (id,primeid,secondid,result)
SELECT  ROW_NUMBER() OVER() as id,a.primary_id,MAX(a.secondary_id) as secondid,MAX(a.matching_value) as result FROM `datascrub-152522.ax_datascrub.Cloud11` a 
INNER JOIN (SELECT primary_id, MAX(matching_value) as matching_value FROM `datascrub-152522.ax_datascrub.Cloud11` GROUP BY primary_id) b ON a.primary_id = b.primary_id
AND a.matching_value = b.matching_value  GROUP BY a.primary_id';
echo $query."<br>";
$useLegacySql=false;
run_query_as_job($projectId, $query, $useLegacySql);
function run_query_as_job($projectId, $query, $useLegacySql)
{
	echo "inside call query useLegacySql=".$useLegacySql;
	$bigQuery = new BigQueryClient([
			'projectId' => $projectId,
	]);
	$job = $bigQuery->runQueryAsJob(
			$query,
			['jobConfig' => ['useLegacySql' => $useLegacySql]]);
	$backoff = new ExponentialBackoff(10);
	$backoff->execute(function () use ($job) {
		print('Waiting for job to complete' . PHP_EOL);
		$job->reload();
		if (!$job->isComplete()) {
			throw new Exception('Job has not yet completed', 500);
		}
	});
		$queryResults = $job->queryResults();

		if ($queryResults->isComplete()) {
			$i = 0;
			$rows = $queryResults->rows();
			foreach ($rows as $row) {
				printf('--- Row %s ---' . PHP_EOL, ++$i);
				foreach ($row as $column => $value) {
					printf('%s: %s' . PHP_EOL, $column, $value);
				}
			}
			printf('Found %s row(s)' . PHP_EOL, $i);
		} else {
			throw new Exception('The query failed to complete');
		}
}




