<?php 
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("functions.php");
checkSession();

$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
	$current_projectid==$_SESSION['project_id'];
	$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}
//echo "===getid=".$current_projectid;





require_once("config/dbconnect.php");
$uploadurl="/project/uploadfiles/".$current_projectid;
require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
use google\appengine\api\cloud_storage\CloudStorageTools;
$options = [ 'gs_bucket_name' => 'datascrub-152522.appspot.com' ];
$upload_url = CloudStorageTools::createUploadUrl($uploadurl,$options);
//$_SESSION['userid']=5;
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}


//$current_projectid=245;
//$current_projectname='my project';
$filedata1=array();
$filedata2=array();
$sql_file1="select filename as name,size from ax_files where id=(select file_1 from ax_project_files where project_id=".$current_projectid.")";
//echo $sql_file1;

if($res_file1 = $conn->query($sql_file1))
{
$file1=$res_file1->fetch_assoc();
}
if(count($file1)>0)
$filedata1[]=$file1;

$sql_file2="select filename as name,size from ax_files where id=(select file_2 from ax_project_files where project_id=".$current_projectid.")";
//echo $sql;
if($res_file2 = $conn->query($sql_file2))
{
	$file2=$res_file2->fetch_assoc();
}
if(count($file2)>0)
$filedata2[]=$file2;

//print_r($filedata1);
//print_r($filedata2);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Datascrub.io</title>
    <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/dropzone.css">
    <link media="all" type="text/css" rel="stylesheet" href="/font-awesome/css/font-awesome.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/theme.default.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/chosen/chosen.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/smoothness/jquery-ui.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/smoothness/theme.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/theme.css">

    <script src="/js/jquery-1.10.2.js"></script>
    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.entwine-dist.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.tablesorter.js"></script>
    <script src="/js/jquery.tablesorter.widgets.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/ui-config.js"></script>
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/jquery-validate-additional-methods.js"></script>
    <script src="/js/blockUI.js"></script>
    <script src="/js/chosen.jquery.js"></script>
    <script src="/js/functions.js"></script>
    <script src="/js/default.js"></script>
    <script src="/js/projectlist.js"></script>
    
</head>
<body>
<div id="container">
<div id="uploadUrl11" style="display:none;"><?=$upload_url?></div>

    <nav class="navbar navbar-default">
    <div class="container container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/images/datascrublogo200.png" alt="Datascrub"/></a>
        </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                
                    <li><a href="/projects">Projects</a></li>
                                    <li><a href="http://datascrub/companies">Companies</a></li>
                    <li><a href="http://datascrub/users">Users</a></li>
                
                    <li><a href="/logout">Logout</a></li>
                            </ul>
            <!--<div class="pull-right" style="text-align: right;">
                <h6>WELCOME <i class="fa fa-user"></i> Supriya Uppal</h6>
                <h6>You are logged in as Admin user. <a href="http://datascrub/auth/logout">Logout</a></h6>
            </div>-->
         </div>
    </div>
</nav>
    <div class="container">
        <div class="row">
            <div class="content_wrapper">
                <!-- Content -->
                <div class="container" id="content">
    <div class="row">
        <div class="col-md-12 border-btm">
          <h3><i class="fa fa-file"></i> <?php echo $current_projectname;?></h3>
          
         
          
          
          
          
          
                <div id="crumbs" class="col-md-12">
                <ul>
                	<li><a class="not">File Upload</a></li>
                    <li><a href="/mapfields/<?=$current_projectid?>">Field Select</a></li>
                    <li><a href="/export/<?=$current_projectid?>">Initialize Learning</a></li>
                    <li><a href="/results/<?=$current_projectid?>">Export Results</a></li>
                    
               </ul>
            </div>
        </div>
    </div>
    <div id="upload_files" class="row">
        <div class="col-md-10">
            <h2>Step 1: Upload Your Files</h2>
        </div>
        <div class="col-md-2 step-btn">
                            <a class="submitbtn ecm-upload-files btn btn-primary" id="<?php echo $current_projectid;?>" role="button" href="/mapfields/<?=$current_projectid;?>">Continue</a>
                        <span data-help="&lt;p&gt;Files must be in CSV format.  You must have a header row with column names.&lt;/p&gt;
&lt;p&gt;Upload the file you would like to analyse in the left most box.&lt;/p&gt;
&lt;p&gt;If you have a second file to compare it against, upload it to the right.&lt;/p&gt;
&lt;p&gt;The columns in the second file DO NOT have to match the first.&lt;/p&gt;
&lt;p&gt;The progress will be shown in a minimized window to the bottom left.  Please be patient and do not close your browser window while the upload is happening.&lt;/p&gt;
&lt;p&gt;Once the files are loaded the Continue button will show up.&lt;/p&gt;
&lt;p&gt;You can re-upload files later if you need to.&lt;/p&gt;
" class="question">?</span>
        </div>
    </div>

    
    

    <div class="row">
        <div class="col-md-12 alert alert-danger fade" id="uploadfileAlert" style="display: none;">
            <a href="#" class="close">&times;</a>
            <strong>Error!</strong> Please upload a base file for comparison.
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 uploadfiles-form">
            <input type="hidden" name="_token" value="sVOvhxjdnDcYGwBx5c3TwpfYqdE3uCQ0eXNMb5Qj" />
            <input type="hidden" id="project_id" name="project_id" value="<?php echo $current_projectid;?>"/>
			
            <div class="col-md-12">
                <div class="col-md-5 col-md-offset-1"><h5>File For Analysis</h5></div>
                <div class="col-md-5 col-md-offset-1"><h5>Reference File</h5></div>
                <div id="my-dropzone1" class="col-md-5 col-md-offset-1 dropzone">

                    <!--<div class="col-md-12 padbtm10">
                        <label for="usr-fn">Add File</label>
                        <input tabindex="1" type="file" name="file_1" />
                    </div>-->

                    <form id="fallback_form1" class="fallback"  >
                        <div class="dz-fallback">
                            <p>Please use the fallback form below to upload your files like in the olden days.</p>
                            <input type="hidden" name="_token" value="sVOvhxjdnDcYGwBx5c3TwpfYqdE3uCQ0eXNMb5Qj" />
                            <input type="hidden" id="project_id" name="project_id" value="<?php echo $current_projectid;?>"/>
                            <?php if(isset($file1)){ ?>
                                <div id="old_file1">Old File: <?php echo $file1['name'] ?></div>
                            <?php } ?>
                            
                                                        <div class="col-md-8"><input type="file" undefined="" name="file_1"></div>
                            <div class="col-md-4"><a class="btn btn-primary" id="fallback_upload1">Upload</a></div>
                        </div>
                    </form>

                </div>

                <div id="my-dropzone2" class="col-md-5 col-md-offset-1 dropzone">
                    <!--<div class="col-md-12 padbtm10">
                        <label for="usr-fn">Add File</label>
                        <input tabindex="2" type="file" name="file_2" />
                    </div>-->
                    <form id="fallback_form2" class="fallback"  >
                        <div class="dz-fallback">
                            <p>Please use the fallback form below to upload your files like in the olden days.</p>
                            <input type="hidden" name="_token" value="sVOvhxjdnDcYGwBx5c3TwpfYqdE3uCQ0eXNMb5Qj" />
                            <input type="hidden" id="project_id" name="project_id" value="<?php echo $current_projectid;?>"/>
                            <?php if(isset($file2)){ ?>
                                <div id="old_file2">Old File: <?php echo $file2['name'] ?></div>
                            <?php } ?>
                                                        <div class="col-md-8"><input type="file" undefined="" name="file_2"></div>
                            <div class="col-md-4"><a class="btn btn-primary" id="fallback_upload2">Upload</a></div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12" style="text-align:right;">
                    <a class="submitbtn ecm-upload-files btn btn-primary" id="<?php echo $current_projectid;?>" role="button" href="/mapfields/<?=$current_projectid;?>" >Continue</a>
            </div>

</div>
<div id="preview-template" style="display: none;">

    <div id="test" class="dz-preview dz-file-preview">
        <div class="dz-image">
            <img data-dz-thumbnail />
        </div>
        <div class="dz-details">
            <div class="dz-filename col-md-7"><span data-dz-name></span></div>
            <div class="dz-size col-md-5">(<span data-dz-size></span>)</div>
        </div>
        <div class="edit">
           <span class="edit_click edit_drop1"><label>Edit</label> <i class="fa fa-caret-down"></i></span>
            <ul class="edit_options" style="display:none;">
                <li data-dz-remove>Delete</li>
                <li class="dz-clickable" id="replace1">Replace</li>
            </ul>
        </div>
        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
        <div class="dz-error-message"><span data-dz-errormessage></span></div>
        <div class="dz-success-mark"><i class="fa fa-check"></i></div>
        <div class="dz-error-mark"><i class="fa fa-close"></i></div>

    </div>
   <!-- <select id="edit1" name="editfile" class="chosen-select">
        <option value="">Edit</option>
        <option value="delete" data-dz-remove>Delete</option>
        <option id="replace1" value="replace" >Replace</option>
    </select>-->
</div>
<script>

    // This example uses jQuery so it creates the Dropzone, only when the DOM has
    // loaded.

    // Disabling autoDiscover, otherwise Dropzone will try to attach twice.
    Dropzone.autoDiscover = false;
    // or disable for specific dropzone:
    // Dropzone.options.myDropzone = false;

    $(function() { //$(".chosen-select").chosen({width: "100%"});

        //$('.edit_click').hide();
        $('.dz-progress').hide();

        if($('#old_file1').length > 0){
            $('.ecm-upload-files').removeClass('active');
            $('.ecm-upload-files').addClass('proceed');
        }

        $('.ecm-upload-files.proceed').entwine({
            onclick: function(){
                if($(this).hasClass('viewResults')){
                    var thismodal = $('#ResultsModal').modal('show');
                    thismodal.find('#proceed_results').attr('href', $(this).attr('data-proceed'));
                    thismodal.find('#rerun_analysis').attr('data-projid', $(this).attr('data-projectid'));
                }else{
                    
                    window.location='<?php echo $upload_url;?>'+$(this).attr('id');
                }
            }
        });

        // Now that the DOM is fully loaded, create the dropzone, and setup the
        // event listeners
        var removeicon = "<i title='Remove' class='fa fa-times-circle remove'></i>";
        var myDropzone = new Dropzone("#my-dropzone1", {
            paramName: "file_1",
            uploadMultiple: false,
            acceptedFiles: ".csv",
            parallelUploads: 1,
            url: "<?php echo $upload_url;?>",
            maxFiles: 1,
            thumbnailWidth: 250,
            previewTemplate: $('#preview-template').html(),
            //forceFallback: true,
            //clickable: '#replace1', // Define the element that should be used as click trigger to select files.
            /*addRemoveLinks: true,
            dictRemoveFile: removeicon,
            dictCancelUpload: removeicon,*/
            dictDefaultMessage: "Drop File Here or <span class='clickable'>Upload</span>",
            dictCancelUploadConfirmation: "Are you sure you want to cancel this file?",
            //clickable: $('#clickable1'),
            //autoProcessQueue: false,
            sending: function(file, xhr, formData) {
                // Pass token. You can use the same method to pass any other values as well such as a id to associate the image with for example.
                formData.append("_token", $('[name=_token').val()); // Laravel expect the token post value to be named _token by default
                formData.append("project_id", $('[name=project_id').val());
            },
            init: function(){

                console.log(this);
                var thisDropzone = this;

                $('#my-dropzone1 .edit_drop1').entwine({
                    onclick: function(){ //console.log('clicked');
                        $(this).next('.edit_options').toggle();
                    }
                });

                $('#my-dropzone1 #replace1').entwine({
                    onclick: function(){
                        /*thisismyDropzone = $(this).closest('#my-dropzone1');
                        console.log(thisismyDropzone);*/
                        console.log(thisDropzone);
                        thisDropzone.hiddenFileInput.click();
                    }
                });


                var data = <?php echo json_encode($filedata1) ?>;
               //alert(data.length+"----"+data);
                if(data.length > 0){ //console.log('here');
                    $('.ecm-upload-files').addClass('active');
                }

                $.each(data, function(key,value){
                    var mockFile = { name: value.name, size: value.size };
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.files.push(mockFile); // here you add them into the files array
                    //thisDropzone.emit("addedfile", mockFile);
                });

                thisDropzone.on("addedfile", function(file) { console.log('added');
                    $('.total_processing_files').removeClass('inactive');

                    $('.notif').addClass('active');
                    $('#proj_link').html('');
                    $('.notif_title_text').html('Processing Files');

                    $('#project_name').html('Project: '+file.name);

                    if($('#process_file1').length > 0){
                        $('#process_file1').html(file.name+' ('+file.size+'b)');
                    }else{
                        $('#processing_files').append('<li id="process_file1">'+file.name+' ('+file.size+'b)</li>');
                    }

                    $('.total_num').html($("#processing_files li").length);

                    /*if($('.total_num').html() == ''){
                        $('.total_num').html('1');
                    }else{
                        $('.total_num').html(parseInt($('.total_num').html(),10)+1);
                    }*/

                    //$('#my-dropzone1 .edit_click').show();

                    $('#my-dropzone1 .dz-progress').show();



                    if (this.files[1]!=null){
                        this.removeFile(this.files[0]);
                    }
                });

                thisDropzone.on("queuecomplete", function() {
                    //alert('here');
                });

                thisDropzone.on("error", function() {
                    $('#my-dropzone1 .edit').addClass('error');
                    $('#my-dropzone1 .edit_click label').html('Fix Error');
                });

                thisDropzone.on("reset", function(file) {
                    //console.log(thisDropzone.files.length);
                    $('#proj_link').html('');
                    $('.ecm-upload-files').removeClass('active');
                });

                thisDropzone.on("removedfile", function(file) {
                    //console.log(thisDropzone.files.length);
                    //console.log(thisDropzone.getUploadingFiles().length);
                    if(thisDropzone.files.length < 1){ //console.log('less');
                        $('#proj_link').html('');
                        $('.ecm-upload-files').removeClass('active');
                        $('#processing_files li#process_file1').remove();
                    }else{
                        if($('#processing_files li#process_file1').length > 0){
                            $('#proj_link').html('');
                            //$('#processing_files').append('<li id="process_file1">'+file.name+' ('+file.size+'b)</li>');
                            //$('#processing_files li#process_file1').remove();
                            /*var totalfiles = parseInt($('.total_num').html(),10);
                            if(totalfiles > 2){
                                $('.total_num').html(totalfiles-1);
                            }*/
                        }
                    }

                    $('.total_num').html($("#processing_files li").length);

                });
            }
        });
//alert("going toropzone2");
        var myDropzone2 = new Dropzone("#my-dropzone2", {
            paramName: "file_2",
            uploadMultiple: false,
            acceptedFiles: ".csv",
            parallelUploads: 1,
            url: "<?php echo $upload_url;?>",
            maxFiles: 1,
            thumbnailWidth: 250,
            previewTemplate: $('#preview-template').html(),
            //forceFallback: true,
            /*addRemoveLinks: true,
            dictRemoveFile: removeicon,
            dictCancelUpload: removeicon,*/
            dictDefaultMessage: "<em>(Optional)</em> Drop File Here or <span class='clickable'>Upload</span>",
            //autoProcessQueue: false,
            sending: function(file, xhr, formData) {
                // Pass token. You can use the same method to pass any other values as well such as a id to associate the image with for example.
                formData.append("_token", $('[name=_token').val()); // Laravel expect the token post value to be named _token by default
                formData.append("project_id", $('[name=project_id').val());
            },
            init: function(){
                var thisDropzone = this;

                $('#my-dropzone2 .edit_click').entwine({
                    onclick: function(){
                        $(this).next('.edit_options').toggle();
                    }
                });

                $('#my-dropzone2 #replace1').entwine({
                    onclick: function(){
                        thisDropzone.hiddenFileInput.click();
                    }
                });

                var data = <?php echo json_encode($filedata2) ?>;
                //alert(data.length);
                //console.log(data);

                $.each(data, function(key,value){
                    var mockFile = { name: value.name, size: value.size };
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.files.push(mockFile); // here you add them into the files array
                });

                thisDropzone.on("addedfile", function(file) {
                    $('.total_processing_files').removeClass('inactive');
                    $('.notif').addClass('active');
                    $('#proj_link').html('');
                    $('.notif_title_text').html('Processing Files');

                    $('#project_name').html('Project: '+file.name);

                    if($('#process_file2').length > 0){
                        $('#process_file2').html(file.name+' ('+file.size+'b)');
                    }else{
                        $('#processing_files').append('<li id="process_file2">'+file.name+' ('+file.size+'b)</li>');
                    }

                    $('.total_num').html($("#processing_files li").length);
                    /*if($('.total_num').html() == ''){
                        $('.total_num').html('1');
                    }else{
                        $('.total_num').html(parseInt($('.total_num').html(),10)+1);
                    }*/

                    $('#my-dropzone2 .edit_click').show();
                    $('#my-dropzone2 .edit_click').addClass('edit_drop2');


                    if (this.files[1]!=null){
                        this.removeFile(this.files[0]);
                    }
                });

                thisDropzone.on("removedfile", function(file) {
                    if(thisDropzone.files.length < 1){
                        $('#processing_files li#process_file2').remove();
                    }

                    $('.total_num').html($("#processing_files li").length);
                    //console.log(file);
                });
            }
        });

        myDropzone.on("dragover", function(event) {
            $('#my-dropzone1').css('background', '#A6C6DB');
        });
        myDropzone2.on("dragover", function(event) {
            $('#my-dropzone2').css('background', '#A6C6DB');
        });
        myDropzone.on("dragleave", function(event) {
            $('.dropzone').css('background', '');
        });
        myDropzone2.on("dragleave", function(event) {
            $('.dropzone').css('background', '');
        });
        myDropzone.on("drop", function(event) {
            $('.dropzone').css('background', '');
        });
        myDropzone2.on("drop", function(event) {
            $('.dropzone').css('background', '');
        });

        /*document.querySelector(".ecm-upload-files").addEventListener("click", function(e) {
        });*/

        /*$('.uploadfiles-form').entwine({
            validateForm: function(elemobj){
                var dis = this;
                //var data = {"_token":elemobj.attr('data-csrftoken')};
                this.validate({
                    submitHandler: function(){
                        dis.ajaxSubmit({
                            url: '/project/uploadfiles.php',
                            method: 'post',
                            type: 'json',
                            //data: data,
                            beforeSend: function() {
                                //$.showLoader();
                            },
                            success: function(data){
                                //console.log(data);
                                if(data != 'error'){
                                    //alert('Please select files to upload');
                                    //location.reload();
                                    window.location='/project/uploadfiles.php/'+data;
                                }else{
                                    $('#uploadfileAlert').addClass('in');
                                    $('#uploadfileAlert').show();
                                }
                                //$.unblockUI();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                                alert(errorThrown);
                            }
                        });
                    }
                }); //end validate
            }
        });*/

        myDropzone.on("success", function(file, response) {
            //alert('ok');
            if(myDropzone2.getUploadingFiles().length > 0){
                myDropzone2.on("success", function(file, response) {
                    //alert('ok');
                    $('.ecm-upload-files').addClass('active');
                    $('.notif_title_text').html('Processing Files Complete');
                    $('#processing_files').html('');
                    $('.total_processing_files span').html('');
                    $('.total_processing_files').addClass('inactive');
                    //$('#proj_link').html('<a href="/project/uploadfiles.php/'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                    $('#proj_link').html('<a href="<?php echo $upload_url;?>'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                    
                });
            }else{
                $('.ecm-upload-files').addClass('active');
                $('.notif_title_text').html('Processing Files Complete');
                $('#processing_files').html('');
                $('.total_processing_files span').html('');
                $('.total_processing_files').addClass('inactive');
                //$('#proj_link').html('<a href="/project/uploadfiles.php/'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                $('#proj_link').html('<a href="<?php echo $upload_url;?>'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                
            }
        });

        myDropzone2.on("success", function(file, response) { //console.log(myDropzone.getUploadingFiles().length); console.log(myDropzone.files.length);
            if(myDropzone.getUploadingFiles().length < 1 && myDropzone.files.length > 0){
                $('.ecm-upload-files').addClass('active');
                $('.notif_title_text').html('Processing Files Complete');
                $('#processing_files').html('');
                $('.total_processing_files span').html('');
                $('.total_processing_files').addClass('inactive');
                //$('#proj_link').html('<a href="/project/uploadfiles.php/'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                $('#proj_link').html('<a href="<?php echo $upload_url;?>'+$('.ecm-upload-files').attr('id')+'">Confirm Import</a>');
                
            }
        });

        $('.ecm-upload-files.active').entwine({
            onclick: function(){
                $.showLoader();

                if($(this).hasClass('viewResults')){
                    var thismodal = $('#ResultsModal').modal('show');
                    thismodal.find('#proceed_results').attr('href', $(this).attr('data-proceed'));
                    thismodal.find('#rerun_analysis').attr('data-projid', $(this).attr('data-projectid'));
                }else{

                    if((myDropzone.getQueuedFiles().length > 0) && (myDropzone2.getQueuedFiles().length <= 0)){
                        myDropzone.processQueue();
                        myDropzone.on("success", function(file, response) {
                            window.location='<?php echo $upload_url;?>'+response;
                            //console.log('file1');
                        });

                    }else if((myDropzone.getQueuedFiles().length > 0) && (myDropzone2.getQueuedFiles().length > 0)){
                        myDropzone.processQueue();
                        myDropzone2.processQueue();
                        myDropzone.on("success", function(file, response) {
                            if(myDropzone2.getUploadingFiles().length > 0){
                                myDropzone2.on("success", function(file, response) {
                                    window.location='<?php echo $upload_url;?>'+response;
                                    //console.log('file2');
                                });
                            }else{
                                window.location='<?php echo $upload_url;?>'+response;
                                //console.log('still file1');
                            }
                        });
                    }else{
                        //console.log(myDropzone.files.length);
                        //console.log(myDropzone2.files.length)

                        if(myDropzone.files.length > 0 && myDropzone2.getQueuedFiles().length > 0){

                            myDropzone2.processQueue();
                            myDropzone2.on("success", function(file, response) {
                                window.location='<?php echo $upload_url;?>'+response;
                                //console.log('file2');
                            });

                            //console.log('old file1 and upload file2');
                        }else if(myDropzone.files.length > 0 && myDropzone2.files.length > 0){
                            window.location='<?php echo $upload_url;?>'+$(this).attr('id');
                            // console.log('old file1 and old file2');
                        }else if(myDropzone.files.length > 0 && myDropzone2.getQueuedFiles().length <= 0){
                            window.location='<?php echo $upload_url;?>'+$(this).attr('id');
                            //console.log('old file1 and no file2');
                        }else{
                            //console.log('form submit');
                            /*var form = $('.uploadfiles-form');
                             var elemobj = this;
                             form.validateForm(elemobj);
                             form.submit();*/
                        }
                    }
                    $.unblockUI();
                }
            }
        });

    })
</script>
            </div>
        </div>

        <div class="modal fade bs-example-modal-sm in" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="btn btn-primary btn-sm uibtn-delete" data-options=""><i class="fa fa-save"></i>&nbsp;Yes</a>
                                <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" role="dialog" aria-labelledby="ResultsModalLabel" aria-hidden="true" id="ResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>This project has already been run. Do you wish to view the results or re-run the analysis?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="proceed_results" href="#" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-file-text-o"></i>&nbsp;View Current Results</a>
                        <a id="rerun_analysis" href="#" class="btn btn-default btn-sm" data-projid=""><i class="fa fa-repeat"></i>&nbsp;Re-Run Analysis</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" role="dialog" aria-labelledby="RerunResultsModalLabel" aria-hidden="true" id="RerunResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>Would you like to use the same fields as previously selected or select new fields for comparison?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="same_fields" href="#" data-csrftoken="sVOvhxjdnDcYGwBx5c3TwpfYqdE3uCQ0eXNMb5Qj" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-check-square-o"></i>&nbsp;Use Same Fields</a>
                        <a id="new_fields" href="#" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i>&nbsp;Select New Fields</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
    <!-- HIDDEN / POP-UP DIV -->
    <div class="modal fade" role="dialog" aria-labelledby="HelpModalLabel" aria-hidden="true" id="HelpModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel"><span class="question">?</span> Help</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div id="popup" class="col-md-12 col-sm-12 padbtm15"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p style="float:right;font-size:10px;">Please contact us for more assistance.</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="push"></div>
</div>

<div id="upload_notif" class="notif">
    <div id="notif_content" style="display: none;">
        <div id="box_title_top"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-down"></i></div>
        <div id="project_data_content">
            <div id="project_name_link"><span id="project_name"></span><span style="float:right; margin-right:10px;" id="proj_link"></span></div>
            <ul id="processing_files"></ul>
        </div>
    </div>
    <div id="box_title_btm"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-up"></i></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="centered">Copyright &copy; 2015 - All Rights Reserved. <a href="http://datascrub.io/">DataScrub.io</a></p>
            </div>
        </div>
   </div>
</footer>
</body>
</html>