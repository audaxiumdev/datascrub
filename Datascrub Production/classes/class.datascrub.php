<?php

//require_once("../../config/dbconnect.php");
//CREATE ANALYSIS TABLE
class datascrub {
	
	public $conn;
	
public function __construct($conn)
    {
        $this->conn=$conn;

    }
	
public function CreateAnalysisTables($projectid,$secondfile)
{
	$conn=$this->conn;
	$includes=array();
	//Check Includes and datatypes
	$sql="SELECT * from ax_job_".$projectid."_process";
	//echo $sql;
	
$result = $conn->query($sql);
//echo "hhh";
while($row = $result->fetch_assoc())
{
	//echo "===".$row['colid']."===".$row['type'];
	$includes[]=$row;
}
	
	//CREATE ANALYSIS TABLE
	$dropsql="DROP TABLE IF EXISTS `ax_job_".$projectid."_analysis_primary`;";
	$conn->query($dropsql);
	$sql_prim="CREATE TABLE `ax_job_".$projectid."_analysis_primary` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`primeid` int(11) ,
		";
	if($secondfile){
		$dropsql="DROP TABLE IF EXISTS `ax_job_".$projectid."_analysis_secondary`;";
		$conn->query($dropsql);

		$sql_sec="CREATE TABLE `ax_job_".$projectid."_analysis_secondary` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`secondid` int(11) ,";
	}
	$addindexprim=[];
	$addindexsec=[];
	$addindexprim[0]="ALTER TABLE `ax_job_".$projectid."_analysis_primary` ADD FULLTEXT INDEX `prim_mashfield_index` (`prim_mashfield` ASC);";
	if($secondfile)
	{
		$addindexsec[0]="ALTER TABLE `ax_job_".$projectid."_analysis_secondary` ADD FULLTEXT INDEX `sec_mashfield_index` (`sec_mashfield` ASC);";
	}
	$i=1;
	$j=1;
	/*foreach($includesall as $includes){
		echo "----------";
		print_r($includes);*/
	foreach($includes as $inc){
		//echo "===========";
		//print_r($inc);
		//echo "==colid=".$inc['colid'];
		if($secondfile){
			$file1colid=$inc['colid'];
			$file2colid=$inc['col2id'];
		}else{
			$file1colid=$inc['colid'];
			$file2colid=$inc['colid'];
		}
		$file1column = 'col_'.$file1colid;
		$file2column = 'col_'.$file2colid;
			
		$col_prim=[];
		$col_sec=[];
		if($inc['type']==0)
		{
				

			$col_prim[]="prim_text_col_".$file1colid;
			//$col_prim[]="prim_text_wordcount_col_".$file1colid;
			if($secondfile){
				$col_sec[]="sec_text_col_".$file2colid;
				//$col_sec[]="sec_text_wordcount_col_".$file2colid;
			}
		}
		elseif($inc['type']==1)
		{
			$col_prim[]="prim_number_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_number_col_".$file2colid;
			}
		}
		elseif($inc['type']==2)
		{
			$col_prim[]="prim_email_col_".$file1colid;
			$col_prim[]="prim_email_account_col_".$file1colid;
			$col_prim[]="prim_email_domain_col_".$file1colid;
			$col_prim[]="prim_email_isfreeemail_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_email_col_".$file2colid;
				$col_sec[]="sec_email_account_col_".$file2colid;
				$col_sec[]="sec_email_domain_col_".$file2colid;
				$col_sec[]="sec_email_isfreeemail_col_".$file2colid;
			}
		}
		elseif($inc['type']==3)
		{
			$col_prim[]="prim_postal_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_postal_col_".$file2colid;
			}
		}
		elseif($inc['type']==4)
		{
			$col_prim[]="prim_phone_col_".$file1colid;
			//$col_prim[]="prim_phone_countrycode_col_".$file1colid;
			//$col_prim[]="prim_phone_areacode_col_".$file1colid;
			$col_prim[]="prim_phone_no_col_".$file1colid;
			$col_prim[]="prim_phone_ext_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_phone_col_".$file2colid;
				//$col_sec[]="sec_phone_countrycode_col_".$file2colid;
				//$col_sec[]="sec_phone_areacode_col_".$file2colid;
				$col_sec[]="sec_phone_no_col_".$file2colid;
				$col_sec[]="sec_phone_ext_col_".$file2colid;
			}
		}
		elseif($inc['type']==5)
		{
			$col_prim[]="prim_company_col_".$file1colid;
			$col_prim[]="prim_company_withoutsuffix_col_".$file1colid;
			$col_prim[]="prim_company_abbr_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_company_col_".$file2colid;
				$col_sec[]="sec_company_withoutsuffix_col_".$file2colid;
				$col_sec[]="sec_company_abbr_col_".$file2colid;
			}
		}
		elseif($inc['type']==6)
		{
			$col_prim[]="prim_address_col_".$file1colid;
			//$col_prim[]="prim_address_no_col_".$file1colid;
			//$col_prim[]="prim_address_streetname_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_address_col_".$file2colid;
				//$col_sec[]="sec_address_no_col_".$file2colid;
				//$col_sec[]="sec_address_streetname_col_".$file2colid;
			}
		}
		elseif($inc['type']==7)
		{
			$col_prim[]="prim_firstname_col_".$file1colid;
			//$col_prim[]="prim_firstname_initial_col_".$file1colid;
			//$col_prim[]="prim_firstname_startswith_col_".$file1colid;
			//$col_prim[]="prim_firstname_wordcount_col_".$file1colid;
			$col_prim[]="prim_firstname_soundslike_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_firstname_col_".$file2colid;
				//$col_sec[]="sec_firstname_initial_col_".$file2colid;
				//$col_sec[]="sec_firstname_startswith_col_".$file2colid;
				//$col_sec[]="sec_firstname_wordcount_col_".$file2colid;
				$col_sec[]="sec_firstname_soundslike_col_".$file2colid;
			}

		}
		elseif($inc['type']==8)
		{
			$col_prim[]="prim_lastname_col_".$file1colid;
			//$col_prim[]="prim_lastname_initial_col_".$file1colid;
			//$col_prim[]="prim_lastname_startswith_col_".$file1colid;
			//$col_prim[]="prim_lastname_wordcount_col_".$file1colid;
			$col_prim[]="prim_lastname_soundslike_col_".$file1colid;

			if($secondfile){
				$col_sec[]="sec_lastname_col_".$file2colid;
				//$col_sec[]="sec_lastname_initial_col_".$file2colid;
				//$col_sec[]="sec_lastname_startswith_col_".$file2colid;
				//$col_sec[]="sec_lastname_wordcount_col_".$file2colid;
				$col_sec[]="sec_lastname_soundslike_col_".$file2colid;
			}
		}
		elseif($inc['type']==9)
		{
			$col_prim[]="prim_city_col_".$file1colid;
				
			if($secondfile){
				$col_sec[]="sec_city_col_".$file2colid;
			}
		}
		elseif($inc['type']==10)
		{
			$col_prim[]="prim_state_col_".$file1colid;
				
			if($secondfile){
				$col_sec[]="sec_state_col_".$file2colid;
			}
		}
		elseif($inc['type']==11)
		{
			$col_prim[]="prim_country_col_".$file1colid;
				
			if($secondfile){
				$col_sec[]="sec_country_col_".$file2colid;
			}
		}
		elseif($inc['type']==12)
		{
			$col_prim[]="prim_webaddress_col_".$file1colid;
			$col_prim[]="prim_webaddress_domain_col_".$file1colid;
			$col_prim[]="prim_webaddress_subdomain_col_".$file1colid;
				
			if($secondfile){
				$col_sec[]="sec_webaddress_col_".$file2colid;
				$col_sec[]="sec_webaddress_domain_col_".$file1colid;
				$col_sec[]="sec_webaddress_subdomain_col_".$file1colid;
			}
		}
		elseif($inc['type']==13)
		{
			$col_prim[]="prim_date_col_".$file1colid;
			$col_prim[]="prim_date_db_col_".$file1colid;
			$col_prim[]="prim_date_year_col_".$file1colid;
			$col_prim[]="prim_date_month_col_".$file1colid;
			$col_prim[]="prim_date_day_col_".$file1colid;
			$col_prim[]="prim_date_time_col_".$file1colid;
				
			if($secondfile){
				$col_sec[]="sec_date_col_".$file2colid;
				$col_sec[]="sec_date_db_col_".$file2colid;
				$col_sec[]="sec_date_year_col_".$file1colid;
				$col_sec[]="sec_date_month_col_".$file1colid;
				$col_sec[]="sec_date_day_col_".$file1colid;
				$col_sec[]="sec_date_time_col_".$file1colid;
			}
		}
			
			
			
		foreach($col_prim as $c)
		{
			$sql_prim .= $c.' VARCHAR(150) , ';
			$addindexprim[$i]="ALTER TABLE `ax_job_".$projectid."_analysis_primary` ADD FULLTEXT INDEX `col_".$c."_index` (`".$c."` ASC);";
			$i++;
		}
		foreach($col_sec as $c)
		{
			$sql_sec .= $c.' VARCHAR(150) , ';
			$addindexsec[$j]="ALTER TABLE `ax_job_".$projectid."_analysis_secondary` ADD FULLTEXT INDEX `col_".$c."_index` (`".$c."` ASC);";
			$j++;
		}
			
	}
	//}
	$sql_prim.='prim_mashfield TEXT , ';
	$sql_prim.='PRIMARY KEY (`id`),';
	$sql_prim.='UNIQUE KEY (`primeid`)
		) DEFAULT CHARSET=utf8;';
	//log::info($sql_prim);
	if($conn->query($sql_prim))
	{
		
	}
	else {
		syslog( LOG_ERR , "queryerror=".$conn->error );
		//exit;
	}
	if($secondfile){
		$sql_sec.='sec_mashfield TEXT , ';
		$sql_sec.='PRIMARY KEY (`id`),';
		$sql_sec.='UNIQUE KEY (`secondid`)
		)  DEFAULT CHARSET=utf8;';
		//log::info($sql_sec);
		if($conn->query($sql_sec))
		{
			
		}
		else {
			echo "queryerror=".$conn->error;
			syslog( LOG_ERR , "queryerror=".$conn->error );
			//exit;
		}
	}
	

	foreach($addindexprim as $ft){
		$conn->query($ft);
		//echo $ft."<br>";
	}
	foreach($addindexsec as $ft){
		$conn->query($ft);
		//echo $ft."<br>";
	}


}
public function InsertIntoAnalysisTables($projectid,$secondfile,$colist,$co2list)
{
	$this->InsertIntoAnalysisTablePrimary($projectid,$secondfile,$colist,$co2list);
	if($secondfile)
	{
		$this->InsertIntoAnalysisTableSecondary($projectid,$secondfile,$colist,$co2list);
	}
}

public function InsertIntoAnalysisTablePrimary($projectid,$secondfile,$colist,$co2list)
{
	$conn=$this->conn;
	//log::info("colist==".$colist);
	$colist1=substr($colist,0,strlen($colist)-1);
	$colprimarray=explode(",",$colist1);
	$col_name_type=[];
	$i=0;
	
	foreach($colprimarray as $c)
	{
		$column=str_replace("col_","",$c);
		$column=str_replace("`","",$column);
		$sql="SELECT type from `ax_job_".$projectid."_process` WHERE colid=".$column;
		//echo $sql;
		$result=$conn->query($sql);
		while($type= $result->fetch_assoc())
		{
		
			//print_r($type);
		//echo $sql."====type==".$type['type'];
		//Log::info("TYPE OF ".$c."=".$type[0]->type);
		$col_name_type[$i]['name']=str_replace("`","",$c);
		$col_name_type[$i]['type']=$type['type'];
		$i++;
		}
	}

	//print_r($col_name_type);
	/*	foreach($col_name_type as $col)
		{
			
		foreach($col as $k=>$c)
		{
		LOg::info($k."===".$c);
		}
		}*/
	$primarys=[];
	$primary_table = 'ax_job_'.$projectid.'_primary';
	$primarys_result = $conn->query("SELECT $colist id,MASHFIELD FROM $primary_table");
	while($row_primarys= $primarys_result->fetch_assoc())
	{
		$primarys[]=$row_primarys;
	
	}
	$col_name_values=[];
	$j=0;
	$result=$conn->query($sql);
	
	foreach($primarys as $prim)
	{
		$primeid=$prim['id'];
		foreach($col_name_type as $col)
		{
			$type="";
			$name="";
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);
				if($k=="type")
				{
					$type=$c;
				}
				elseif($k=="name")
				{
					$name=$c;
				}

			}
			//log::info($type."====".$name);
			//log::info($prim[$name]);

			if($type==0)
			{
					
					
				$columnname="prim_text_".$name;
				//$$columnname=$prim[$name];
				//Log::info($columnname."=========".$$columnname);
				$cleanval=$this->cleanField($prim[$name],'text');
					
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
				/*$columnname="prim_text_wordcount_".$name;
				 //$$columnname=str_word_count($prim[$name]);
				 //Log::info($columnname."=========".$$columnname);
				 	
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
			}
			elseif($type==1)
			{
				$columnname="prim_number_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($prim[$name],'number');
				$j++;
			}
			elseif($type==2)
			{
				//$col_prim[]="prim_email_col_".$file1colid;
				$cleanval=$this->cleanField($prim[$name],'email');
					
				$columnname="prim_email_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
				//$col_prim[]="prim_email_account_col_".$file1colid;
				$columnname="prim_email_account_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterEmail($cleanval,'account');
				$j++;
					
				//$col_prim[]="prim_email_domain_col_".$file1colid;
				$columnname="prim_email_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterEmail($cleanval,'domain');
				$j++;
					
				/*$columnname="prim_email_isfreeemail_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterEmail($cleanval,'isfreeemail');
				$j++;*/
			}
			elseif($type==3)
			{
				//$col_prim[]="prim_postal_col_".$file1colid;
				$columnname="prim_postal_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($prim[$name],'postal');
				$j++;
					
			}
			elseif($type==4)
			{
				$origval=$prim[$name];
				$cleanval=$this->cleanField($prim[$name],'phone');
					
				//$col_prim[]="prim_phone_col_".$file1colid;
				$columnname="prim_phone_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				//$col_prim[]="prim_phone_countrycode_col_".$file1colid;
				$columnname="prim_phone_countrycode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'countrycode');
				$j++;

				//$col_prim[]="prim_phone_areacode_col_".$file1colid;
				$columnname="prim_phone_areacode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'areacode');
				$j++;

				//$col_prim[]="prim_phone_no_col_".$file1colid;
				$columnname="prim_phone_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'no');
				$j++;

				//$col_prim[]="prim_phone_ext_col_".$file1colid;
				$columnname="prim_phone_ext_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'ext');
				$j++;

			}
			elseif($type==5)
			{
				$cleanval=$this->cleanField($prim[$name],'company');
				//$col_prim[]="prim_company_col_".$file1colid;
				$columnname="prim_company_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
				$columnname="prim_company_withoutsuffix_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterCompany($cleanval,'suffix');
				$j++;
					
				$columnname="prim_company_abbr_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterCompany($cleanval,'abbr');
				$j++;
			}
			elseif($type==6)
			{
				$cleanval=$this->cleanField($prim[$name],'address');
				//$col_prim[]="prim_address_col_".$file1colid;
				$columnname="prim_address_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
				//$col_prim[]="prim_address_no_col_".$file1colid;
				$columnname="prim_address_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterAddress($cleanval,'no');
				$j++;
					
				//$col_prim[]="prim_address_streetname_col_".$file1colid;
				$columnname="prim_address_streetname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterAddress($cleanval,'streetname');
				$j++;
			}
			elseif($type==7)
			{
				$cleanval=$this->cleanField($prim[$name],'firstname');
				//$col_prim[]="prim_firstname_col_".$file1colid;
				$columnname="prim_firstname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
				//$col_prim[]="prim_firstname_initial_col_".$file1colid;
			/*	$columnname="prim_firstname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
					
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
				//log::info("strlen==".strlen($cleanval)."==initial==".$initial);
				$col_name_values[$j]['value']=$initial;
				$j++;
					
				//$col_prim[]="prim_firstname_startswith_col_".$file1colid;
				$columnname="prim_firstname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->startswith($cleanval);
					
					
				$j++;
			*/
				//$col_prim[]="prim_firstname_wordcount_col_".$file1colid;
				/*$columnname="prim_firstname_wordcount_".$name;
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
					
				$columnname="prim_firstname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				//log::info("soundlike=".$soundslike."=for=".strlen($cleanval));
				$col_name_values[$j]['value']=$soundslike;
				$j++;
			}
			elseif($type==8)
			{
				$cleanval=$this->cleanField($prim[$name],'lastname');
				//$col_prim[]="prim_lastname_col_".$file1colid;
				$columnname="prim_lastname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
					
			/*	//$col_prim[]="prim_lastname_initial_col_".$file1colid;
				$columnname="prim_lastname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
				$col_name_values[$j]['value']=$initial;
				$j++;
					
				//$col_prim[]="prim_lastname_startswith_col_".$file1colid;
				$columnname="prim_lastname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->startswith($cleanval);
				$j++;
			*/		
				//$col_prim[]="prim_lastname_wordcount_col_".$file1colid;
				/*$columnname="prim_lastname_wordcount_".$name;
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
					
				$columnname="prim_lastname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				$j++;
			}
			elseif($type==9)
			{
				//$col_prim[]="prim_city_col_".$file1colid;
				$columnname="prim_city_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($prim[$name],'city');
				$j++;
			}
			elseif($type==10)
			{
				//$col_prim[]="prim_state_col_".$file1colid;
				$columnname="prim_state_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($prim[$name],'state');
				$j++;
			}
			elseif($type==11)
			{
				//$col_prim[]="prim_country_col_".$file1colid;
				$columnname="prim_country_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($prim[$name],'country');
				$j++;
			}
			elseif($type==12)
			{
				$cleanval=$this->cleanField($prim[$name],'webaddress');
				$columnname="prim_webaddress_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
				$columnname="prim_webaddress_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterWebAddress($cleanval,'domain');
				$j++;
				$columnname="prim_webaddress_subdomain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterWebAddress($cleanval,'subdomain');
				$j++;
			}
			elseif($type==13)
			{
				//$cleanval=$this->cleanField($prim[$name],'date');
				$columnname="prim_date_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$prim[$name];
				$j++;
				$columnname="prim_date_db_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=date("Y-m-d H:i:s", strtotime($prim[$name]));
				$j++;
				$columnname="prim_date_year_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($prim[$name],'year');
				$j++;
				$columnname="prim_date_month_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($prim[$name],'month');
				$j++;
				$columnname="prim_date_day_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($prim[$name],'day');
				$j++;
				$columnname="prim_date_time_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($prim[$name],'time');
				$j++;
			}

		}

		//$insertstr="['primeid' => '".$primeid."'";
		$columns=[];
		$columns['primeid']=$primeid;
		$columnnames="";
		$columnvalues="";
		foreach($col_name_values as $col)
		{
			//log::info("-------prim-------");
			$name="";
			$value="";
			foreach($col as $k=>$c)
			{
				////log::info($k."===".$c);

				if($k=="name")
				{
					$name=$c;
					
				}
				if($k=="value")
				{
					$value=$c;
					
				}
				
			}
			
			$columns[$name]=$value;
			
		}
		//print_r($columns);
		$colname="";
		$colval="";
	foreach($columns as $name=>$val)
	{
		
		$colname.=$name.",";
		$colval.="'".$val."',";
		//echo "\n".$name."===".$val;
		//echo "\n"."======";
	}
	/*echo "\n"."---------------";
	echo "\n".$colname;
	echo "\n"."======";
	echo "\n".$colval;
	*/
	$colname=substr($colname,0, strlen($colname)-1);
	$colval=substr($colval,0, strlen($colval)-1);
	$ins="INSERT into ax_job_".$projectid."_analysis_primary($colname) values($colval)";
	//echo $ins;
	//echo "\n"."======\n";
	$conn->query($ins);
	
		//$id = $conn->query('job_'.$projectid.'_analysis_primary')->insertGetId($columns);
		//echo "----".$columnnames."======".$columnvalues;
		//$columns[$name]=$value;
		/*$columnnames=substr($columnnames,0, strlen($columnnames)-1);
		$columnvalues=substr($columnvalues,0, strlen($columnvalues)-1);
		$columnnames="";
		$columnvalues="";
		//$id = $conn->query('job_'.$projectid.'_analysis_secondary')->insertGetId($columns);
		$ins="INSERT into ax_job_".$projectid."_analysis_primary($columnnames) values($columnvalues)";
		//echo $ins;
		$conn->query($ins);*/
	}



}
public function InsertIntoAnalysisTableSecondary($projectid,$secondfile,$colist,$co2list)
{
	$conn=$this->conn;
	//log::info("co2list==".$co2list);
	$colist1=substr($co2list,0,strlen($co2list)-1);
	$colsecarray=explode(",",$colist1);
	$col_name_type=[];
	$i=0;
	foreach($colsecarray as $c)
	{
		$column=str_replace("col_","",$c);
		$column=str_replace("`","",$column);
		$sql="SELECT type from `ax_job_".$projectid."_process` WHERE colid=".$column;
		$result=$conn->query($sql);
		while($type = $result->fetch_assoc()) {
		//Log::info("TYPE OF ".$c."=".$type[0]->type);
		$col_name_type[$i]['name']=str_replace("`","",$c);
		$col_name_type[$i]['type']=$type['type'];
		$i++;
		}
	}

	//print_r($col_name_type);
	/*	foreach($col_name_type as $col)
	 {

	 foreach($col as $k=>$c)
	 {
	 LOg::info($k."===".$c);
	 }
	 }*/
	$secondarys=[];
	$secondary_table = 'ax_job_'.$projectid.'_secondary';
	$result = $conn->query("SELECT $co2list id,MASHFIELD FROM $secondary_table");
	while($row = $result->fetch_assoc()) {
		
		$secondarys[]=$row;
	}
	$col_name_values=[];
	$j=0;
	foreach($secondarys as $sec)
	{
		$secid=$sec['id'];
		foreach($col_name_type as $col)
		{
			$type="";
			$name="";
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);
				if($k=="type")
				{
					$type=$c;
				}
				elseif($k=="name")
				{
					$name=$c;
				}

			}
			//log::info($type."====".$name);
			//log::info($sec[$name]);

			if($type==0)
			{

				$cleanval=$this->cleanField($sec[$name],'text');
				$columnname="sec_text_".$name;
				//$$columnname=$sec[$name];
				//Log::info($columnname."=========".$$columnname);

				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				/*$columnname="sec_text_wordcount_".$name;
					//$$columnname=str_word_count($sec[$name]);
					//Log::info($columnname."=========".$$columnname);

					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/
			}
			elseif($type==1)
			{
				$columnname="sec_number_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'number');
				$j++;
			}
			elseif($type==2)
			{
					
				$cleanval=$this->cleanField($sec[$name],'email');
				//$col_sec[]="sec_email_col_".$file1colid;
				$columnname="sec_email_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				//$col_sec[]="sec_email_account_col_".$file1colid;
				$columnname="sec_email_account_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterEmail($cleanval,'account');
				$j++;

				//$col_sec[]="sec_email_domain_col_".$file1colid;
				$columnname="sec_email_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterEmail($cleanval,'domain');
				$j++;


			}
			elseif($type==3)
			{
				//$col_sec[]="sec_postal_col_".$file1colid;
				$columnname="sec_postal_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'postal');
				$j++;

			}
			elseif($type==4)
			{
				$origval=$sec[$name];
				$cleanval=$this->cleanField($sec[$name],'phone');
				//$col_sec[]="sec_phone_col_".$file1colid;
				$columnname="sec_phone_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				//$col_sec[]="sec_phone_countrycode_col_".$file1colid;
				$columnname="sec_phone_countrycode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'countrycode');
				$j++;

				//$col_sec[]="sec_phone_areacode_col_".$file1colid;
				$columnname="sec_phone_areacode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'areacode');
				$j++;

				//$col_sec[]="sec_phone_no_col_".$file1colid;
				$columnname="sec_phone_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'no');
				$j++;

				//$col_sec[]="sec_phone_ext_col_".$file1colid;
				$columnname="sec_phone_ext_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterPhone($origval,'ext');
				$j++;

			}
			elseif($type==5)
			{
				//$col_sec[]="sec_company_col_".$file1colid;
				$columnname="sec_company_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'company');
				$j++;
			}
			elseif($type==6)
			{
				$cleanval=$this->cleanField($sec[$name],'address');
				//$col_sec[]="sec_address_col_".$file1colid;
				$columnname="sec_address_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($cleanval,'address');
				$j++;

				$col_sec[]="sec_address_no_col_".$file1colid;
				$columnname="sec_address_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterAddress($cleanval,'no');
				$j++;

				$col_sec[]="sec_address_streetname_col_".$file1colid;
				$columnname="sec_address_streetname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterAddress($cleanval,'streetname');
				$j++;
			}
			elseif($type==7)
			{
				$cleanval=$this->cleanField($sec[$name],'firstname');
				//$col_sec[]="sec_firstname_col_".$file1colid;
				$columnname="sec_firstname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				//$col_sec[]="sec_firstname_initial_col_".$file1colid;
			/*	$columnname="sec_firstname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
				$col_name_values[$j]['value']=$initial;
				$j++;

				//$col_sec[]="sec_firstname_startswith_col_".$file1colid;
				$columnname="sec_firstname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->startswith($cleanval);
				$j++;
			*/
				//$col_sec[]="sec_firstname_wordcount_col_".$file1colid;
				/*	$columnname="sec_firstname_wordcount_".$name;
					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/

				$columnname="sec_firstname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				$j++;
			}
			elseif($type==8)
			{
				$cleanval=$this->cleanField($sec[$name],'lastname');
				//$col_sec[]="sec_lastname_col_".$file1colid;
					
				$columnname="sec_lastname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;

				//$col_sec[]="sec_lastname_initial_col_".$file1colid;
			/*	$columnname="sec_lastname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
					
				$col_name_values[$j]['value']=$initial;
				$j++;

				//$col_sec[]="sec_lastname_startswith_col_".$file1colid;
				$columnname="sec_lastname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->startswith($cleanval);
				$j++;
			*/
				//$col_sec[]="sec_lastname_wordcount_col_".$file1colid;
				/*$columnname="sec_lastname_wordcount_".$name;
					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/

				$columnname="sec_lastname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				$j++;
			}
			elseif($type==9)
			{
				//$col_sec[]="sec_city_col_".$file1colid;
				$columnname="sec_city_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'city');
				$j++;
			}
			elseif($type==10)
			{
				//$col_sec[]="sec_state_col_".$file1colid;
				$columnname="sec_state_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'state');
				$j++;
			}
			elseif($type==11)
			{
				//$col_sec[]="sec_country_col_".$file1colid;
				$columnname="sec_country_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->cleanField($sec[$name],'country');
				$j++;
			}
			elseif($type==12)
			{
				$cleanval=$this->cleanField($sec[$name],'webaddress');
				$columnname="sec_webaddress_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				$j++;
				$columnname="sec_webaddress_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterWebAddress($cleanval,'domain');
				$j++;
				$columnname="sec_webaddress_subdomain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterWebAddress($cleanval,'subdomain');
				$j++;
			}
			elseif($type==13)
			{
				$cleanval=$this->cleanField($sec[$name],'date');
				$db_date=date("Y-m-d H:i:s", strtotime($sec[$name]));
				$columnname="sec_date_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$sec[$name];
				$j++;
				$columnname="sec_date_db_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=date("Y-m-d H:i:s", strtotime($sec[$name]));
				$j++;
				$columnname="sec_date_year_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($sec[$name],'year');
				$j++;
				$columnname="sec_date_month_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($sec[$name],'month');
				$j++;
				$columnname="sec_date_day_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($sec[$name],'day');
				$j++;
				$columnname="sec_date_time_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$this->filterDate($sec[$name],'time');
				$j++;
			}

		}

		//$insertstr="['seceid' => '".$seceid."'";
		$columns=[];
		$columns['secondid']=$secid;
		$columnnames="";
		$columnvalues="";
		foreach($col_name_values as $col)
		{
			//log::info("--------------");
			$name="";
			$value="";
			
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);

				if($k=="name")
				{
					$name=$c;
					
				}
				if($k=="value")
				{
					$value=$c;
					
				}

			}
			//log::info("name=".$name."==value==".$value);
			$columns[$name]=$value;
				
		}
/*echo $columnnames."======".$columnvalues;
$columnnames=substr($columnnames,0, strlen($columnnames)-1);
$columnvalues=substr($columnvalues,0, strlen($columnvalues)-1);
		//$id = $conn->query('job_'.$projectid.'_analysis_secondary')->insertGetId($columns);
$ins="INSERT into ax_job_'.$projectid.'_analysis_secondary($columnnames) values($columnvalues)";
$conn->query($ins);*/
		//print_r($columns);
		$colname="";
		$colval="";
		foreach($columns as $name=>$val)
		{
		
			$colname.=$name.",";
			$colval.="'".$val."',";
			//echo "\n".$name."===".$val;
			//echo "\n"."======";
		}
		/*echo "\n"."---------------";
		 echo "\n".$colname;
		 echo "\n"."======";
		 echo "\n".$colval;
		 */
		$colname=substr($colname,0, strlen($colname)-1);
		$colval=substr($colval,0, strlen($colval)-1);
		$ins="INSERT into ax_job_".$projectid."_analysis_secondary($colname) values($colval)";
		//echo $ins;
		//echo "\n"."======\n";
		$conn->query($ins);
	}



}
public function cleanField($field,$type)
{
	$conn=$this->conn;
	$field=strtolower($field);
	$search=array("+","(",")",",","?","#","%","!","_","�","'","\\","<",">","*","\/","`","[","]","none","n/a","'","\'","not available","unknown");
	$field =trim($field);
	$field = str_replace($search,"",$field);
	$field=strtolower($field);

	$search=array(" and "," & "," the "," of "," on "," in ");
	$field = str_replace($search," ",$field);
	$field=mb_convert_encoding($field, "UTF-8");
	if($type=="firstname")
	{
		$search=array(".");
		$salutations=array("Mr","Mrs","Miss","Ms","Dr","Prof","Lady","Lord","Sir","Junior","Jr");
		$field = str_replace($search,"",$field);
		$field = str_replace($salutations,"",$field);
		$field = str_replace("-"," ",$field);
	}
	elseif($type=="number")
	{
		$field = str_replace("-","",$field);
	}
	elseif($type=="postal")
	{
		$search=array("-"," ");
		$field = str_replace($search,"",$field);
		$field=strtoupper($field);
	}
	elseif($type=="phone")
	{
			
		$search=array("-"," ",".","x","ext","extension","+","(",")","'","\'");
		$field = str_replace($search,"",$field);

	}
	elseif($type=="webaddress")
	{
		//log::info("webaddress=".$field);
		$field=parse_url($field,PHP_URL_HOST);
		$search=array("http://","https://","ftp://","www.");
			
			
		$field = str_replace($search,"",$field);
		if(strpos($field,"/")!==false)
		{
			$field=substr($field,0,strpos($field,"/"));
		}
		if(strpos($field,"?")!==false)
		{
			$field=substr($field,0,strpos($field,"?"));
		}
		if(strpos($field,"#")!==false)
		{
			$field=substr($field,0,strpos($field,"#"));
		}
			
		//log::info("webaddressafter=".$field);
		$pos=0;
		$cntdots=substr_count($field, ".");
		for($i=0;$i<$cntdots;$i++)
		{

			$pos=strpos($field,".",$pos);
			$partstr=substr($field,$pos,strlen($field));
			$pos++;
			//log::info("webaddresdotsafter=".$partstr);
			$partstr1=substr($partstr,1,strlen($partstr));
			$sqlp="SELECT count(*) as cnt FROM `public_suffix_list` where suffix='".$partstr1."'";
			//log::info("sqlp=".$sqlp);
			$result=$conn->query($sqlp);
			while($db_web = $result->fetch_assoc()) {
			if($db_web['cnt']>0)
			{
				$field=str_replace($partstr,"",$field);
				//log::info("webaddressfinal=".$field);
				break;
					

			}
			}
		}
			
	}
	elseif($type=="company")
	{
			
		//log::info("begin field=".$field);
			
		$search=array("-",".","(",")");
		$field = str_replace($search,"",$field);
		/*if(strpos($field," ")!==false)
			{
			$pos=0;
			$cntspaces=substr_count($field, " ");
			for($i=0;$i<$cntspaces;$i++)
			{

			$pos=strpos($field," ",$pos);
			$partstr=substr($field,$pos,strlen($field));
			$pos++;
			log::info("companynamerafter=".$partstr);
			$partstr1=substr($partstr,1,strlen($partstr));
			$sqlp="SELECT count(*) as cnt FROM `company_suffix_list` where suffix='".$partstr1."'";
			log::info("sqlp=".$sqlp);
			$db_web=DB::select(DB::raw($sqlp));
			if($db_web[0]->cnt>0)
			{
			$field=str_replace($partstr,"",$field);
			log::info("companynamefinal=".$field);
			break;
				

			}
			}
				


			}*/
	}


	return $field;
}
public function filterCompany($field,$type)
{
	$conn=$this->conn;
	if($type=="suffix")
	{
		if(strpos($field," ")!==false)
		{
			$pos=0;
			$cntspaces=substr_count($field, " ");
			for($i=0;$i<$cntspaces;$i++)
			{
					
				$pos=strpos($field," ",$pos);
				$partstr=substr($field,$pos,strlen($field));
				$pos++;
				//log::info("companynamerafter=".$partstr);
				$partstr1=substr($partstr,1,strlen($partstr));
				$sqlp="SELECT count(*) as cnt FROM `ax_company_suffix_list` where suffix='".$partstr1."'";
				//echo $sqlp;
				//log::info("sqlp=".$sqlp);
				$result=$conn->query($sqlp);
				while($db_web = $result->fetch_assoc()) {
				if($db_web['cnt']>0)
				{
					$field=str_replace($partstr,"",$field);
					//log::info("companynamefinal=".$field);
					break;
						
						
				}
				}
			}
				
				
				
		}
		return $field;
	}
	elseif($type=="abbr")
	{


		$abbr="";
		if(strpos($field," ")!==false)
		{
			$parts=explode(" ", $field);
			foreach($parts as $part)
			{
				$p=substr($part,0,1);
				$abbr.=$p;
			}
		}
		return $abbr;
	}
}
public function filterEmail($field,$type)
{
	$conn=$this->conn;
	if($field!="")
	{
		if(strpos($field,"@")=== false)
		{
			$field="";
		}
		else {

				
			$partsemail=explode("@",$field);
			$account=$partsemail[0];
				
			$partsafter=explode(".",$partsemail[1]);
			$domain=$partsafter[0];
				
			if($type=="account")
			{
				return $account;
			}
			else if($type=="domain")
			{
				return $domain;
			}
			else if($type=="isfreeemail")
			{
				$checkFreeEmail=false;
				$sqlp="SELECT count(*) as cnt FROM `ax_free_email_providers` where domain='".$partsemail[1]."'";
				//log::info("sqlp=".$sqlp);
				$result=$conn->query($sqlp);
				while($db_web = $result->fetch_assoc()) {
				if($db_web['cnt']>0)
				{
					$checkFreeEmail=true;
				}
				}	
				return $checkFreeEmail;
			}
			else
			{
				return $field;
			}
		}
	}
	else
	{
		return $field;
	}
}
public function filterPhone($field,$type)
{
	$conn=$this->conn;
	//log::info("original value=".$field);




	$extension="";
	$countrycode="";
	$areacode="";
	$no="";
	$country="";
	$area="";

	if(stripos($field,"x")>0 || stripos($field,"ext")>0 || stripos($field,"extension")>0 || stripos($field,"eten")>0 || stripos($field,"et")>0 || stripos($field,"exten")>0)
	{
		if(stripos($field,"extension")>0)
		{
			$extension=substr($field,stripos($field,"extension")+1,strlen($field));
			$no=substr($field,0,stripos($field,"extension"));
		}
		else if(stripos($field,"exten")>0)
		{
			$extension=substr($field,stripos($field,"exten")+1,strlen($field));
			$no=substr($field,0,stripos($field,"exten"));
		}
		else if(stripos($field,"eten")>0)
		{
			$extension=substr($field,stripos($field,"eten")+1,strlen($field));
			$no=substr($field,0,stripos($field,"eten"));
		}
		else if(stripos($field,"ext")>0)
		{
			$extension=substr($field,stripos($field,"ext")+1,strlen($field));
			$no=substr($field,0,stripos($field,"ext"));
		}
		else if(stripos($field,"et")>0)
		{
			$extension=substr($field,stripos($field,"et")+1,strlen($field));
			$no=substr($field,0,stripos($field,"et"));
		}
		else if(stripos($field,"x")>0)
		{
			$extension=substr($field,stripos($field,"x")+1,strlen($field));
			$no=substr($field,0,stripos($field,"x"));
		}
		$search_array=array(".","-","(",")","'","\'");
		$extension=trim(str_replace($search_array, "", $extension));
		$extension = preg_replace('/\s+/', ' ',$extension);
	}
	else
	{
		$no=$field;
	}
	//log::info("extension=".$extension);

	$countrycodepresent=false;
	if(stripos($field,"+")!==false)
	{
		$countrycodepresent=true;
	}


	$search_array=array(".","-","(",")","'","\'");
	$field=str_replace($search_array, " ", $field);
	$field=str_replace("+", "", $field);
	$field = preg_replace('/\s+/', ' ',$field);
	$field=trim($field);
	$no=str_replace($search_array, "", $no);
	$no=str_replace("+", "", $no);
	$no = preg_replace('/\s+/', '',$no);
	$no=trim($no);
	//log::info("new phone value=".$field);
	$phoneparts=array();
	if(stripos($field," ")!==false)
	{
		$phoneparts=explode(" ",$field);
	}
	if($countrycodepresent && count($phoneparts)>1)
	{
		$sqlp="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$phoneparts[0]."'";
		//log::info("sqlp=".$sqlp);
		$db_phone=$conn->query($sqlp);
		if($db_phone)
		{
			$countrycode=$db_phone[0]->countrycode;
			//$areacode=$db_phone[0]->areacode;
			$country=$db_phone[0]->countryname;
			//$area=$db_phone[0]->area;
				
		}
	}

	if(count($phoneparts)>1)
	{
		$sqlp="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$phoneparts[0]."' and areacode='".$phoneparts[1]."'";
		//log::info("sqlp1=".$sqlp);
		$db_phone=$conn->query($sqlp);
		if($db_phone)
		{
			$countrycode=$db_phone[0]->countrycode;
			$areacode=$db_phone[0]->areacode;
			$country=$db_phone[0]->countryname;
			$area=$db_phone[0]->area;
				
		}
			
	}

	/*
	 $firstdigit=substr($field,0,1);//1 digit country code for countrycode 1 and 7
	 $digit23=substr($field,1,2);//2 digit area code for countrycode 1 and 7
	 $digit234=substr($field,1,3);//3 digit area code for countrycode 1 and 7
	 $digit2345=substr($field,1,4);//4 digit area code  for country code 7
	 $digit23456=substr($field,1,5);//5 digit area code  for country code 7
	 	
	 $digit123=substr($field,0,3);//3 digit countrycode
	 $digit456=substr($field,3,3);//3 digit area code
	 $digit45=substr($field,3,2);//2 digit area code
	 $digit4=substr($field,3,1);//1 digit area code
	 	
	 $digit12=substr($field,0,2);//2 digit countrycode
	 $digit345=substr($field,2,3);//3 digit area code
	 $digit34=substr($field,2,2);//2 digit area code
	 $digit3=substr($field,2,1);//1 digit area code
	 	
	 $len=strlen($field);
	 	
	 	
	 if($firstdigit=="1" )
	 {

	 if(strlen($field)>10)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit234."'" ;
	 log::info("sql1=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
			
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 	

	 }
	 }

	 }
	 else if($firstdigit=="7")
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit23456."'" ;
	 log::info("sql2=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
			
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit2345."'" ;
	 log::info("sql3=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }

			
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit234."'" ;
	 log::info("sql4=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }



	 }
	 	
	 else {

	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit456."'" ;
	 log::info("sql5=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit45."'" ;
	 log::info("sql6=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit4."'" ;
	 log::info("sql7=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit345."'" ;
	 log::info("sql8=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit34."'" ;
	 log::info("sql9=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit3."'" ;
	 log::info("sql10=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }



	 }
	 if($countrycode=="" && $areacode=="" && strlen($field)==10)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='1'  and areacode='".$digit123."'" ;
	 log::info("sql11=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
	 	
	 }
	 }
	 	
		//}*/
	//log::info("ext=".$extension."=country=".$countrycode."=area=".$areacode."=".$country."=".$area);


	if($type=="countrycode")
	{
		return $countrycode;
	}
	else if($type=="areacode")
	{
		return $areacode;
	}
	else if($type=="no")
	{
		return $no;
	}
	if($type=="ext")
	{
		return $extension;
	}
	return $field;
}
public function filterAddress($field,$type)
{
	return $field;
}
public function startswith($field)
{

	$pos=strpos($field," ");


	if($pos===false)
	{
		if(strlen($field)>2)
		{
			return $field;
		}
	}
	else {
		$partsfield=explode(" ",$field);
		foreach($partsfield as $part)
		{
			if(strlen($part)>2)
			{
				return $part;
			}
		}
	}

		
}
public function filterWebAddress($field,$type)
{
	$domain="";
	$subdomain="";

	$field=strtolower($field);
	if((strpos($field,"."))!==false)
	{
		$partsweb=[];
		$partsweb=explode(".",$field);

		$domain=$partsweb[count($partsweb)-1];
		//log::info("domain=====".$domain);
			
		for($i=0;$i<(count($partsweb)-1);$i++)
		{
			$subdomain.=$partsweb[$i].".";

		}
		$subdomain=substr($subdomain,0,strlen($subdomain)-1);
	}
	else
	{
		$domain=$field;
	}
	if($type=="domain")
	{
		return $domain;
	}
	else if($type=="subdomain")
	{
			
		return $subdomain;
	}






	return $field;

}
public function filterDate($field,$type)
{
	/*$search=array("/"," ",".",":",",");
	 $field =trim($field);
	 $field = str_replace($search,"-",$field);
	 $field = preg_replace('/\s+/', ' ',$field);*/
	$timestamp = strtotime($field);
	$date=date("Y-m-d",$timestamp);
	if($date=="1970-01-01" && strpos($field,"/")!==false)
	{
		$timestamp = strtotime(str_replace('/', '-', $field));
	}





	//log::info("origdate===".$field."===".$type."===date=".$date);
	$year=date("Y", $timestamp);
	//log::info("year===".$year."===".$type);






	if($type=="year")
	{
		$year=date("Y", $timestamp);
		//log::info("year===".$year);
		return $year;
	}
	elseif($type=="month")
	{
		$month=date("m", $timestamp);
		//log::info("month===".$month);
		return $month;
	}
	elseif($type=="day")
	{
		$day=date("d", $timestamp);
		return $day;
	}
	elseif($type=="time")
	{
		$time=date("H:i:s", $timestamp);
		return $time;
	}


}
}

