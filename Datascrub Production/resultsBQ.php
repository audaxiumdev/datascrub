<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



/*
echo "====post==";
print_r($_POST);
echo "====request==";
print_r($_REQUEST);
echo "====get==";
print_r($_GET);

*/



$current_projectid=get_id();

//echo "----------------".$current_projectid;
if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}
//checkProjectCompleted($current_projectid,$conn);
//echo "===".$_SESSION['userid'];
if(!isset($_SESSION['userid']) or $_SESSION['userid']=="")
{
	echo "<h1>Your session has been expired . Please Login again</h1>";
	exit;	
}
else 
{
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

}
//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}





$sql_ml_id="select id from ax_ml where project_id=".$current_projectid;
if( $ml_id_res = $conn->query($sql_ml_id))
{
	while($ml_id_row = $ml_id_res->fetch_assoc())
	{
		$ml_id = $ml_id_row['id'];

	}
}




//BEGIN BIG QUERY

# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\BigQuery\BigQueryClient;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Core\ExponentialBackoff;
/*
//Create Results Table in Big Query
$fields = [
		[
				'name' => 'id',
				'type' => 'integer',
				'mode' => 'required'
		],
		[
				'name' => 'primeid',
				'type' => 'integer',
				'mode' => 'required'
		],
		[
				'name' => 'secondid',
				'type' => 'integer',
				'mode' => 'required'
		],
		[
				'name' => 'result',
				'type' => 'float'
				
		],
];


$projectId="datascrub-152522";
$datasetId="ax_datascrub";
$tableId="ax_job_".$current_projectid."_results";
$schema = ['fields' => $fields];
//echo "calling create table";
 $t=create_table($projectId, $datasetId, $tableId, $schema);
 echo "==Table created==".$t;
 echo "---";
 print_r($t);
function create_table($projectId, $datasetId, $tableId, $schema)
{
	echo "<br>inside create table<br>";
	$bigQuery = new BigQueryClient([
			'projectId' => $projectId,
	]);
	$dataset = $bigQuery->dataset($datasetId);
	$options = ['schema' => $schema];
	$table = $dataset->createTable($tableId, $options);
	return $table;
}

//End BQ Create Table

 
//BEGIN Query To Insert into results table
echo "calling query to run";
$projectId="datascrub-152522";
$resultstable="datascrub-152522.ax_datascrub.ax_job_".$current_projectid."_results";
$cloudSQLtable="datascrub-152522.ax_datascrub.Cloud11";
$query='INSERT INTO  `'.$resultstable.'` (id,primeid,secondid,result)
SELECT  ROW_NUMBER() OVER() as id,a.primary_id,MAX(a.secondary_id) as secondid,MAX(a.matching_value) as result FROM `'.$cloudSQLtable.'` a
INNER JOIN (SELECT primary_id, MAX(matching_value) as matching_value FROM `'.$cloudSQLtable.'` GROUP BY primary_id) b ON a.primary_id = b.primary_id
AND a.matching_value = b.matching_value  GROUP BY a.primary_id';
echo $query."<br>";
$useLegacySql=false;
run_query_as_job($projectId, $query, $useLegacySql);
function run_query_as_job($projectId, $query, $useLegacySql)
{
	echo "inside call query useLegacySql=".$useLegacySql;
	$bigQuery = new BigQueryClient([
			'projectId' => $projectId,
	]);
	$job = $bigQuery->runQueryAsJob(
			$query,
			['jobConfig' => ['useLegacySql' => $useLegacySql]]);
	$backoff = new ExponentialBackoff(10);
	$backoff->execute(function () use ($job) {
		print('Waiting for job to complete' . PHP_EOL);
		$job->reload();
		if (!$job->isComplete()) {
			throw new Exception('Job has not yet completed', 500);
		}
	});
		$queryResults = $job->queryResults();

		if ($queryResults->isComplete()) {
			$i = 0;
			$rows = $queryResults->rows();
			print_r($rows);*/
			/*foreach ($rows as $row) {
				printf('--- Row %s ---' . PHP_EOL, ++$i);
				foreach ($row as $column => $value) {
					printf('%s: %s' . PHP_EOL, $column, $value);
				}
			}
			printf('Found %s row(s)' . PHP_EOL, $i);*/
	/*	} else {
			throw new Exception('The query failed to complete');
		}
}
*/

/*
//echo "<br>ml id====".$ml_id;
//exit;
$sql_chk_results="Select * from `ax_job_".$current_projectid."_results` ";
$res_chk_results=$conn->query($sql_chk_results);

	if(!$res_chk_results || $res_chk_results->num_rows<=0)
	{
		//CREATE RESULTS TABLE
		$dropsql="DROP TABLE IF EXISTS `ax_job_".$current_projectid."_results`";
		//echo "<br>".$dropsql;
		$conn->query($dropsql);
		$sql="CREATE TABLE ax_job_".$current_projectid."_results (
  			id int(11) unsigned NOT NULL AUTO_INCREMENT,
  			primeid int(11) DEFAULT NULL,
  			secondid int(11) DEFAULT NULL,
  			result decimal(10,2) DEFAULT NULL,
  			PRIMARY KEY (id),
			KEY result (result),
  			KEY primeid (primeid),
  			KEY secondid (secondid)
			)  CHARSET=latin1;";
		//echo "<br>".$sql;
		if($conn->query($sql))
		{
			//echo "Results table created";
		}
		else 
		{
			echo $conn->error;
			error_log($conn->error);
			exit;
		}
		$sql_prim="select id from ax_job_".$current_projectid."_primary";
		echo $sql_prim;
		if( $res_prim = $conn->query($sql_prim))
		{
			
			//echo "inside";
			while($row_prim = $res_prim->fetch_assoc())
			{
				$prim_id = $row_prim['id'];
				echo $prim_id;
				$colsql = "INSERT INTO ax_job_".$current_projectid."_results(primeid,secondid,result) ";
				$colsql .= "SELECT a.primary_id,MAX(a.secondary_id) as secondid,MAX(a.matching_value) as result FROM ax_ml_".$ml_id."_comparisons a
INNER JOIN (SELECT primary_id, MAX(matching_value) as matching_value FROM ax_ml_".$ml_id."_comparisons GROUP BY primary_id) b ON a.primary_id = b.primary_id AND a.matching_value = b.matching_value AND a.primary_id=".$prim_id." GROUP BY a.primary_id";
				
				//echo "<br>".$colsql;
				//exit;
				if($conn->query($colsql))
				{
						
				}
				else
				{
					echo $conn->error;
					error_log($conn->error);
					exit;
				}
		
			}
		}
		
		
	}
	
		



*/



function run_query_as_job($projectId, $query, $useLegacySql)
{
	echo "inside call query useLegacySql=".$useLegacySql."<br>".$query;
	$bigQuery = new BigQueryClient([
			'projectId' => $projectId,
	]);
	$job = $bigQuery->runQueryAsJob(
			$query,
			['jobConfig' => ['useLegacySql' => $useLegacySql]]);
	$backoff = new ExponentialBackoff(10);
	$backoff->execute(function () use ($job) {
		print('Waiting for job to complete' . PHP_EOL);
		$job->reload();
		if (!$job->isComplete()) {
			throw new Exception('Job has not yet completed', 500);
		}
	});
		$queryResults = $job->queryResults();

		if ($queryResults->isComplete()) {
			$i = 0;
			$rows = $queryResults->rows();
			//print_r($rows);
			foreach ($rows as $row) {
				printf('--- Row %s ---' . PHP_EOL, ++$i);
			foreach ($row as $column => $value) {
			printf('%s: %s' . PHP_EOL, $column, $value);
			}
			}
			printf('Found %s row(s)' . PHP_EOL, $i);
				} else {
			 throw new Exception('The query failed to complete');
			}
}

function run_query($projectId, $query)
{
$bigQuery = new BigQueryClient([
    'projectId' => $projectId,
]);
//echo"inside run_query function";
//echo "<br>".$query."<br>";



$useLegacySql=true;
$options = ['useLegacySql' => $useLegacySql];
$queryResults = $bigQuery->runQuery($query, $options);

if ($queryResults->isComplete()) {
	//echo "query complete";
    $i = 0;
    $rows = $queryResults->rows();
  
    $arr=array();
    
    foreach ($rows as $row) {
    	//echo "<br>count===".count($row)."<br>";
    
    	if(count($row)==1)
    	{
    		foreach ($row as $column => $value) {
    			return $value;
    		}
    	}
    	else{
    		$arr[]= $row;
    	}
    	
    		
    }
    return $arr;
   
    
} else {
	//echo "query not complete";
    throw new Exception('The query failed to complete');
}
}

//select count(id) as cnt,round(matching_value,2) as matching_value from ax_datascrub.ax_ml_12977_comparisons group by round(matching_value,2);
//select round(matching_value,2) as matching_value,count(id) as cnt from ax_datascrub.ax_ml_12977_comparisons group by round(matching_value,2);







function analysisAction($id,$ml_id,$conn){
	 
	 
	//$project = Project::find($id);
	$sql_project="Select * from ax_projects where id=".$id;
	if( $res_sql_project = $conn->query($sql_project))
	{
		while($row_sql_project = $res_sql_project->fetch_assoc())
		{
			$project = $row_sql_project['project_name'];
	
		}
	}
	
	//echo "project name==".$project;
	$from=0;
	$to=100;


	$slider_range_sql="Select * from ax_project_slider_values where project_id=".$id;
	if( $slider_range_res = $conn->query($slider_range_sql))
	{
		while($slider_range = $slider_range_res->fetch_assoc())
		{
			$from = $slider_range['range_from']*0.01;
			$to = $slider_range['range_to']*0.01;
		}
	}		
	
	//echo "<br>to===".$to;
	
	 
	if($to==100)
	{
		$to=1.0;
	}
	if($from==0)
	{
		$from=0.01;
	}
	
	//echo "====FROM==".$from."===to===".$to;
	
	
	$from=25;
	$to=70;
	
	
	
	//$resultstable="datascrub-152522.ax_datascrub.ax_job_".$current_projectid."_results";
	$projectId="datascrub-152522";
	
	$from=25;
	$primarytable="datascrub-152522.ax_datascrub.ds_443_primary";
	$resultstable="datascrub-152522.ax_datascrub.ds_443_results";
	//$get_uniques_sql = "SELECT COUNT(A.id) as `unique` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value is null OR B.matching_value <=".$from;
	
	
	$get_uniques_sql = 'SELECT COUNT(A.id) as unique FROM [datascrub-152522.ax_datascrub.ds_443_primary] A '.
		'LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_results] B '.
		'on (A.id=B.primeid) WHERE B.result is null OR B.result <='.$from;
//	echo "<br>".$get_uniques_sql."<br>-----";
	
	
	
	$total_uniques=run_query($projectId, $get_uniques_sql);
	//echo "<br>uniques==".$total_uniques;
	
	
	
	

	
	

	//$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value >".$from." AND B.matching_value <=".$to;
	//$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$from." AND B.result <=".$to;
	
	$get_maybes_sql = 'SELECT COUNT(A.id) as maybe FROM [datascrub-152522.ax_datascrub.ds_443_primary] A '.
			'LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_results] B '.
			'on (A.id=B.primeid) WHERE B.result >'.$from.'  AND B.result <'.$to;
	//echo "<br>".$get_maybes_sql."<br>-----";
	
	
	
	$total_maybes=run_query($projectId, $get_maybes_sql);
	
	
	
	
	
	//echo "<br>maybes==".$total_maybes;
	
	
	
	//$get_dups_sql= "SELECT COUNT(A.id) as `duplicate` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$to;
	 
	$get_dups_sql = 'SELECT COUNT(A.id) as maybe FROM [datascrub-152522.ax_datascrub.ds_443_primary] A '.
			'LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_results] B '.
			'on (A.id=B.primeid) WHERE  B.result>'.$to;
	//echo "<br>".$get_dups_sql."<br>-----";
	
	
	
	$total_dups=run_query($projectId, $get_dups_sql);
	
	//echo "<br>dups==".$total_dups;
	//exit;


	$sql_check_file2="select file_2 from ax_project_files where project_id=".$id;
	if( $res_check_file2 = $conn->query($sql_check_file2))
	{
		while($row_check_file2 = $res_check_file2->fetch_assoc())
		{
			$isfile2 = $row_check_file2['file_2'];
			
		}
	}
	
	
	$sql_cols="select * from ax_job_".$id."_process";
	if( $res_cols = $conn->query($sql_cols))
	{
		while($row_cols = $res_cols->fetch_object())
		{
			$columns[]=$row_cols;
		}
		
	}	
	
	//print_r($columns);
	//exit;
	$colistheader[]="";
	$co2listheader[]="";
	$colist="A.id ,";
	$co2list="B.id ,";
	$co2listBQ_First="FIRST(B.id) as id ,";
	$colistBQ_First="FIRST(A.id) as id ,";
	$co2listBQ="B.id as id ,";
	$colistBQ="A.id as id ,";
	$columnlist="id,";
	foreach($columns as $col){
		$colistheader[]="col_".$col->colid;
		$colist.="A.`col_".$col->colid."`,";
		$colistBQ_First.="FIRST(A.col_".$col->colid.") AS col_".$col->colid.",";
		$colistBQ.="A.col_".$col->colid." AS col_".$col->colid.",";
		if($col->col2id){
			$co2listheader[]="col_".$col->col2id;
			$co2list.="B.`col_".$col->col2id."`,";
			$co2listBQ_First.="FIRST(B.col_".$col->colid.") AS col_".$col->colid.",";
			$co2listBQ.="B.col_".$col->colid." AS col_".$col->colid.",";
		}
			
	}

	//$fulllist=$colist.substr($co2list,0,strlen($co2list)-1);
	$fulllist=substr($co2list,0,strlen($co2list)-1);
	$fulllistBQ=substr($co2listBQ,0,strlen($co2listBQ)-1);
	$colist=substr($colist,0,strlen($colist)-1);
	$colistBQ=substr($colistBQ,0,strlen($colistBQ)-1);
	//echo "<br><br>colistheader=======";
	
	
	//END OF FETCHING FIELD NAMES SELECTED BY USER TO MATCH


	$projectId="datascrub-152522";
	
	//$duplicates_query_main="select r.result,r.primeid,".$colist."  from `datascrub-152522.ax_datascrub.ax_job_".$id."_results` r LEFT JOIN `datascrub-152522.ax_datascrub.ax_job_".$id."_primary` A on (A.id=r.primeid) where result>".$to." GROUP BY primeid ORDER BY primeid LIMIT 10";
	
	$duplicates_query_main = 'SELECT "" as match,r.primeid as primeid,'.$colistBQ_First.' FROM [datascrub-152522.ax_datascrub.ds_443_results] r '.
			'LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A '.
			'on (A.id=r.primeid) WHERE  r.result>='.$to.' '.
			'GROUP BY primeid ORDER BY primeid LIMIT 10';
	
	
	
	
	//echo $duplicates_query_main."<br>";
	
	$results_duplicates_main=run_query($projectId, $duplicates_query_main);
	//print_r($results_duplicates_main);
	
	
	
	

		
/*	$duplicates_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where result>".$to." GROUP BY primeid ORDER BY primeid LIMIT 10";
	//echo "<br>dqm==".$duplicates_query_main;
	
	$res_duplicates_query_main = $conn->query($duplicates_query_main);
	if( $res_duplicates_query_main->num_rows>0)
	{
		while($row_duplicates_query_main = $res_duplicates_query_main->fetch_object())
		{
			$prev_duplicates_main[]=$row_duplicates_query_main;
		}
	*/
	if(count($results_duplicates_main)>0)
	{
	$arr2_dup=[];
	$arr_dup=[];
	foreach($results_duplicates_main as $main)
	{
		//echo "<br>main=====<br>";
		//print_r($main);
		//exit;
		
		$arr_dup=[];
		$main->id="";
		$main->result="";
		$arr_dup[]=$main;
		//echo "<br><br><br>=======================<br>";
		//print_r($main);
		//$arr_dup[]=$main;
		//echo "<br>".$main->primeid."<br>";

		if($isfile2 =='' || $isfile2 == NULL){
			//ONE FILE
			/*$duplicates_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";*/
			
			$duplicates_query="select yt1.result as result,yt1.primeid as primeid,".$fulllistBQ."
		            from [datascrub-152522.ax_datascrub.ds_443_detail_results] yt1
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A on (A.id=yt1.primeid)
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] B on (B.id=yt1.secondid)
		            where yt1.result >=".$to." and yt1.primeid=".$main['primeid']." and yt1.secondid<>".$main['primeid']."
		            ORDER BY yt1.result DESC LIMIT 5";
		          
		}else{
			/*$duplicates_query="select yt1.matching_value,yt1.primary_id,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";*/
			$duplicates_query="select yt1.result as result,yt1.primeid as primeid,".$fulllistBQ."
		            from [datascrub-152522.ax_datascrub.ds_443_detail_results] yt1
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A on (A.id=yt1.primeid)
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_secondary] B on (B.id=yt1.secondid)
		            where yt1.result >=".$to." and yt1.primeid=".$main['primeid']."
		            ORDER BY yt1.result DESC LIMIT 5";
		}
		//echo $duplicates_query;
		$results_duplicates=run_query($projectId, $duplicates_query);
		//print_r($results_duplicates);
		/*if( $res_duplicates_query = $conn->query($duplicates_query))
		{
			$prev_duplicates=array();
			while($row_duplicates_query = $res_duplicates_query->fetch_object())
			{
				$prev_duplicates[]=$row_duplicates_query;
			}
		}*/
		//echo "=dq=".$duplicates_query."<br><br>";
		//$prev_duplicates = DB::select(DB::raw($duplicates_query));
		//echo "<br>===================<br>";
		//print_r($prev_duplicates);
		//$prev_duplicates=array_merge($main,$prev_duplicates);
		//echo "=====================";
		
	/*	foreach($results_duplicates as $d)
		{
			$arr_dup[]=$d;
				
			
		}
		//print_r($arr_dup);
		$arr2_dup=array_merge($arr2_dup,$arr_dup);
*/
		$arr2_dup=array_merge($arr2_dup,$arr_dup);
		$arr2_dup=array_merge($arr2_dup,$results_duplicates);
	}
	//echo "<br>arr2_dup==";
	//print_r($arr2_dup);
}


	 $tbheaders = array();
	 if(!empty($results_duplicates)){
	 	 
	 	$tbheaders[0]='Match %';
	 	$tbheaders[1]='Record ID from File 1';
	 	$tbheaders[2]='Matching Record ID from File 2';
	 	$i=3;
	 	foreach($colistheader as $coheader)
	 	{
	 		$coheader=str_replace("col_","",$coheader);

	 		//$colname_map = DB::table('job_'.$id.'_name_mapping_primary')->where('colid', '=', $coheader)->pluck('colname');
	 		$colmap_sql="Select colname from ax_job_".$id."_name_mapping_primary where colid=".$coheader;
	 		
	 		//echo "<br>".$colmap_sql;
	 		$res_colmap = $conn->query($colmap_sql);
	 		if($res_colmap->num_rows>0)
	 		{
	 			while($row_colmap = $res_colmap->fetch_object())
	 			{
	 				$colname_map=$row_colmap->colname;
	 			}	
	 		}
	 		
	 		//echo '-=-=-='.$colname_map;
	 		if($colname_map!="")
	 			$tbheaders[$i++] = $colname_map;
	 	}
	 	 
	 	 
	 }
	 //echo "<br>headers==";
	// print_r($tbheaders);
	 //exit;
//echo "<br><br>==================MAYBES================================<br>";
	/* $maybes_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r 
	 		LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where (r.result >".$from." AND r.result<=".$to.") GROUP BY primeid ORDER BY primeid LIMIT 10";
	*/
	
	 $maybes_query_main = 'SELECT "" as match,r.primeid as primeid,'.$colistBQ_First.' FROM [datascrub-152522.ax_datascrub.ds_443_results] r '.
	 		'LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A '.
	 		'on (A.id=r.primeid) WHERE  (r.result >'.$from.' AND r.result<='.$to.')'.
	 		'GROUP BY primeid ORDER BY primeid LIMIT 10';
	
	
	
	
	
	// echo "<br>==".$maybes_query_main."<br>";
	 //$prev_maybes_main = DB::select(DB::raw($maybes_query_main));
	 //print_r($prev_maybes_main);
	/* $res_maybes_query_main = $conn->query($maybes_query_main);
	 if( $res_maybes_query_main->num_rows>0)
	 {
	 	while($row_maybes_query_main = $res_maybes_query_main->fetch_object())
	 	{
	 		$prev_maybes_main[]=$row_maybes_query_main;
	 	}*/
	 
	 $results_maybes_main=run_query($projectId, $maybes_query_main);
	 //print_r($results_maybes_main);
	if(count($results_maybes_main)>0)
	{
	 $arr2_may=[];
	 $arr_may=[];
	 foreach($results_maybes_main as $main)
	 {
	 	//echo "<br>main=====<br>";
	 	//print_r($main); 
	 	// exit;
	 	$arr_may=[];
	 	$main->id="";
	 	$main->result="";
	 	$arr_may[]=$main;
	 	if($isfile2 =='' || $isfile2 == NULL){
	 		/*$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";*/
	 		$maybes_query="select yt1.result as result,yt1.primeid as primeid,".$fulllistBQ."
		            from [datascrub-152522.ax_datascrub.ds_443_detail_results] yt1
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A on (A.id=yt1.primeid)
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] B on (B.id=yt1.secondid)
		            where  (yt1.result >".$from." AND yt1.result<=".$to.") and yt1.primeid=".$main['primeid']." and yt1.secondid<>".$main['primeid']."
		            ORDER BY yt1.result DESC LIMIT 5";
	 		
	 		
	 	}else{
	 		/*$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";*/
	 		$maybes_query="select yt1.result as result,yt1.primeid as primeid,".$fulllistBQ."
		            from [datascrub-152522.ax_datascrub.ds_443_detail_results] yt1
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_primary] A on (A.id=yt1.primeid)
		            LEFT JOIN [datascrub-152522.ax_datascrub.ds_443_secondary] B on (B.id=yt1.secondid)
		            where  (yt1.result >".$from." AND yt1.result<=".$to.") and yt1.primeid=".$main['primeid']."
		            ORDER BY yt1.result DESC LIMIT 5";
	 		
	 		
	 		
	 	}
	 	//echo "----------".$maybes_query;
	 	//$prev_maybes = DB::select(DB::raw($maybes_query));
	 	/*$res_maybes_query = $conn->query($maybes_query);
	 	$prev_maybes=array();
	 	if( $res_maybes_query->num_rows>0)
	 	{
	 		while($row_maybes_query = $res_maybes_query->fetch_object())
	 		{
	 			$prev_maybes[]=$row_maybes_query;
	 		}
	 	}*/
	 	//echo $maybes_query;
	 	$results_maybes=run_query($projectId, $maybes_query);
	 	//print_r($results_maybes);
	 //	echo "<br>=============<br>";
	 //	print_r($prev_maybes);
	 	
	 	/*foreach($results_maybes as $d)
	 	{
	 		$arr_may[]=$d;

	 		
	 	}
	 	print_r($arr_may);
	 	
	 	echo "<br>================<br>";
	 	$arr2_may=array_merge($arr2_may,$arr_may);
	 	print_r($arr2_may);
	 	 
	 	echo "<br>-------------<br>";
	 	$arr_may=[];*/
	 //	$arr_may=array_merge($arr_may,$results_maybes);
	 	
	 	//print_r($arr_may);
	 	//echo "<br>=========<br>";
	 	$arr2_may=array_merge($arr2_may,$arr_may);
	 	$arr2_may=array_merge($arr2_may,$results_maybes);
	 	//print_r($arr2_may);
	 	}
	 
	 }
	// echo "<br>=============<br>";
	// print_r($arr2_may);
	 //exit;
	// $results = DB::table('job_'.$id.'_results')->get();
	/*$sql_results="Select * from 'ax_ml_'.$ml_id.'_comparisons'";
	 $res_results = $conn->query($sql_results);
	 if( $res_results->num_rows>0)
	 {
	 	while($row_results = $res_results->fetch_object())
	 	{
	 		$results[]=$row_results;
	 	}
	 }/

	 /*echo "<br><br> total uniques===".$total_uniques;
	 echo "<br><br> total maybes===".$total_maybes;
	 echo "<br><br> total duplicates===".$total_dups;
	 */
	 
	 
	 //$total = $total_uniques[0]->unique + $total_maybes[0]->maybe + $total_dups[0]->duplicate;
	 $total = $total_uniques + $total_maybes + $total_dups;
	 //echo "<br><br> total ===".$total;
		$uper = ($total_uniques == 0 ? 0 : ($total_uniques / $total) * 100);
		$mper = ($total_maybes == 0 ? 0 : ($total_maybes / $total) * 100);
		$dper = ($total_dups == 0 ? 0 : ($total_dups / $total) * 100);

		if(($uper+$mper+$dper) == 0){
			$chartempty = 'true';
		}else{
			$chartempty = 'false';
		}

		$data = array(
				'project' => $project,
				'uniques' => $total_uniques,
				'uniqueper' => $uper,
				'maybes' => $total_maybes,
				'maybeper' => $mper,
				'dups' => $total_dups,
				'dupper' => $dper,
				'preview_duplicates' => $arr2_dup,
				'tbheaders' => $tbheaders,
				'preview_maybes' => $arr2_may,
				'tbmheaders' => $tbheaders,
				'results' => $results,
				'from' => $from,
				'to' => $to,
				'ischartempty' => $chartempty,
				'help' => $help
		);
		
return $data;
		//$results = DB::table('job_21_results')->get();

		
}
function reanalyzeAction($id,$ml_id,$conn,$from,$to){


	//$project = Project::find($id);
	$sql_project="Select * from ax_projects where id=".$id;
	if( $res_sql_project = $conn->query($sql_project))
	{
		while($row_sql_project = $res_sql_project->fetch_assoc())
		{
			$project = $row_sql_project['project_name'];
	
		}
	}
	/*	$files = glob('project/'.$id.'/files/*'); // get all file names

	foreach($files as $file){ // iterate files
	if(is_file($file))
		unlink($file); // delete file
		}

		$help = DB::table('help_tooltip')->where('page', '=', 'project_analysis')->pluck('content');
		*/
	$slider_range_sql="update ax_project_slider_values set range_from=".$from." ,range_to=".$to." where project_id=".$id;
	 $conn->query($slider_range_sql);
	
	/*


	if($to==100)
	{
		$to=0.59;
	}
	if($from==0)
	{
		$from=0.01;
	}
	*/
	//var_dump($from);
	//var_dump($to);
	//echo "==reaana==FROM==".$from."===to===".$to;







	//$get_uniques_sql = "SELECT COUNT(A.id) as `unique` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value is null OR B.matching_value <=".$from;
	$get_uniques_sql = "SELECT COUNT(A.id) as `unique` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result is null OR B.result <=".$from;

	//echo "<br>".$get_uniques_sql;

	//	$total_uniques = DB::select(DB::raw($get_uniques));
	if( $get_uniques_res = $conn->query($get_uniques_sql))
	{
		while($get_uniques_row = $get_uniques_res->fetch_assoc())
		{
			$total_uniques = $get_uniques_row['unique'];
				
		}
	}

	//echo "<br>uniques==".$total_uniques;


	//$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value >".$from." AND B.matching_value <=".$to;
	$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$from." AND B.result <=".$to;

	//echo "<br>".$get_maybes_sql;
	//$total_maybes = DB::select(DB::raw($get_maybes));

	//echo "------------".$total_maybes[0]->maybe;

	if( $get_maybes_res = $conn->query($get_maybes_sql))
	{
		while($get_maybes_row = $get_maybes_res->fetch_assoc())
		{
			$total_maybes = $get_maybes_row['maybe'];

		}
	}

	//echo "<br>maybes==".$total_maybes;



	$get_dups_sql= "SELECT COUNT(A.id) as `duplicate` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$to;

	//echo "<br>".$get_dups_sql;
	//$total_maybes = DB::select(DB::raw($get_maybes));

	//echo "------------".$total_maybes[0]->maybe;

	if( $get_dups_res = $conn->query($get_dups_sql))
	{
		while($get_dups_row = $get_dups_res->fetch_assoc())
		{
			$total_dups = $get_dups_row['duplicate'];

		}
	}

	//echo "<br>dups==".$total_dups;



	$sql_check_file2="select file_2 from ax_project_files where project_id=".$id;
	if( $res_check_file2 = $conn->query($sql_check_file2))
	{
		while($row_check_file2 = $res_check_file2->fetch_assoc())
		{
			$isfile2 = $row_check_file2['file_2'];
				
		}
	}
	//echo "<br>isfile2===".$isfile2;

	//$isfile2 = DB::table('project_files')->where('project_id', '=', $id)->pluck('file_2');

	//START FETCHING FIELD NAMES SELECTED BY USER TO MATCH

	//$fetchcols = 'job_'.$id.'_process';
	//$columns = DB::table($fetchcols)->where('id', '>', 0)->get();//

	$sql_cols="select * from ax_job_".$id."_process";
	if( $res_cols = $conn->query($sql_cols))
	{
		while($row_cols = $res_cols->fetch_object())
		{
			$columns[]=$row_cols;
		}

	}

	//print_r($columns);
	//exit;
	$colistheader[]="";
	$co2listheader[]="";
	$colist="A.id ,";
	$co2list="B.id ,";
	$columnlist="id,";
	foreach($columns as $col){
		$colistheader[]="col_".$col->colid;
		$colist.="A.`col_".$col->colid."`,";
		if($col->col2id){
			$co2listheader[]="col_".$col->col2id;
			$co2list.="B.`col_".$col->col2id."`,";
		}
			
	}

	//$fulllist=$colist.substr($co2list,0,strlen($co2list)-1);
	$fulllist=substr($co2list,0,strlen($co2list)-1);
	$colist=substr($colist,0,strlen($colist)-1);
	//echo "<br><br>colistheader=======";

	//echo $colist."==";
	//print_r($colistheader);
	/*echo "<br>=======<br>";
	 echo $co2list."==";
	 print_r($co2listheader);*/
	//echo "<br>".$fulllist;
	//END OF FETCHING FIELD NAMES SELECTED BY USER TO MATCH





	$duplicates_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where result>".$to." GROUP BY primeid ORDER BY primeid LIMIT 10";
	//echo "<br>dqm==".$duplicates_query_main;
	//$prev_duplicates_main = DB::select(DB::raw($duplicates_query_main));
	//print_r($prev_duplicates_main);
	$res_duplicates_query_main = $conn->query($duplicates_query_main);
	if( $res_duplicates_query_main->num_rows>0)
	{
		while($row_duplicates_query_main = $res_duplicates_query_main->fetch_object())
		{
			$prev_duplicates_main[]=$row_duplicates_query_main;
		}


		$arr2_dup=[];
		$arr_dup=[];
		foreach($prev_duplicates_main as $main)
		{
			$arr_dup=[];
			$main->id="";
			$main->result="";
			$arr_dup[]=$main;
			//echo "<br><br><br>=======================<br>";
			//print_r($main);
			//$arr_dup[]=$main;
			//echo "<br>".$main->primeid."<br>";

			if($isfile2 =='' || $isfile2 == NULL){
				//ONE FILE
				$duplicates_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";
			}else{
				$duplicates_query="select yt1.matching_value,yt1.primary_id,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";
			}

			if( $res_duplicates_query = $conn->query($duplicates_query))
			{
				$prev_duplicates=array();
				while($row_duplicates_query = $res_duplicates_query->fetch_object())
				{
					$prev_duplicates[]=$row_duplicates_query;
				}
			}
			//echo "=dq=".$duplicates_query."<br><br>";
			//$prev_duplicates = DB::select(DB::raw($duplicates_query));
			//echo "<br>===================<br>";
			//print_r($prev_duplicates);
			//$prev_duplicates=array_merge($main,$prev_duplicates);
			//echo "=====================";

			foreach($prev_duplicates as $d)
			{
				$arr_dup[]=$d;

				/*foreach($d as $f=>$t)
				 echo "<br>".$f."=========".$t;*/
			}
			//print_r($arr_dup);
			$arr2_dup=array_merge($arr2_dup,$arr_dup);

		}
		//echo "<br>arr2_dup==";
		//print_r($arr2_dup);
	}


	$tbheaders = array();
	if(!empty($prev_duplicates)){

		$tbheaders[0]='Match %';
		$tbheaders[1]='Record ID from File 1';
		$tbheaders[2]='Matching Record ID from File 2';
		$i=3;
		foreach($colistheader as $coheader)
		{
			$coheader=str_replace("col_","",$coheader);

			//$colname_map = DB::table('job_'.$id.'_name_mapping_primary')->where('colid', '=', $coheader)->pluck('colname');
			$colmap_sql="Select colname from ax_job_".$id."_name_mapping_primary where colid=".$coheader;

			//echo "<br>".$colmap_sql;
			$res_colmap = $conn->query($colmap_sql);
			if($res_colmap->num_rows>0)
			{
				while($row_colmap = $res_colmap->fetch_object())
				{
					$colname_map=$row_colmap->colname;
				}
			}

			//echo '-=-=-='.$colname_map;
			if($colname_map!="")
				$tbheaders[$i++] = $colname_map;
		}


	}
	//echo "<br>headers==";
	// print_r($tbheaders);
	//exit;
	//echo "<br><br>==================MAYBES================================<br>";
	$maybes_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where (r.result >".$from." AND r.result<=".$to.") GROUP BY primeid ORDER BY primeid LIMIT 10";
	// echo "<br>==".$maybes_query_main."<br>";
	//$prev_maybes_main = DB::select(DB::raw($maybes_query_main));
	//print_r($prev_maybes_main);
	$res_maybes_query_main = $conn->query($maybes_query_main);
	if( $res_maybes_query_main->num_rows>0)
	{
		while($row_maybes_query_main = $res_maybes_query_main->fetch_object())
		{
			$prev_maybes_main[]=$row_maybes_query_main;
		}
	 //	print_r($prev_maybes_main);
	 $arr2_may=[];
	 $arr_may=[];
	 foreach($prev_maybes_main as $main)
	 {


	 	$arr_may=[];
	 	$main->id="";
	 	$main->result="";
	 	$arr_may[]=$main;
	 	if($isfile2 =='' || $isfile2 == NULL){
	 		$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";
	 	}else{
	 		$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";
	 	}
	 	//echo "----------".$maybes_query;
	 	//$prev_maybes = DB::select(DB::raw($maybes_query));
	 	$res_maybes_query = $conn->query($maybes_query);
	 	$prev_maybes=array();
	 	if( $res_maybes_query->num_rows>0)
	 	{
	 		while($row_maybes_query = $res_maybes_query->fetch_object())
	 		{
	 			$prev_maybes[]=$row_maybes_query;
	 		}
	 	}
	 	//	echo "<br>=============<br>";
	 	//	print_r($prev_maybes);
	 	 
	 	foreach($prev_maybes as $d)
	 	{
	 		$arr_may[]=$d;

	 		/*foreach($d as $f=>$t)
	 		 echo "<br>".$f."=========".$t;*/
	 	}
	 	//print_r($arr_dup);
	 	$arr2_may=array_merge($arr2_may,$arr_may);

	 }

	}
	// echo "<br>=============<br>";
	// print_r($arr2_may);
	//exit;
	// $results = DB::table('job_'.$id.'_results')->get();
	$sql_results="Select * from 'ax_ml_'.$ml_id.'_comparisons'";
	$res_results = $conn->query('job_'.$id.'_results');
	if( $res_results->num_rows>0)
	{
		while($row_results = $res_results->fetch_object())
		{
			$results[]=$row_results;
		}
	}

	/*echo "<br><br> total uniques===".$total_uniques;
	 echo "<br><br> total maybes===".$total_maybes;
	 echo "<br><br> total duplicates===".$total_dups;
	 */


	//$total = $total_uniques[0]->unique + $total_maybes[0]->maybe + $total_dups[0]->duplicate;
	$total = $total_uniques + $total_maybes + $total_dups;
	//echo "<br><br> total ===".$total;
	$uper = ($total_uniques == 0 ? 0 : ($total_uniques / $total) * 100);
	$mper = ($total_maybes == 0 ? 0 : ($total_maybes / $total) * 100);
	$dper = ($total_dups == 0 ? 0 : ($total_dups / $total) * 100);

	if(($uper+$mper+$dper) == 0){
		$chartempty = 'true';
	}else{
		$chartempty = 'false';
	}

	$data = array(
			'project' => $project,
			'uniques' => $total_uniques,
			'uniqueper' => $uper,
			'maybes' => $total_maybes,
			'maybeper' => $mper,
			'dups' => $total_dups,
			'dupper' => $dper,
			'preview_duplicates' => $arr2_dup,
			'tbheaders' => $tbheaders,
			'preview_maybes' => $arr2_may,
			'tbmheaders' => $tbheaders,
			'results' => $results,
			'total' => $total,
			'from' => $from,
			'to' => $to,
			'ischartempty' => $chartempty,
			'help' => $help
	);

	return $data;
	//$results = DB::table('job_21_results')->get();


}

//print_r($data);
//print_r($data['preview_maybes']);
//echo "-===-===-===".$data['maybes'];

/*if(isset($_POST['to']) && isset($_POST['from']) )
{
//	$data=reanalyzeAction($current_projectid,$ml_id,$conn,$_POST['from'],$_POST['to']);
//	require_once("/reanalyze-results.php");
}
else {*/
	$data=analysisAction($current_projectid,$ml_id,$conn);
	require_once("templates/header.html");
	require_once("templates/nav.html");
	require_once("templates/resultsBQ.php");
	require_once("templates/footer.html");
//}

