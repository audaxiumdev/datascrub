<?php


echo "Inside Localminmax";
/*
 * 	
Actually my previous algorithm can be modified to get all the maxima in O(log n) time. I tested that it works great for all the input provided. Please let me know your feedback

public class LocalMaximas {

@Test
public void test () {
    System.out.println("maximas: please modify code to handle if array size is <= 2");

    int []a = {5,8,10,25,6,3,44,51,55,56,57,58,34,5,59,2};
    localMaximas(a);

    int []b = {9,7,2,8,5,6,3,4, 2}; //9,8,6,4
    localMaximas(b);

    int [] c= {15, 13, 12, 18, 19, 20, 7, 6, 5, 4, 3, 2, 1};//15,20
    localMaximas(c);
}

public  void localMaximas (int [] a) {
    System.out.println("\n\n");
    if(isMaxima(a,0)) {
        System.out.println(a[0]);
    }
    if(isMaxima(a,a.length-1)) {
        System.out.println(a[a.length-1]);
    }
    localMaximas(a,0,a.length-1);
}

int localMaximas(int []a,int low, int high) {
    int mid = (low+high)/2;
    if(high-low > 3) {     // more than 4 items in currently  divided array
        if(isMaxima(a,mid)) {
            System.out.println(a[mid]);
        }   
        localMaximas(a,low, mid);
        localMaximas(a,mid, high);
    }
    else if(high-low == 3){ //exactly 4 items in currently  divided array
        localMaximas(a,low, mid+1);
        localMaximas(a,mid, high);
    }   
    else if((high-low == 2) && (isMaxima(a,low+1))) {
        System.out.println(a[low+1]);
    }
    return 0;
}

int maxof(int []a, int i, int j) {
    if(a[i] <a[j]) {
        return j;
    }
    else {
        return i;
    }
}

boolean isMaxima(int []a ,int mid) {
    if(mid == 0) {
        if(maxof(a, mid, mid+1) == mid) {
            return true;
        }
        else {
            return false;
        }
    }
    else if(mid==a.length-1) {
        if(maxof(a,mid,mid-1) == mid) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if((maxof(a, mid, mid+1) == mid) && (maxof(a, mid, mid-1) == mid)) {
            return true;
        }
        else {
            return false;
        }           
    }
}
}
 */
/*
test();


	 function test() {
		print("maximas: please modify code to handle if array size is <= 2");

		exit;
		$a = array(5,8,10,25,6,3,44,51,55,56,57,58,34,5,59,2);
		localMaximas($a);

		$b = array(9,7,2,8,5,6,3,4, 2); //9,8,6,4
		localMaximas($b);

		$c= array(15, 13, 12, 18, 19, 20, 7, 6, 5, 4, 3, 2, 1);//15,20
		localMaximas($c);
	}

	  function localMaximas ($a) {
		print("\n\n");
		if(isMaxima($a,0)) {
			print($a[0]);
		}
		if(isMaxima($a,count($a)-1)) {
			print($a[count($a)-1]);
		}
		localMaximas($a,0,count($a)-1);
	}

	function localMaximas2($a,$low, $high) {
		$mid = ($low+$high)/2;
		if($high-$low > 3) {     // more than 4 items in currently  divided array
			if(isMaxima($a,$mid)) {
				print($a[$mid]);
			}
			localMaximas($a,$low, $mid);
			localMaximas($a,$mid, $high);
		}
		else if($high-$low == 3){ //exactly 4 items in currently  divided array
			localMaximas($a,$low, $mid+1);
			localMaximas($a,$mid, $high);
		}
		else if(($high-$low == 2) && (isMaxima($a,$low+1))) {
			print($a[$low+1]);
		}
		return 0;
	}

	function maxof($a, $i, $j) {
		if($a[$i] <$a[$j]) {
			return $j;
		}
		else {
			return $i;
		}
	}

	function isMaxima($a ,$mid) {
		if($mid == 0) {
			if(maxof($a, $mid, $mid+1) == $mid) {
				return true;
			}
			else {
				return false;
			}
		}
		else if($mid==(count($a)-1)) {
			if(maxof($a,$mid,$mid-1) == $mid) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			if((maxof($a, $mid, $mid+1) == $mid) && (maxof($a, $mid, $mid-1) == $mid)) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	
	*/

//===============================================================================================================================
/*
Mine's similar to jprofitt's

but I divided them into peaks and valleys, so I can do some more stuff with it.

I think his loop is much more cleaner than mine is, but I just wanted to test it out for myself.
Don't judge meeeeee

This script just plots the points out and selects the peaks and valleys and give them green and red respectively. See it as a visual aid. :P

graph

<?php
*/
//$plot = array(10,9,8,8,9,7,6,5,4,6,7,8,11,10,12,14,16,20,30,29,28,29,27,25,20,18,15,10,8,5,4,1);

$res = local_extremes($plot);
echo "<br>peaks======";
print_r($res['peaks']);
echo "<br>valleys======";
print_r($res['valleys']);
echo "<br>peak_keys======";
print_r($res['peak_keys']);
echo "<br>valley_keys======";
print_r($res['valley_keys']);







function local_extremes(array $array){
    $peaks = array();
    $valleys = array();

    $peak_keys = array();
    $valley_keys = array();

    for($i = 0; $i < count($array); $i++){
    	echo "<br>(i-1)". $array[$i-1]."==i==". $array[$i]."==i+1==". $array[$i+1]."<br>";
        $more_than_last = $array[$i] > $array[$i-1];
        $more_than_next = $array[$i] > $array[$i+1];
		echo "<br>morethanlast==".$more_than_last."==more_than_next==".$more_than_next."<br>";
        $next_is_equal = $array[$i] == $array[$i+1];

        if($next_is_equal) continue;

        if($i == 0){
        	echo "<br>condition first==".$array[$i];
            if($more_than_next){
            	
            	echo "<br>condition first==morethannext".$array[$i];
                $peaks[] = $array[$i];
                
                
                $peak_keys[] = $i;
            }else{
            	echo "<br>condition first==NOTmorethannext".$array[$i];
                $valleys[] = $array[$i];
                $valley_keys[] = $i;
            }
        }elseif($i == (count($array)-1)){
        	echo "<br>condition last";
            if($more_than_last){
            	echo "<br>condition last===morethanlast";
                $peaks[] = $array[$i];
                
                $peak_keys[] = $i;
            }else{
            	echo "<br>condition last===NOTmorethanlast";
                $valleys[] = $array[$i];
                $valley_keys[] = $i;
            }
        }else{
            if($more_than_last && $more_than_next){
            	
            	
            	echo "<br>condition morethanlast and more than next";
                $peaks[] = $array[$i];
                $peak_keys[] = $i;
                
            }elseif(!$more_than_last && !$more_than_next){
            	echo "<br>condition NOTmorethanlast and NOTmore than next";
                $valleys[] = $array[$i];
                $valley_keys[] = $i;
            }
        }
        echo "<br>=====peaks array==<br>";
        print_r($peaks);
        echo "<br>=====valleys array==<br>";
        print_r($valleys);
        
    }
    

    return array("peaks" => $peaks, "valleys" => $valleys, "peak_keys" => $peak_keys, "valley_keys" => $valley_keys);
}
?>

<style type="text/css">
    .container{
        position: absolute;
    }

    .point{
        position: absolute;
        width: 4px;
        height: 4px;
        background: black;
    }

    .extreme{
        position: absolute;
        top: 5px;
    }

    .extr_low{
        background: red;
    }

    .extr_high{
        background: green;
    }
</style>

<?php
exit;
//Plot
echo "<div class='container'>";
foreach($plot as $key => $point){
    $left = ($key*10);
    $top = 400 - ($point*10);

    if(in_array($key, $res['peak_keys']) || in_array($key, $res['valley_keys'])){
        $extreme = "<div class='extreme'>$point</div>";
    }else{
        $extreme = "";
    }

    if(in_array($key, $res['peak_keys'])){
        $xc = "extr_high";
    }elseif(in_array($key, $res['valley_keys'])){
        $xc = "extr_low";
    }else{
        $xc = "";
    }

    echo "<div class='point $xc' style='left: ".$left."px; top: ".$top."px;'>$extreme</div>";
}
echo "</div>";

?>

<table>
    <tr>
        <th>&nbsp;</th>
        <th>Valley</th>
        <th>Peak</th>
    </tr>
    <tr>
        <th>Lowest</th>

        <td><?php echo min($res['valleys']); ?></td>
        <td><?php echo min($res['peaks']); ?></td>
    </tr>
    <tr>
        <th>Highest</th>
        <td><?php echo max($res['valleys']); ?></td>
        <td><?php echo max($res['peaks']); ?></td>
    </tr>
</table>
