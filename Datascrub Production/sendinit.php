<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



$current_projectid=get_id();

if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}


$ml_id="";
$status="";
$sql_chk_ml_project="Select * from ax_ml where project_id=".$current_projectid;
//echo $sql_chk_ml_project;
if($res_chk_ml_project=$conn->query($sql_chk_ml_project))
{
	if($res_chk_ml_project->num_rows>0)
	{
		
		while($row_chk_ml_project=$res_chk_ml_project->fetch_assoc())
		{
			$ml_id=$row_chk_ml_project['id'];
			//echo $ml_id;
			$status=$row_chk_ml_project['status'];
		}
	}
}


$sql="select * from ax_project_files where project_id='".$current_projectid."'";
//echo $sql;
$res = $conn->query($sql);
$file1="";
$file2="";

while($row=$res->fetch_assoc())
{

	$file1=$row['file_1'];
	$file2=$row['file_2'];
}

$is_data_linkage=0;  //0 for one file and 1 for two files
if($file2!="")
{
	$is_data_linkage=1;	
}
  
$sampling_dbs="heuristic";  // 3 types of sampling data blocking strategies:1. heuristic  2. full  3. covering
$sampling_data_phase="raw"; // 3 types of sampling data phase strategies:1. raw  2. full  ; here blocking is being done from raw table
$sampling_max_count=20000;  // null means no limit
$comparison_strategy="jaro";  // 4 types of comparison strategies:1. jaro  2. levenshtein  3. demareau levenshtein   4. hamming
$comparison_data_phase="analysis"; // comparisons are being done from analyzed data
$al_sampling_method="balanced";  // 4 types of sampling methods:1. entropy  2. al(active learner)  3. sm(smallest margin)  4. lc(least confidence) 5. balanced
$clustering_dbs="covering";  //covers the blocking methods
$status="New";   //1. New 2. Initializing  3. Initialized 4. Clustering 5. Clustered
$clustering_max_count=null;  //null means no limit  should be atkeast in billions

$ml_id="";
$sql_chk_ml_project="Select * from ax_ml where project_id=".$current_projectid;
//echo $sql_chk_ml_project;
if($res_chk_ml_project=$conn->query($sql_chk_ml_project))
{
	if($res_chk_ml_project->num_rows>0)
	{

		while($row_chk_ml_project=$res_chk_ml_project->fetch_assoc())
		{
			$ml_id=$row_chk_ml_project['id'];
			//echo $ml_id;
		}



			
		//	$sql_update_ml_project="update ax_ml set status='".$status."' where id=".$ml_id;
		$sql_delete_ml_project="delete from ax_ml  where id=".$ml_id;

		//echo $sql_update_ml_project;
		if($conn->query($sql_delete_ml_project))
		{
				
				
			$dropsql="DROP TABLE IF EXISTS `ax_ml_".$ml_id."_comparisons`";
			//echo "<br>".$dropsql;
			$conn->query($dropsql);
			$sql="DROP TABLE IF EXISTS `ax_ml_".$ml_id."_training`";
			//echo "<br>".$dropsql;
			$conn->query($dropsql);
			$dropsql="DROP TABLE IF EXISTS `ax_job_".$current_projectid."_results`";
			//echo "<br>".$dropsql;
			$conn->query($dropsql);
				
			$conn->query("Update ax_projects set status=4 where id=".$current_projectid);
			//header("Location:/initializeml/".$current_projectid);

		}
		else {
			echo $conn->error;
		}
	}
}

$sql_create_ml_project="Insert into ax_ml(project_id,is_data_linkage,sampling_dbs,sampling_data_phase,sampling_max_count,comparison_strategy,comparison_data_phase,al_sampling_method,clustering_dbs,status)
values($current_projectid,$is_data_linkage,'".$sampling_dbs."','".$sampling_data_phase."',$sampling_max_count,'".$comparison_strategy."','".$comparison_data_phase."','".$al_sampling_method."','".$clustering_dbs."','".$status."')";

//echo $sql_create_ml_project;
if($conn->query($sql_create_ml_project))
{
	$ml_id=$conn->insert_id;
	//echo $ml_id;

	$dropsql="DROP TABLE IF EXISTS `ax_ml_".$ml_id."_comparisons`";
	//echo "<br>".$dropsql;
	$conn->query($dropsql);
	$sql="DROP TABLE IF EXISTS `ax_ml_".$ml_id."_training`";
	//echo "<br>".$dropsql;
	$conn->query($dropsql);
	$dropsql="DROP TABLE IF EXISTS `ax_job_".$current_projectid."_results`";
	//echo "<br>".$dropsql;
	$conn->query($dropsql);
	$conn->query("Update ax_projects set status=4 where id=".$current_projectid);
	//header("Location:/initializeml/".$current_projectid);

}
else {
	//echo $conn->error;
}
























$service_url = "http://ml.datascrub-152522.appspot.com/portal/ml/".$ml_id."/init";
$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
//echo "curl---response=".$curl_response;
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);

	echo "<h2 style='margin-left:300px;margin-top:200px;'>There was some problem initializing the Learning .<br><br>So, you are getting redirected to Field Select page.</h2>";

	?>
							<meta http-equiv="refresh" content="5;url=/mapfields/<?=$current_projectid;?>"> 
						<?php	
						die('error occured during curl exec. Additional info: ' . var_export($info));
						exit;
}
else {
	curl_close($curl);
	
	header("Location:/initializeAL/".$current_projectid);
	exit;
}