<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



/*
echo "====post==";
print_r($_POST);
echo "====request==";
print_r($_REQUEST);
echo "====get==";
print_r($_GET);

*/



$current_projectid=get_id();

//echo "----------------".$current_projectid;
if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}
checkProjectCompleted($current_projectid,$conn);
//echo "===".$_SESSION['userid'];
if(!isset($_SESSION['userid']) or $_SESSION['userid']=="")
{
	echo "<h1>Your session has been expired . Please Login again</h1>";
	exit;	
}
else 
{
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

}
//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}





$sql_ml_id="select id from ax_ml where project_id=".$current_projectid;
if( $ml_id_res = $conn->query($sql_ml_id))
{
	while($ml_id_row = $ml_id_res->fetch_assoc())
	{
		$ml_id = $ml_id_row['id'];

	}
}

$chartdata="";
$sql_chart_data="select round(matching_value,2) as matching_value,count(id) as cnt from ax_datascrub.ax_ml_".$ml_id."_comparisons group by round(matching_value,2)";
$res_chart_data=$conn->query($sql_chart_data);
$i=0;
if( $res_chart_data->num_rows>0)
{
	$total_rows=$res_chart_data->num_rows;
	while($row_chart_data = $res_chart_data->fetch_assoc())
	{
		$chartdata .= "[".$row_chart_data['matching_value'].",".$row_chart_data['cnt']."]";
		if($i<($total_rows-1))
		{
			$chartdata.=",";
		}
		$i++;
}
}

?>
<html>
  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Matching Values');
        data.addColumn('number', 'Count');
        data.addRows([

					<?=$chartdata;?>	

        ]);
/*
 *  ['Mushrooms', 3],
 ['Onions', 1],
 ['Olives', 1],
 ['Zucchini', 1],
 ['Pepperoni', 2]
 [0.24,	92],
						[0.25,	565],
						[0.26,	100],
						[0.27,	29],
						[0.28,	546],
						[0.29,	2932],
						[0.3,	9508],
						[0.31,	18891],
						[0.32,	19781],
						[0.33,	10564],
						[0.34,	6642],
						[0.35,	12414],
						[0.36,	22041],
						[0.37,	29475],
						[0.38,	37306],
						[0.39,	50706],
						[0.4,	63758],
						[0.41,	63799],
						[0.42,	53104],
						[0.43,	42820],
						[0.44,	43250],
						[0.45,	49342],
						[0.46,	51655],
						[0.48,	28101],
						[0.49,	14678],
						[0.5,	6361],
						[0.51,	2328],
						[0.52,	812],
						[0.53,	380],
						[0.54,	136],
						[0.55,	65],
						[0.56,	26],
						[0.57,	16],
						[0.58,	9],
						[0.79,	11],
						[0.63,	380],
						[0.84,	136],
						[0.25,	65],
						[0.56,	26],
						[0.97,	16],
						[0.98,	9000],
						[0.59,	11],
						[0.6,	9],
						[0.61,	2],
						[0.62,	1]
 
 */
        // Set chart options
        var options = {'title':'Match Result',
                       'width':1500,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    <style>
    .chart-txt
    {
    margin-left:200px;
    margin-top:50px;
    width:1000px;
    height:80px;
    border:0px solid grey;
    text-align:middle;
    font-size:18px;
    font-face:bold;
    }
    </style>
  </head>

  <body>
  <div class="chart-txt">Barchart for the project <b><?=$current_projectname;?></b></div>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>
</html>