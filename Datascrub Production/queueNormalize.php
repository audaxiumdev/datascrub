<?php
//error_log("logging error",3,"logs/log_error.txt");
//error_log("You messed up!", 3, "/logs/errors.log");
//openlog("myScriptLog", LOG_PID | LOG_PERROR, LOG_LOCAL0);
//syslog( LOG_ERR , "You messed up!" );
//closelog();
//exit;
ini_set('display_errors',1);
ini_set('log_errors',1);
ini_set('error_log',dirname(__FILE__).'/logs/error.log');
//error_log("You messed up!", 3, dirname(__FILE__).'/logs/log_error.txt');
syslog( LOG_ERR , "Testing SYSLOG in normalization!" );
//error_log("fffffff");
error_reporting(E_ALL);

/*openlog("myScriptLog", LOG_PID | LOG_PERROR, LOG_LOCAL0);
syslog(LOG_INFO, 'Authorized access');
closelog();*/
//exit;
require_once("config/dbconnect.php");
$projectid=$_POST['projectid'];
$tabletype=$_POST['tabletype'];
$secondfile=$_POST['secondfile'];
$colist=$_POST['colist'];
$co2list=$_POST['co2list'];
$start=$_POST['start'];
$div=$_POST['div'];
/*$projectid=325;
$tabletype="primary";
$secondfile="";
$colist="col_1,col_2,col_3,col_4,col_5,";
$co2list="col_8,col_1,col_2,col_5,col_6,";
*/
if($tabletype=="primary")
{
	InsertIntoAnalysisTablePrimary($projectid,$secondfile,$colist,$co2list,$conn,$start,$div);
}
if($tabletype=="secondary")
{
	InsertIntoAnalysisTableSecondary($projectid,$secondfile,$colist,$co2list,$conn,$start,$div);
}


  function InsertIntoAnalysisTablePrimary($projectid,$secondfile,$colist,$co2list,$conn,$start,$div)
{
	$memcache= new Memcache;
	$dropsql="DROP TABLE IF EXISTS ax_job_".$projectid."_primary_analysis_dataTypes";
	//echo "<br>".$dropsql;
	$conn->query($dropsql);
	$sql_create_datatype_table="Create table ax_job_".$projectid."_primary_analysis_dataTypes(
			id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			columnName varchar(255) NOT NUll,
			dataType varchar(255) NOT NUll)";
	$chkInsertdatatypes=false;
	$chkInsertDtypes=false;
	
	$insertrows_array=array();
		$insertrows_counter=0;
		$batch_size=500;
		$batch_no=0;
		$loopsize=500;
	
	$conn->query($sql_create_datatype_table);
	
	//log::info("colist==".$colist);
	$colist1=substr($colist,0,strlen($colist)-1);
	$colprimarray=explode(",",$colist1);
	$col_name_type=[];
	$i=0;
	
	foreach($colprimarray as $c)
	{
		$column=str_replace("col_","",$c);
		$column=str_replace("`","",$column);
		$sql="SELECT type from `ax_job_".$projectid."_process` WHERE colid=".$column;
		//echo $sql;
		error_log($sql);
		$result=$conn->query($sql);
		while($type= $result->fetch_assoc())
		{
		
			//print_r($type);
		//echo $sql."====type==".$type['type'];
		//error_log("TYPE OF ".$c."=".$type[0]->type);
		
		$col_name_type[$i]['name']=str_replace("`","",$c);
		$col_name_type[$i]['type']=$type['type'];
		$i++;
		}
	}

	//print_r($col_name_type);
	/*	foreach($col_name_type as $col)
		{
			
		foreach($col as $k=>$c)
		{
		LOg::info($k."===".$c);
		}
		}*/
	$primarys=[];
	$primary_table = 'ax_job_'.$projectid.'_primary';
	/*$Total_primarys=0;
	$primarys_count_result = $conn->query("SELECT count(id) as count FROM $primary_table ");
	while($row_count_primarys= $primarys_count_result->fetch_assoc())
	{
		$Total_primarys=$row_count_primarys['count'];
	}
	$loop_primarys=0;
	$loop_counter=0;
	while($loop_primarys<=$Total_primarys)
	{*/
	
		$primarys_result = $conn->query("SELECT $colist id,MASHFIELD FROM $primary_table LIMIT $start,$div");
	error_log("SELECT $colist id,MASHFIELD FROM $primary_table LIMIT $start,$div");
	//$primarys_result = $conn->query("SELECT $colist id,MASHFIELD FROM $primary_table limit 50000");
	while($row_primarys= $primarys_result->fetch_assoc())
	{
		$primarys[]=$row_primarys;
	
	}
	$col_name_values=[];
	$col_datatypes=[];
	$dt=0;
	$j=0;
	//commented to check if useful    $result=$conn->query($sql);
	
	foreach($primarys as $prim)
	{
		$primeid=$prim['id'];
		foreach($col_name_type as $col)
		{
			$type="";
			$name="";
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);
				if($k=="type")
				{
					$type=$c;
				}
				elseif($k=="name")
				{
					$name=$c;
				}

			}
			//log::info($type."====".$name);
			//log::info($prim[$name]);
			$prim[$name]=utf8_decode($prim[$name]);
			if($type==0)
			{
					
					
				$columnname="prim_text_".$name;
				//$$columnname=$prim[$name];
				//Log::info($columnname."=========".$$columnname);
				$cleanval= cleanField($prim[$name],'text',$conn);
					
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				
				if(!$chkInsertdatatypes)
				{
					$col_datatypes[$dt]['name']=$columnname;
					$col_datatypes[$dt]['type']="Text";
					$dt++;
				}
				
				
				$j++;
					
				/*$columnname="prim_text_wordcount_".$name;
				 //$$columnname=str_word_count($prim[$name]);
				 //Log::info($columnname."=========".$$columnname);
				 	
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
			}
			elseif($type==1)
			{
				
				$columnname="prim_number_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($prim[$name],'number',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Number";
				$dt++;
				}
				$j++;
			}
			elseif($type==2)
			{
				//$col_prim[]="prim_email_col_".$file1colid;
				$cleanval= cleanField($prim[$name],'email',$conn);
					
				$columnname="prim_email_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email";
				$dt++;
				}
				$j++;
					
				//$col_prim[]="prim_email_account_col_".$file1colid;
				$columnname="prim_email_account_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'account',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email Account";
				$dt++;
				}
				$j++;
					
				//$col_prim[]="prim_email_domain_col_".$file1colid;
				$columnname="prim_email_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'domain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email Domain";
				$dt++;
				}
				$j++;
					
				/*$columnname="prim_email_isfreeemail_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'isfreeemail',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email IsFreeAccount";
				$dt++;
				}
				$j++;*/
			}
			elseif($type==3)
			{
				//$col_prim[]="prim_postal_col_".$file1colid;
				$columnname="prim_postal_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($prim[$name],'postal',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Postal";
				$dt++;
				}
				$j++;
					
			}
			elseif($type==4)
			{
				$origval=$prim[$name];
				$cleanval= cleanField($prim[$name],'phone',$conn);
					
				//$col_prim[]="prim_phone_col_".$file1colid;
				$columnname="prim_phone_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone";
				$dt++;
				}
				$j++;

				//$col_prim[]="prim_phone_countrycode_col_".$file1colid;
				/*$columnname="prim_phone_countrycode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'countrycode',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Country Code";
				$dt++;
				}
				$j++;

				//$col_prim[]="prim_phone_areacode_col_".$file1colid;
				$columnname="prim_phone_areacode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'areacode',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Area Code";
				$dt++;
				}
				$j++;
					*/
				//$col_prim[]="prim_phone_no_col_".$file1colid;
				$columnname="prim_phone_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'no',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Number";
				$dt++;
				}
				$j++;
				

				//$col_prim[]="prim_phone_ext_col_".$file1colid;
				$columnname="prim_phone_ext_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'ext',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Extension";
				$dt++;
				}
				$j++;

			}
			elseif($type==5)
			{
				$cleanval= cleanField($prim[$name],'company',$conn);
				//$col_prim[]="prim_company_col_".$file1colid;
				$columnname="prim_company_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company";
				$dt++;
				}
				$j++;
					
				/*$columnname="prim_company_withoutsuffix_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterCompany($cleanval,'suffix',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company Without Suffix";
				$dt++;
				}
				$j++;*/
					
				$columnname="prim_company_abbr_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterCompany($cleanval,'abbr',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company Abbreviation";
				$dt++;
				}
				$j++;
			}
			elseif($type==6)
			{
				$cleanval= cleanField($prim[$name],'address',$conn);
				//$col_prim[]="prim_address_col_".$file1colid;
				$columnname="prim_address_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address";
				$dt++;
				}
				
				$j++;
					
				//$col_prim[]="prim_address_no_col_".$file1colid;
				/*$columnname="prim_address_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterAddress($cleanval,'no',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address House No";
				$dt++;
				}
				$j++;
				*/	
				//$col_prim[]="prim_address_streetname_col_".$file1colid;
				/*$columnname="prim_address_streetname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterAddress($cleanval,'streetname',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address Street Name";
				$dt++;
				}
				$j++;*/
			}
			elseif($type==7)
			{
				$cleanval= cleanField($prim[$name],'firstname',$conn);
				//$col_prim[]="prim_firstname_col_".$file1colid;
				$columnname="prim_firstname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name";
				$dt++;
				}
				$j++;
					
				//$col_prim[]="prim_firstname_initial_col_".$file1colid;
			/*	$columnname="prim_firstname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name Initial";
				$dt++;
				}
				$initial="";
					
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
				//log::info("strlen==".strlen($cleanval)."==initial==".$initial);
				$col_name_values[$j]['value']=$initial;
				$j++;
					
				//$col_prim[]="prim_firstname_startswith_col_".$file1colid;
				$columnname="prim_firstname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= startswith($cleanval,$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name StartsWith";
				$dt++;
				}	
				$j++;*/
					
				//$col_prim[]="prim_firstname_wordcount_col_".$file1colid;
				/*$columnname="prim_firstname_wordcount_".$name;
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
					
				$columnname="prim_firstname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				//log::info("soundlike=".$soundslike."=for=".strlen($cleanval));
				$col_name_values[$j]['value']=$soundslike;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name SoundsLike";
				$dt++;
				}
				$j++;
			}
			elseif($type==8)
			{
				$cleanval= cleanField($prim[$name],'lastname',$conn);
				//$col_prim[]="prim_lastname_col_".$file1colid;
				$columnname="prim_lastname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name";
				$dt++;
				}
				$j++;
					
			/*	//$col_prim[]="prim_lastname_initial_col_".$file1colid;
				$columnname="prim_lastname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
				$col_name_values[$j]['value']=$initial;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name Initial";
				$dt++;
				}
				$j++;
					
				//$col_prim[]="prim_lastname_startswith_col_".$file1colid;
				$columnname="prim_lastname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= startswith($cleanval,$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name StartsWith";
				$dt++;
				}
				$j++;
				*/	
				//$col_prim[]="prim_lastname_wordcount_col_".$file1colid;
				/*$columnname="prim_lastname_wordcount_".$name;
				 $col_name_values[$j]['name']=$columnname;
				 $col_name_values[$j]['value']=count(explode(" ", $cleanval));
				 $j++;*/
					
				$columnname="prim_lastname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name SoundsLike";
				$dt++;
				}
				$j++;
			}
			elseif($type==9)
			{
				//$col_prim[]="prim_city_col_".$file1colid;
				$columnname="prim_city_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($prim[$name],'city',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="City";
				$dt++;
				}
				$j++;
			}
			elseif($type==10)
			{
				//$col_prim[]="prim_state_col_".$file1colid;
				$columnname="prim_state_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($prim[$name],'state',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="State";
				$dt++;
				}
				$j++;
			}
			elseif($type==11)
			{
				//$col_prim[]="prim_country_col_".$file1colid;
				$columnname="prim_country_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($prim[$name],'country',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Country";
				$dt++;
				}
				$j++;
			}
			elseif($type==12)
			{
				$cleanval= cleanField($prim[$name],'webaddress',$conn);
				$columnname="prim_webaddress_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address";
				$dt++;
				}
				$j++;
				$columnname="prim_webaddress_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterWebAddress($cleanval,'domain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address Domain";
				$dt++;
				}
				$j++;
				$columnname="prim_webaddress_subdomain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterWebAddress($cleanval,'subdomain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address Subdomain";
				$dt++;
				}
				$j++;
			}
			elseif($type==13)
			{
				//$cleanval= cleanField($prim[$name],'date');
				$columnname="prim_date_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$prim[$name];
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date";
				$dt++;
				}
				$j++;
				$columnname="prim_date_db_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=date("Y-m-d H:i:s", strtotime($prim[$name]));
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Formatted";
				$dt++;
				}
				$j++;
				$columnname="prim_date_year_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($prim[$name],'year',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Year";
				$dt++;
				}
				$j++;
				$columnname="prim_date_month_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($prim[$name],'month',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Month";
				$dt++;
				}
				$j++;
				$columnname="prim_date_day_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($prim[$name],'day',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Day";
				$dt++;
				}
				$j++;
				$columnname="prim_date_time_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($prim[$name],'time',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Time";
				$dt++;
				}
				$j++;
			}

		}
		$chkInsertdatatypes=true;
		//$insertstr="['primeid' => '".$primeid."'";
		$columns=[];
		$columns['primeid']=$primeid;
		$columnnames="";
		$columnvalues="";
		foreach($col_name_values as $col)
		{
			//log::info("-------prim-------");
			$name="";
			$value="";
			foreach($col as $k=>$c)
			{
				////log::info($k."===".$c);

				if($k=="name")
				{
					$name=$c;
					
				}
				if($k=="value")
				{
					$value=$c;
					
				}
				
			}
			
			$columns[$name]=$value;
			
		}
		
		//print_r($columns);
		$colname="";
		$colval="";
	foreach($columns as $name=>$val)
	{
		
		$colname.=$name.",";
		$colval.="'".$val."',";
		//echo "\n".$name."===".$val;
		//echo "\n"."======";
	}
	//echo "\n".$colname;
	/*echo "\n"."---------------";
	echo "\n".$colname;
	echo "\n"."======";
	echo "\n".$colval;
	*/
	$colname=substr($colname,0, strlen($colname)-1);
	$colval=substr($colval,0, strlen($colval)-1);
	
	//BEGIN BATCHING
	echo "<br>===============";
	echo $colval;
	echo "<br>===============<br>";
	$insertrows_array[$insertrows_counter]['values']=$colval;
	echo "<br>count(insertrows_array)==".count($insertrows_array);
	if(count($insertrows_array)>=$batch_size)
	{
		$ins_sqll="INSERT IGNORE into ax_job_".$projectid."_analysis_primary($colname) values";
		for($j=0;$j<$batch_size;$j++)
		{
				
			//echo "<br>insertrows_counter==".$insertrows_counter;
			$ins_sqll.="(";
			$insertrows_array[$j]['values']=utf8_encode($insertrows_array[$j]['values']);
			$ins_sqll.=$insertrows_array[$j]['values'];
			$ins_sqll.=")";
				
			$ins_sqll.=",";
		}
		$ins_sqll=substr($ins_sqll,0,strlen($ins_sqll)-1);
		//echo "<br><br>".$ins_sqll;
		if($conn->query($ins_sqll))
		{
			echo "<br>succesfulll=batchno=".$batch_no;
			error_log("succesfulll=batchno=".$batch_no);
			$key="prime_counter_".$projectid;
			$primcounter=$memcache->get($key);
			$primcounter=$primcounter-$batch_size;
			error_log("primcounter=".$primcounter);
			$setprimcounter=$memcache->set($key,$primcounter);
		}
		else
		{
			echo "<br>failed prim =batchno=".$batch_no."===".$conn->error;
			log_error("<br>failed prim =batchno=".$batch_no."===".$conn->error);
			//$insert_sql="Insert into test(`id`,`check`) VALUES(".$projectid.",'".$conn->error."')";
	
			//$conn->query($insert_sql);
			exit;
		}
		$insertrows_counter=0;
		$insertrows_array=[];
		$batch_no++;
		$col_name_values=array();
	}
	else {
		$insertrows_counter++;
	}	
	
	
	//END BATCHING
	
	/*$ins="INSERT into ax_job_".$projectid."_analysis_primary($colname) values($colval)";
	//echo $ins;
	//echo "\n"."======\n";
	$conn->query($ins);
	$key="prime_counter_".$projectid;
	$primcounter=$memcache->get($key);
	$primcounter--;
	$setprimcounter=$memcache->set($key,$primcounter);
	$insert_sql="Insert into test(`id`,`check`) VALUES(,'".$primcounter."')";
	
	$conn->query($insert_sql);*/
	
	
	
	
	
	}
	if($chkInsertdatatypes  &&  !$chkInsertDtypes)
	{
	$insert_datatypes="Insert IGNORE into  ax_job_".$projectid."_primary_analysis_dataTypes(`id`,`columnName`,`dataType`) VALUES";
	//echo "<br>";
	//print_r($col_datatypes);
	$dtp=1;
	foreach($col_datatypes as $col)
	{
		$insert_datatypes.= "(";
		//echo $col['name']."=====".$col['type']."<br>";
		$insert_datatypes.=$dtp.",'".$col['name']."','".$col['type']."'),";
		$dtp++;
	
	}
	$insert_datatypes=substr($insert_datatypes,0,strlen($insert_datatypes)-1);
	//echo "<br>".$insert_datatypes;
	if($conn->query($insert_datatypes))
	{
		$chkInsertDtypes=true;
	}
	else{
		//echo "<br><br>".$conn->error;
	}
	
	}
	
	echo "<br>wwwwcount(insertrows_array)==".count($insertrows_array);
	//echo "<br>------------------------------------";
	//print_r($insertrows_array);
	//echo "<br>------------------------------------";
	if(count($insertrows_array)>0)
	{
		$ins_sqll="INSERT into ax_job_".$projectid."_analysis_primary($colname) values";
		for($j=0;$j<count($insertrows_array);$j++)
		{
				
			//echo "<br>insertrows_counter==".$insertrows_counter;
			$ins_sqll.="(";
			$insertrows_array[$j]['values']=utf8_encode($insertrows_array[$j]['values']);
			$ins_sqll.=$insertrows_array[$j]['values'];
			$ins_sqll.=")";
				
			$ins_sqll.=",";
		}
		$ins_sqll=substr($ins_sqll,0,strlen($ins_sqll)-1);
		echo "<br><br>".$ins_sqll;
		if($conn->query($ins_sqll))
		{
			echo "<br>succesfulll=batchno=".$batch_no;
			error_log("succesfulll=batchno=".$batch_no);
			$key="prime_counter_".$projectid;
			$primcounter=$memcache->get($key);
			$primcounter=$primcounter-count($insertrows_array);
			$setprimcounter=$memcache->set($key,$primcounter);
			error_log("primcounter=".$primcounter);
		}
		else
		{
			echo "<br>failed prim extra=batchno=".$batch_no."===".$conn->error;
			error_log("<br>failed prim extra=batchno=".$batch_no."===".$conn->error);
			$insert_sql="Insert into test(`id`,`check`) VALUES(".$projectid.",'".$conn->error."')";
	
			$conn->query($insert_sql);
			exit;
		}
		$insertrows_counter=0;
		$insertrows_array=[];
		$batch_no++;
		$col_name_values=array();
	}
	/*$loop_primarys=$loop_primarys+$loopsize;
	}*/

}
  function InsertIntoAnalysisTableSecondary($projectid,$secondfile,$colist,$co2list,$conn,$start,$div)
{
	$memcache= new Memcache;
	$dropsql="DROP TABLE IF EXISTS ax_job_".$projectid."_secondary_analysis_dataTypes";
	//echo "<br>".$dropsql;
	$conn->query($dropsql);
	$sql_create_datatype_table="Create table ax_job_".$projectid."_secondary_analysis_dataTypes(
			id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			columnName varchar(255) NOT NUll,
			dataType varchar(255) NOT NUll)";
	
	$conn->query($sql_create_datatype_table);
	//log::info("co2list==".$co2list);
	$colist1=substr($co2list,0,strlen($co2list)-1);
	$colsecarray=explode(",",$colist1);
	$col_name_type=[];
	$ccnnyy=0;
	$i=0;
	$chkInsertdatatypes=false; 
	$chkInsertDtypes=false;
	
	$insertrows_array=array();
	$insertrows_counter=0;
	$batch_size=500;
	$loopsize=500;
	$batch_no=0;
	
	foreach($colsecarray as $c)
	{
		$column=str_replace("col_","",$c);
		$column=str_replace("`","",$column);
		$sql="SELECT type from `ax_job_".$projectid."_process` WHERE col2id=".$column;
		$result=$conn->query($sql);
		while($type = $result->fetch_assoc()) {
		//Log::info("TYPE OF ".$c."=".$type[0]->type);
		$col_name_type[$i]['name']=str_replace("`","",$c);
		$col_name_type[$i]['type']=$type['type'];
		$i++;
		}
	}

	//print_r($col_name_type);
	/*	foreach($col_name_type as $col)
	 {

	 foreach($col as $k=>$c)
	 {
	 LOg::info($k."===".$c);
	 }
	 }*/
	$secondarys=[];
	$secondary_table = 'ax_job_'.$projectid.'_secondary';
	/*$Total_secondarys=0;
	$secondarys_count_result = $conn->query("SELECT count(id) as count FROM $secondary_table ");
	while($row_count_secondarys= $secondarys_count_result->fetch_assoc())
	{
		$Total_secondarys=$row_count_secondarys['count'];
	}
	$loop_secondarys=0;
	$loop_counter=0;
	while($loop_secondarys<=$Total_secondarys)
	{*/
	
		$result_sec = $conn->query("SELECT $co2list id,MASHFIELD FROM $secondary_table LIMIT $start,$div");
		syslog( LOG_ERR , "queryy="."SELECT $co2list id,MASHFIELD FROM $secondary_table LIMIT $start,$div" );
	//$result_sec = $conn->query("SELECT $co2list id,MASHFIELD FROM $secondary_table limit 50000");
	while($row_sec = $result_sec->fetch_assoc()) {
		
		$secondarys[]=$row_sec;
	}
	$col_name_values=[];
	$j=0;
	$col_datatypes=[];
	$dt=0;
	
	foreach($secondarys as $sec)
	{
		$secid=$sec['id'];
		$ccnnyy++;
		foreach($col_name_type as $col)
		{
			$type="";
			$name="";
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);
				if($k=="type")
				{
					$type=$c;
				}
				elseif($k=="name")
				{
					$name=$c;
				}

			}
			//log::info($type."====".$name);
			//log::info($sec[$name]);

			if($type==0)
			{

				$cleanval= cleanField($sec[$name],'text',$conn);
				$columnname="sec_text_".$name;
				//$$columnname=$sec[$name];
				//Log::info($columnname."=========".$$columnname);

				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Text";
				$dt++;
				}
				$j++;

				/*$columnname="sec_text_wordcount_".$name;
					//$$columnname=str_word_count($sec[$name]);
					//Log::info($columnname."=========".$$columnname);

					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/
			}
			elseif($type==1)
			{
				$columnname="sec_number_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'number',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Number";
				$dt++;
				}
				$j++;
			}
			elseif($type==2)
			{
					
				$cleanval= cleanField($sec[$name],'email',$conn);
				//$col_sec[]="sec_email_col_".$file1colid;
				$columnname="sec_email_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_email_account_col_".$file1colid;
				$columnname="sec_email_account_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'account',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email Account";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_email_domain_col_".$file1colid;
				$columnname="sec_email_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'domain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email Domain";
				$dt++;
				}
				$j++;
				
				/*$columnname="sec_email_isfreeemail_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterEmail($cleanval,'isfreeemail',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Email IsFreeAccount";
				$dt++;
				}
				
				$j++;*/


			}
			elseif($type==3)
			{
				//$col_sec[]="sec_postal_col_".$file1colid;
				$columnname="sec_postal_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'postal',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Postal";
				$dt++;
				}
				$j++;

			}
			elseif($type==4)
			{
				$origval=$sec[$name];
				$cleanval= cleanField($sec[$name],'phone',$conn);
				//$col_sec[]="sec_phone_col_".$file1colid;
				$columnname="sec_phone_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone";
				$dt++;
				}
				$j++;

			/*	//$col_sec[]="sec_phone_countrycode_col_".$file1colid;
				$columnname="sec_phone_countrycode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'countrycode',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Country Code";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_phone_areacode_col_".$file1colid;
				$columnname="sec_phone_areacode_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'areacode',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Area Code";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_phone_no_col_".$file1colid;
				$columnname="sec_phone_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'no',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Number";
				$dt++;
				}
				$j++;
*/
				//$col_sec[]="sec_phone_ext_col_".$file1colid;
				$columnname="sec_phone_ext_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterPhone($origval,'ext',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Phone Extension";
				$dt++;
				}
				$j++;

			}
			elseif($type==5)
			{
				//$col_sec[]="sec_company_col_".$file1colid;
				/*$columnname="sec_company_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'company',$conn);
				$j++;*/
				
				
				$cleanval= cleanField($sec[$name],'company',$conn);
				//$col_prim[]="prim_company_col_".$file1colid;
				$columnname="sec_company_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company";
				$dt++;
				}
				$j++;
					
				/*$columnname="sec_company_withoutsuffix_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterCompany($cleanval,'suffix',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company Without Suffix";
				$dt++;
				}
				$j++;*/
					
				$columnname="sec_company_abbr_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterCompany($cleanval,'abbr',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Company Abbreviation";
				$dt++;
				}
				$j++;
				
				
				
			}
			elseif($type==6)
			{
				$cleanval= cleanField($sec[$name],'address',$conn);
				//$col_sec[]="sec_address_col_".$file1colid;
				$columnname="sec_address_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($cleanval,'address',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_address_no_col_".$file1colid;
				$columnname="sec_address_no_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterAddress($cleanval,'no',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address House No";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_address_streetname_col_".$file1colid;
				$columnname="sec_address_streetname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterAddress($cleanval,'streetname',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Address Street Name";
				$dt++;
				}
				$j++;
			}
			elseif($type==7)
			{
				$cleanval= cleanField($sec[$name],'firstname',$conn);
				//$col_sec[]="sec_firstname_col_".$file1colid;
				$columnname="sec_firstname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name";
				$dt++;
				}
				$j++;

			/*	//$col_sec[]="sec_firstname_initial_col_".$file1colid;
				$columnname="sec_firstname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
				$col_name_values[$j]['value']=$initial;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name Initial";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_firstname_startswith_col_".$file1colid;
				$columnname="sec_firstname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= startswith($cleanval,$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name StartsWith";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_firstname_wordcount_col_".$file1colid;
					$columnname="sec_firstname_wordcount_".$name;
					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/

				$columnname="sec_firstname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="First Name SoundsLike";
				$dt++;
				}
				$j++;
			}
			elseif($type==8)
			{
				$cleanval= cleanField($sec[$name],'lastname',$conn);
				//$col_sec[]="sec_lastname_col_".$file1colid;
					
				$columnname="sec_lastname_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name";
				$dt++;
				}
				$j++;

				/*//$col_sec[]="sec_lastname_initial_col_".$file1colid;
				$columnname="sec_lastname_initial_".$name;
				$col_name_values[$j]['name']=$columnname;
				$initial="";
				if(strlen($cleanval)>0)
				{
					$initial=substr($cleanval,0,1);
				}
					
					
				$col_name_values[$j]['value']=$initial;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name Initial";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_lastname_startswith_col_".$file1colid;
				$columnname="sec_lastname_startswith_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= startswith($cleanval,$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name StartsWith";
				$dt++;
				}
				$j++;

				//$col_sec[]="sec_lastname_wordcount_col_".$file1colid;
				$columnname="sec_lastname_wordcount_".$name;
					$col_name_values[$j]['name']=$columnname;
					$col_name_values[$j]['value']=count(explode(" ", $cleanval));
					$j++;*/

				$columnname="sec_lastname_soundslike_".$name;
				$col_name_values[$j]['name']=$columnname;
				$soundslike="";
				if(strlen($cleanval)>1)
				{
					$soundslike=metaphone($cleanval);
				}
				$col_name_values[$j]['value']=$soundslike;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Last Name SoundsLike";
				$dt++;
				}
				$j++;
			}
			elseif($type==9)
			{
				//$col_sec[]="sec_city_col_".$file1colid;
				$columnname="sec_city_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'city',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="City";
				$dt++;
				}
				$j++;
			}
			elseif($type==10)
			{
				//$col_sec[]="sec_state_col_".$file1colid;
				$columnname="sec_state_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'state',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="State";
				$dt++;
				}
				$j++;
			}
			elseif($type==11)
			{
				//$col_sec[]="sec_country_col_".$file1colid;
				$columnname="sec_country_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= cleanField($sec[$name],'country');
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Country";
				$dt++;
				}
				$j++;
			}
			elseif($type==12)
			{
				$cleanval= cleanField($sec[$name],'webaddress',$conn);
				$columnname="sec_webaddress_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$cleanval;
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address";
				$dt++;
				}
				$j++;
				$columnname="sec_webaddress_domain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterWebAddress($cleanval,'domain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address Domain";
				$dt++;
				}
				$j++;
				$columnname="sec_webaddress_subdomain_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterWebAddress($cleanval,'subdomain',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Web Address SubDomain";
				$dt++;
				}
				$j++;
			}
			elseif($type==13)
			{
				$cleanval= cleanField($sec[$name],'date',$conn);
				$db_date=date("Y-m-d H:i:s", strtotime($sec[$name]));
				$columnname="sec_date_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=$sec[$name];
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date";
				$dt++;
				}
				$j++;
				$columnname="sec_date_db_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']=date("Y-m-d H:i:s", strtotime($sec[$name]));
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Formatted";
				$dt++;
				}
				$j++;
				$columnname="sec_date_year_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($sec[$name],'year',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Year";
				$dt++;
				}
				$j++;
				$columnname="sec_date_month_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($sec[$name],'month',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Month";
				$dt++;
				}
				$j++;
				$columnname="sec_date_day_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($sec[$name],'day',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Day";
				$dt++;
				}
				$j++;
				$columnname="sec_date_time_".$name;
				$col_name_values[$j]['name']=$columnname;
				$col_name_values[$j]['value']= filterDate($sec[$name],'time',$conn);
				if(!$chkInsertdatatypes)
				{
				$col_datatypes[$dt]['name']=$columnname;
				$col_datatypes[$dt]['type']="Date Time";
				$dt++;
				}
				$j++;
			}

		}
		//echo "<br>=====================================================<br>";
		//echo $chkInsertdatatypes;
		//print_r($col_datatypes);
		//echo "<br>======================================================<br>";
		$chkInsertdatatypes=true;
		//echo $chkInsertdatatypes."===========".$chkInsertDtypes."==".$ccnnyy;
		//$insertstr="['seceid' => '".$seceid."'";
		
		
		$columns=[];
		$columns['secondid']=$secid;
		$columnnames="";
		$columnvalues="";
		foreach($col_name_values as $col)
		{
			//log::info("--------------");
			$name="";
			$value="";
			
			foreach($col as $k=>$c)
			{
				//LOg::info($k."===".$c);

				if($k=="name")
				{
					$name=$c;
					
				}
				if($k=="value")
				{
					$value=$c;
					
				}

			}
			//log::info("name=".$name."==value==".$value);
			$columns[$name]=$value;
				
		}
/*echo $columnnames."======".$columnvalues;
$columnnames=substr($columnnames,0, strlen($columnnames)-1);
$columnvalues=substr($columnvalues,0, strlen($columnvalues)-1);
		//$id = $conn->query('job_'.$projectid.'_analysis_secondary')->insertGetId($columns);
$ins="INSERT into ax_job_'.$projectid.'_analysis_secondary($columnnames) values($columnvalues)";
$conn->query($ins);*/
		//print_r($columns);
		$colname="";
		$colval="";
		
		//echo "<br><br>=======================<br>";
		//print_r($columns);
		//error_log("start");
		foreach($columns as $name=>$val)
		{
		
			$colname.=$name.",";
			$colval.="'".$val."',";
			//echo "\n".$name."===".$val;
			//echo "\n"."======";
		}
		/*echo "\n"."---------------";
		 echo "\n".$colname;
		 echo "\n"."======";
		 echo "\n".$colval;
		 */
		$colname=substr($colname,0, strlen($colname)-1);
		$colval=substr($colval,0, strlen($colval)-1);
		/*$ins="INSERT into ax_job_".$projectid."_analysis_secondary($colname) values($colval)";
		echo $ins;
		//echo "\n"."======\n";
		$conn->query($ins);
		$key="sec_counter_".$projectid;
		$seccounter=$memcache->get($key);
		$seccounter--;
		$setseccounter=$memcache->set($key,$seccounter);
		$insert_sql="Insert into test(`id`,`check`) VALUES(".$projectid.",'".$seccounter."')";
		$conn->query($insert_sql);
		*/
		
		//BEGIN BATCHING
		$insertrows_array[$insertrows_counter]['values']=$colval;
		echo "<br>count(insertrows_array)==".count($insertrows_array);
		if(count($insertrows_array)>=$batch_size)
		{
			$ins_sqll="INSERT IGNORE into ax_job_".$projectid."_analysis_secondary($colname) values";
			for($j=0;$j<$batch_size;$j++)
			{
		
				//echo "<br>insertrows_counter==".$insertrows_counter;
				$ins_sqll.="(";
				$insertrows_array[$j]['values']=utf8_encode($insertrows_array[$j]['values']);
				$ins_sqll.=$insertrows_array[$j]['values'];
				$ins_sqll.=")";
		
				$ins_sqll.=",";
			}
			$ins_sqll=substr($ins_sqll,0,strlen($ins_sqll)-1);
			//echo "<br><br>".$ins_sqll;
			if($conn->query($ins_sqll))
			{
				echo "<br>succesfulll=batchno=".$batch_no;
				error_log("succesfulll=batchno=".$batch_no);
				$key="sec_counter_".$projectid;
		$seccounter=$memcache->get($key);
		$seccounter=$seccounter-$batch_size;
		$setseccounter=$memcache->set($key,$seccounter);
		error_log("seccounter=".$seccounter);
			}
			else
			{
				echo "<br>failedll=batchno=".$batch_no."===".$conn->error;
				error_log("<br>failedll=batchno=".$batch_no."===".$conn->error);
				//$insert_sql="Insert into test(`id`,`check`) VALUES(".$projectid.",'".$conn->error."')";
		
				//$conn->query($insert_sql);
				exit;
			}
			$insertrows_counter=0;
			$insertrows_array=[];
			$batch_no++;
			$col_name_values=array();
		}
		else {
			$insertrows_counter++;
		}
		
		
		//END BATCHING
	}
	//error_log("end");
	if($chkInsertdatatypes  &&  !$chkInsertDtypes)
	{
	$insert_datatypes="Insert IGNORE into  ax_job_".$projectid."_secondary_analysis_dataTypes(`id`,`columnName`,`dataType`) VALUES";
	//echo "<br>";
	//print_r($col_datatypes);
	
	$dtp=1;
	foreach($col_datatypes as $col)
	{
		$insert_datatypes.= "(";
		//echo $col['name']."=====".$col['type']."<br>";
		$insert_datatypes.=$dtp.",'".$col['name']."','".$col['type']."'),";
		$dtp++;
	
	}
	$insert_datatypes=substr($insert_datatypes,0,strlen($insert_datatypes)-1);
	//echo "<br>".$insert_datatypes;
	if($conn->query($insert_datatypes))
	{
		$chkInsertDtypes=true;
	}
	else{
		//echo "<br><br>".$conn->error;
	}
	}
	//echo $chkInsertdatatypes."===========".$chkInsertDtypes;
	
	
	
	//echo "<br>wwwwcount(insertrows_array)==".count($insertrows_array);
	//echo "<br>------------------------------------";
	//print_r($insertrows_array);
	//echo "<br>------------------------------------";
	if(count($insertrows_array)>0)
	{
		$ins_sqll="INSERT IGNORE into ax_job_".$projectid."_analysis_secondary($colname) values";
		for($j=0;$j<count($insertrows_array);$j++)
		{
	
			//echo "<br>insertrows_counter==".$insertrows_counter;
			$ins_sqll.="(";
			$insertrows_array[$j]['values']=utf8_encode($insertrows_array[$j]['values']);
			$ins_sqll.=$insertrows_array[$j]['values'];
			$ins_sqll.=")";
	
			$ins_sqll.=",";
		}
		$ins_sqll=substr($ins_sqll,0,strlen($ins_sqll)-1);
		//echo "<br><br>".$ins_sqll;
		//error_log($ins_sqll);
		if($conn->query($ins_sqll))
		{
			echo "<br>succesfulll=batchno=".$batch_no;
			error_log("succesfulll=batchno=".$batch_no);
			$key="sec_counter_".$projectid;
		$seccounter=$memcache->get($key);
		$seccounter=$seccounter-count($insertrows_array);
		$setseccounter=$memcache->set($key,$seccounter);
		error_log("seccounter=".$seccounter);
		}
		else
		{
			echo "<br>failedll=batchno=".$batch_no."===".$conn->error;
			$insert_sql="Insert into test(`id`,`check`) VALUES(".$projectid.",'".$conn->error."')";
	
			$conn->query($insert_sql);
			exit;
		}
		$insertrows_counter=0;
		$insertrows_array=[];
		$batch_no++;
		$col_name_values=array();
	}
	
	/*
	
	$loop_secondarys=$loop_secondarys+$loopsize;
	}
	*/
	
	
	
}
  function cleanField($field,$type,$conn)
{
	//$field=mb_convert_encoding($field, "UTF-8");
	//$field=utf8_encode($field, "UTF-8");
	$field=strtolower($field);
	$search=array("+","(",")",",","?","#","%","!","_","�","'","\\","<",">","*","\/","`","[","]","none","n/a","'","\'","not available","unknown");
	$field =trim($field);
	$field = str_replace($search,"",$field);
	$field=strtolower($field);

	$search=array(" and "," & "," the "," of "," on "," in ");
	$field = str_replace($search," ",$field);
	//$field=mb_convert_encoding($field, "UTF-8");
	if($type=="firstname")
	{
		$search=array(".");
		$salutations=array("Mr","Mrs","Miss","Ms","Dr","Prof","Lady","Lord","Sir","Junior","Jr");
		$field = str_replace($search,"",$field);
		$field = str_replace($salutations,"",$field);
		$field = str_replace("-"," ",$field);
	}
	elseif($type=="number")
	{
		$field = str_replace("-","",$field);
	}
	elseif($type=="postal")
	{
		$search=array("-"," ");
		$field = str_replace($search,"",$field);
		$field=strtoupper($field);
	}
	elseif($type=="phone")
	{
		$search=array("-"," ",".","+","(",")","'","\'",":");
		//$search=array("-"," ",".","extension","exten","eten","ext","et","xt","x","+","(",")","'","\'");
		$field = str_replace($search,"",$field);

	}
	elseif($type=="webaddress")
	{
		error_log("webaddress1=".$field);
		/*if (!preg_match("~^(?:f|ht)tps?://~i", $field)) {
			$field = "http://" . $field;
		*/
		
		
			$field1=parse_url($field,PHP_URL_HOST);
	if($field!="" && $field1=="" && !(strpos($field,":")))
		{
			$field="http://".$field;
			
		}
		$field=parse_url($field,PHP_URL_HOST);
		$search=array("http://","https://","ftp://","www.");
			
		error_log("webaddress2=".$field);
		$field = str_replace($search,"",$field);
		error_log("webaddress3=".$field);
		if(strpos($field,"/")!==false)
		{
			$field=substr($field,0,strpos($field,"/"));
		}
		if(strpos($field,"?")!==false)
		{
			$field=substr($field,0,strpos($field,"?"));
		}
		if(strpos($field,"#")!==false)
		{
			$field=substr($field,0,strpos($field,"#"));
		}
			
		error_log("webaddressafter=".$field);
		$pos=0;
		$cntdots=substr_count($field, ".");
		for($i=0;$i<$cntdots;$i++)
		{

			$pos=strpos($field,".",$pos);
			$partstr=substr($field,$pos,strlen($field));
			$pos++;
			error_log("webaddresdotsafter=".$partstr);
			$partstr1=substr($partstr,1,strlen($partstr));
			$sqlp="SELECT count(*) as cnt FROM `public_suffix_list` where suffix='".$partstr1."'";
			error_log("sqlp=".$sqlp);
			if($result=$conn->query($sqlp)){
				
			
				while($db_web = $result->fetch_assoc()) {
					if($db_web['cnt']>0)
					{
						$field=str_replace($partstr,"",$field);
						error_log("webaddressfinal=".$field);
						break;
							
		
					}
				}
			}
		}
			
	}
	elseif($type=="company")
	{
			
		//log::info("begin field=".$field);
		
		$search=array("-",".","(",")");
		$field = str_replace($search,"",$field);
		/*if(strpos($field," ")!==false)
			{
			$pos=0;
			$cntspaces=substr_count($field, " ");
			for($i=0;$i<$cntspaces;$i++)
			{

			$pos=strpos($field," ",$pos);
			$partstr=substr($field,$pos,strlen($field));
			$pos++;
			log::info("companynamerafter=".$partstr);
			$partstr1=substr($partstr,1,strlen($partstr));
			$sqlp="SELECT count(*) as cnt FROM `company_suffix_list` where suffix='".$partstr1."'";
			log::info("sqlp=".$sqlp);
			$db_web=DB::select(DB::raw($sqlp));
			if($db_web[0]->cnt>0)
			{
			$field=str_replace($partstr,"",$field);
			log::info("companynamefinal=".$field);
			break;
				

			}
			}
				


			}*/
	}


	return $field;
}
  function filterCompany($field,$type,$conn)
{
	//$field=mb_convert_encoding($field, "UTF-8");
	if($type=="suffix")
	{
		if(strpos($field," ")!==false)
		{
			$pos=0;
			$cntspaces=substr_count($field, " ");
			for($i=0;$i<$cntspaces;$i++)
			{
					
				$pos=strpos($field," ",$pos);
				$partstr=substr($field,$pos,strlen($field));
				$pos++;
				//log::info("companynamerafter=".$partstr);
				$partstr1=substr($partstr,1,strlen($partstr));
				$sqlp="SELECT count(*) as cnt FROM `ax_company_suffix_list` where suffix='".$partstr1."'";
				//echo $sqlp;
				//log::info("sqlp=".$sqlp);
				$result=$conn->query($sqlp);
				while($db_web = $result->fetch_assoc()) {
				if($db_web['cnt']>0)
				{
					$field=str_replace($partstr,"",$field);
					//log::info("companynamefinal=".$field);
					break;
						
						
				}
				}
			}
				
				
				
		}
		return $field;
	}
	elseif($type=="abbr")
	{


		$abbr="";
		if(strpos($field," ")!==false)
		{
			$parts=explode(" ", $field);
			foreach($parts as $part)
			{
				$p=substr($part,0,1);
				$abbr.=$p;
			}
		}
		return $abbr;
	}
}
  function filterEmail($field,$type,$conn)
{
	//$field=mb_convert_encoding($field, "UTF-8");
	if($field!="")
	{
		if(strpos($field,"@")=== false)
		{
			$field="";
		}
		else {

				
			$partsemail=explode("@",$field);
			$account=$partsemail[0];
				
			$partsafter=explode(".",$partsemail[1]);
			$domain=$partsafter[0];
				
			if($type=="account")
			{
				return $account;
			}
			else if($type=="domain")
			{
				return $domain;
			}
			else if($type=="isfreeemail")
			{
				$checkFreeEmail=false;
				$sqlp="SELECT count(*) as cnt FROM `ax_free_email_providers` where domain='".$partsemail[1]."'";
				//log::info("sqlp=".$sqlp);
				$result=$conn->query($sqlp);
				while($db_web = $result->fetch_assoc()) {
				if($db_web['cnt']>0)
				{
					$checkFreeEmail=true;
				}
				}	
				return $checkFreeEmail;
			}
			else
			{
				return $field;
			}
		}
	}
	else
	{
		return $field;
	}
}
  function filterPhone($field,$type,$conn)
{
	
	//log::info("original value=".$field);
	//$field=mb_convert_encoding($field, "UTF-8");



	$extension="";
	$countrycode="";
	$areacode="";
	$no="";
	$country="";
	$area="";
	$field=strtolower($field);
	if( stripos($field,"xt")>0 || stripos($field,"x")>0 || stripos($field,"ext")>0 || stripos($field,"extension")>0 || stripos($field,"eten")>0 || stripos($field,"et")>0 || stripos($field,"exten")>0)
	{
		if(stripos($field,"extension")>0)
		{
			$extension=substr($field,stripos($field,"extension")+9,strlen($field));
			$no=substr($field,0,stripos($field,"extension"));
		}
		else if(stripos($field,"exten")>0)
		{
			$extension=substr($field,stripos($field,"exten")+5,strlen($field));
			$no=substr($field,0,stripos($field,"exten"));
		}
		else if(stripos($field,"eten")>0)
		{
			$extension=substr($field,stripos($field,"eten")+4,strlen($field));
			$no=substr($field,0,stripos($field,"eten"));
		}
		else if(stripos($field,"ext")>0)
		{
			$extension=substr($field,stripos($field,"ext")+3,strlen($field));
			$no=substr($field,0,stripos($field,"ext"));
		}
		else if(stripos($field,"et")>0)
		{
			$extension=substr($field,stripos($field,"et")+2,strlen($field));
			$no=substr($field,0,stripos($field,"et"));
		}
		else if(stripos($field,"xt")>0)
		{
			$extension=substr($field,stripos($field,"xt")+2,strlen($field));
			$no=substr($field,0,stripos($field,"xt"));
		}
		else if(stripos($field,"x")>0)
		{
			$extension=substr($field,stripos($field,"x")+1,strlen($field));
			$no=substr($field,0,stripos($field,"x"));
		}
		
		$search_array=array(".","-","(",")","'","\'",":"," ");
		$extension=trim(str_replace($search_array, "", $extension));
		$extension = preg_replace('/\s+/', ' ',$extension);
	}
	else
	{
		$no=$field;
	}
	//log::info("extension=".$extension);

	$countrycodepresent=false;
	if(stripos($field,"+")!==false)
	{
		$countrycodepresent=true;
	}


	$search_array=array(".","-","(",")","'","\'");
	$field=str_replace($search_array, " ", $field);
	$field=str_replace("+", "", $field);
	$field = preg_replace('/\s+/', ' ',$field);
	$field=trim($field);
	$no=str_replace($search_array, "", $no);
	$no=str_replace("+", "", $no);
	$no = preg_replace('/\s+/', '',$no);
	$no=trim($no);
	//log::info("new phone value=".$field);
	$phoneparts=array();
	if(stripos($field," ")!==false)
	{
		$phoneparts=explode(" ",$field);
	}
	if($countrycodepresent && count($phoneparts)>1)
	{
		$sqlp="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$phoneparts[0]."'";
		//log::info("sqlp=".$sqlp);
		$db_phone=$conn->query($sqlp);
		if($db_phone)
		{
			$countrycode=$db_phone[0]->countrycode;
			//$areacode=$db_phone[0]->areacode;
			$country=$db_phone[0]->countryname;
			//$area=$db_phone[0]->area;
				
		}
	}

	if(count($phoneparts)>1)
	{
		$sqlp="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$phoneparts[0]."' and areacode='".$phoneparts[1]."'";
		//log::info("sqlp1=".$sqlp);
		$db_phone=$conn->query($sqlp);
		if($db_phone)
		{
			$countrycode=$db_phone[0]->countrycode;
			$areacode=$db_phone[0]->areacode;
			$country=$db_phone[0]->countryname;
			$area=$db_phone[0]->area;
				
		}
			
	}

	/*
	 $firstdigit=substr($field,0,1);//1 digit country code for countrycode 1 and 7
	 $digit23=substr($field,1,2);//2 digit area code for countrycode 1 and 7
	 $digit234=substr($field,1,3);//3 digit area code for countrycode 1 and 7
	 $digit2345=substr($field,1,4);//4 digit area code  for country code 7
	 $digit23456=substr($field,1,5);//5 digit area code  for country code 7
	 	
	 $digit123=substr($field,0,3);//3 digit countrycode
	 $digit456=substr($field,3,3);//3 digit area code
	 $digit45=substr($field,3,2);//2 digit area code
	 $digit4=substr($field,3,1);//1 digit area code
	 	
	 $digit12=substr($field,0,2);//2 digit countrycode
	 $digit345=substr($field,2,3);//3 digit area code
	 $digit34=substr($field,2,2);//2 digit area code
	 $digit3=substr($field,2,1);//1 digit area code
	 	
	 $len=strlen($field);
	 	
	 	
	 if($firstdigit=="1" )
	 {

	 if(strlen($field)>10)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit234."'" ;
	 log::info("sql1=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
			
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 	

	 }
	 }

	 }
	 else if($firstdigit=="7")
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit23456."'" ;
	 log::info("sql2=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
			
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit2345."'" ;
	 log::info("sql3=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }

			
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$firstdigit."'  and areacode='".$digit234."'" ;
	 log::info("sql4=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }



	 }
	 	
	 else {

	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit456."'" ;
	 log::info("sql5=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit45."'" ;
	 log::info("sql6=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit123."'  and areacode='".$digit4."'" ;
	 log::info("sql7=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit345."'" ;
	 log::info("sql8=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit34."'" ;
	 log::info("sql9=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }
	 if(!$db_phone)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='".$digit12."'  and areacode='".$digit3."'" ;
	 log::info("sql10=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;

	 }
	 }



	 }
	 if($countrycode=="" && $areacode=="" && strlen($field)==10)
	 {
	 $sql="SELECT * FROM `countries_areas_diallingcodes` where countrycode='1'  and areacode='".$digit123."'" ;
	 log::info("sql11=".$sql);
	 $db_phone=DB::select(DB::raw($sql));
	 if($db_phone)
	 {
	 $countrycode=$db_phone[0]->countrycode;
	 $areacode=$db_phone[0]->areacode;
	 $country=$db_phone[0]->countryname;
	 $area=$db_phone[0]->area;
	 	
	 }
	 }
	 	
		//}*/
	//log::info("ext=".$extension."=country=".$countrycode."=area=".$areacode."=".$country."=".$area);


	if($type=="countrycode")
	{
		return $countrycode;
	}
	else if($type=="areacode")
	{
		return $areacode;
	}
	else if($type=="no")
	{
		return $no;
	}
	if($type=="ext")
	{
		return $extension;
	}
	return $field;
}
  function filterAddress($field,$type,$conn)
{
	//$field=mb_convert_encoding($field, "UTF-8");
	return $field;
}
  function startswith($field,$conn)
{

	$pos=strpos($field," ");


	if($pos===false)
	{
		if(strlen($field)>2)
		{
			return $field;
		}
	}
	else {
		$partsfield=explode(" ",$field);
		foreach($partsfield as $part)
		{
			if(strlen($part)>2)
			{
				return $part;
			}
		}
	}

		
}
  function filterWebAddress($field,$type,$conn)
{
	//$field=mb_convert_encoding($field, "UTF-8");
	
	$domain="";
	$subdomain="";

	$field=strtolower($field);
	if((strpos($field,"."))!==false)
	{
		$partsweb=[];
		$partsweb=explode(".",$field);

		$domain=$partsweb[count($partsweb)-1];
		//log::info("domain=====".$domain);
			
		for($i=0;$i<(count($partsweb)-1);$i++)
		{
			$subdomain.=$partsweb[$i].".";

		}
		$subdomain=substr($subdomain,0,strlen($subdomain)-1);
	}
	else
	{
		$domain=$field;
	}
	if($type=="domain")
	{
		return $domain;
	}
	else if($type=="subdomain")
	{
			
		return $subdomain;
	}






	return $field;

}
  function filterDate($field,$type,$conn)
{
	/*$search=array("/"," ",".",":",",");
	 $field =trim($field);
	 $field = str_replace($search,"-",$field);
	 $field = preg_replace('/\s+/', ' ',$field);*/
	$timestamp = strtotime($field);
	$date=date("Y-m-d",$timestamp);
	if($date=="1970-01-01" && strpos($field,"/")!==false)
	{
		$timestamp = strtotime(str_replace('/', '-', $field));
	}





	//log::info("origdate===".$field."===".$type."===date=".$date);
	$year=date("Y", $timestamp);
	//log::info("year===".$year."===".$type);






	if($type=="year")
	{
		$year=date("Y", $timestamp);
		//log::info("year===".$year);
		return $year;
	}
	elseif($type=="month")
	{
		$month=date("m", $timestamp);
		//log::info("month===".$month);
		return $month;
	}
	elseif($type=="day")
	{
		$day=date("d", $timestamp);
		return $day;
	}
	elseif($type=="time")
	{
		$time=date("H:i:s", $timestamp);
		return $time;
	}


}