<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");

/*echo "---------------reana no temp";
echo "====post==";
print_r($_POST);
echo "====request==";
print_r($_REQUEST);
echo "====get==";
print_r($_GET);

*/



$current_projectid=get_id();

//echo "----------------".$current_projectid;
if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}

//echo "===".$_SESSION['userid'];
if(!isset($_SESSION['userid']) or $_SESSION['userid']=="")
{
	echo "<h1>Your session has been expired . Please Login again</h1>";
	exit;	
}
else 
{
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

}
//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}





$sql_ml_id="select id from ax_ml where project_id=".$current_projectid;
if( $ml_id_res = $conn->query($sql_ml_id))
{
	while($ml_id_row = $ml_id_res->fetch_assoc())
	{
		$ml_id = $ml_id_row['id'];

	}
}

//echo "<br>ml id====".$ml_id;

$sql_chk_results="Select * from `ax_job_".$current_projectid."_results` ";
$res_chk_results=$conn->query($sql_chk_results);

if(!$res_chk_results || $res_chk_results->num_rows<=0)
{
//CREATE RESULTS TABLE
$dropsql="DROP TABLE IF EXISTS `ax_job_".$current_projectid."_results`";
//echo "<br>".$dropsql;
$conn->query($dropsql);
$sql="CREATE TABLE `ax_job_".$current_projectid."_results` (
  			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  			`primeid` int(11) DEFAULT NULL,
  			`secondid` int(11) DEFAULT NULL,
  			`result` decimal(10,2) DEFAULT NULL,
  			PRIMARY KEY (`id`),
			KEY `result` (`result`),
  			KEY `primeid` (`primeid`),
  			KEY `secondid` (`secondid`)
			)  CHARSET=latin1;";
//echo "<br>".$sql;
$conn->query($sql);

$colsql = "INSERT INTO `ax_job_".$current_projectid."_results`(`primeid`,`secondid`,`result`) ";
	$colsql .= "SELECT a.`primary_id`,MAX(a.secondary_id) as secondid,MAX(a.matching_value) as result FROM `ax_ml_".$ml_id."_comparisons` a 
INNER JOIN (SELECT primary_id, MAX(matching_value) as matching_value FROM `ax_ml_".$ml_id."_comparisons` GROUP BY primary_id) b ON a.primary_id = b.primary_id AND a.matching_value = b.matching_value AND a.primary_id<>-1 GROUP BY a.primary_id";
	
//echo "<br>".$colsql;

$conn->query($colsql);
}

function reanalyzeAction($id,$ml_id,$conn,$from,$to){


	//$project = Project::find($id);


	/*	$files = glob('project/'.$id.'/files/*'); // get all file names

	foreach($files as $file){ // iterate files
	if(is_file($file))
		unlink($file); // delete file
		}

		$help = DB::table('help_tooltip')->where('page', '=', 'project_analysis')->pluck('content');
		*/
	$from=$from*.01;
	$to=$to*.01;
	$slider_id="";
	$sql_find_slider_id="select id from  ax_project_slider_values where project_id=".$id;
	//echo "<br>".$sql_find_slider_id;
	if( $find_slider_id_res = $conn->query($sql_find_slider_id))
	{
		while($find_slider_id_row = $find_slider_id_res->fetch_assoc())
		{
			$slider_id = $find_slider_id_row['id'];
	
		}
		
		
	}
	
	
	if($slider_id>0)
	{
		$slider_range_sql="update ax_project_slider_values set range_from=".($from*100)." ,range_to=".($to*100)." where id=".$slider_id;
		//echo "<br>".$slider_range_sql;
		$conn->query($slider_range_sql);
	}
	elseif($slider_id=="") {
		$slider_range_sql="insert into  ax_project_slider_values(project_id,range_from,range_to,created_at,updated_at) values($id,".($from*100).",".($to*100).",now(),now()) ";
		//echo "<br>".$slider_range_sql;
		$conn->query($slider_range_sql);
	}
	
	

	


	if($to==100)
	{
		$to=1.0;
	}
	if($from==0)
	{
		$from=0.0;
	}

	//var_dump($from);
	//var_dump($to);
	//echo "==reaana==FROM==".$from."===to===".$to;







	//$get_uniques_sql = "SELECT COUNT(A.id) as `unique` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value is null OR B.matching_value <=".$from;
	$get_uniques_sql = "SELECT COUNT(A.id) as `unique` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result is null OR B.result <=".$from;

	//echo "<br>".$get_uniques_sql;

	//	$total_uniques = DB::select(DB::raw($get_uniques));
	if( $get_uniques_res = $conn->query($get_uniques_sql))
	{
		while($get_uniques_row = $get_uniques_res->fetch_assoc())
		{
			$total_uniques = $get_uniques_row['unique'];

		}
	}

	//echo "<br>uniques==".$total_uniques;


	//$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_ml_".$ml_id."_comparisons` B on (A.id=B.primary_id) WHERE B.matching_value >".$from." AND B.matching_value <=".$to;
	$get_maybes_sql = "SELECT COUNT(A.id) as `maybe` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$from." AND B.result <=".$to;

	//echo "<br>".$get_maybes_sql;
	//$total_maybes = DB::select(DB::raw($get_maybes));

	//echo "------------".$total_maybes[0]->maybe;

	if( $get_maybes_res = $conn->query($get_maybes_sql))
	{
		while($get_maybes_row = $get_maybes_res->fetch_assoc())
		{
			$total_maybes = $get_maybes_row['maybe'];

		}
	}

	//echo "<br>maybes==".$total_maybes;



	$get_dups_sql= "SELECT COUNT(A.id) as `duplicate` FROM `ax_job_".$id."_primary` A LEFT JOIN `ax_job_".$id."_results` B on (A.id=B.primeid) WHERE B.result >".$to;

	//echo "<br>".$get_dups_sql;
	//$total_maybes = DB::select(DB::raw($get_maybes));

	//echo "------------".$total_maybes[0]->maybe;

	if( $get_dups_res = $conn->query($get_dups_sql))
	{
		while($get_dups_row = $get_dups_res->fetch_assoc())
		{
			$total_dups = $get_dups_row['duplicate'];

		}
	}

	//echo "<br>dups==".$total_dups;



	$sql_check_file2="select file_2 from ax_project_files where project_id=".$id;
	if( $res_check_file2 = $conn->query($sql_check_file2))
	{
		while($row_check_file2 = $res_check_file2->fetch_assoc())
		{
			$isfile2 = $row_check_file2['file_2'];

		}
	}
	//echo "<br>isfile2===".$isfile2;

	//$isfile2 = DB::table('project_files')->where('project_id', '=', $id)->pluck('file_2');

	//START FETCHING FIELD NAMES SELECTED BY USER TO MATCH

	//$fetchcols = 'job_'.$id.'_process';
	//$columns = DB::table($fetchcols)->where('id', '>', 0)->get();//

	$sql_cols="select * from ax_job_".$id."_process";
	if( $res_cols = $conn->query($sql_cols))
	{
		while($row_cols = $res_cols->fetch_object())
		{
			$columns[]=$row_cols;
		}

	}

	//print_r($columns);
	//exit;
	$colistheader[]="";
	$co2listheader[]="";
	$colist="A.id ,";
	$co2list="B.id ,";
	$columnlist="id,";
	foreach($columns as $col){
		$colistheader[]="col_".$col->colid;
		$colist.="A.`col_".$col->colid."`,";
		if($col->col2id){
			$co2listheader[]="col_".$col->col2id;
			$co2list.="B.`col_".$col->col2id."`,";
		}
			
	}

	//$fulllist=$colist.substr($co2list,0,strlen($co2list)-1);
	$fulllist=substr($co2list,0,strlen($co2list)-1);
	$colist=substr($colist,0,strlen($colist)-1);
	//echo "<br><br>colistheader=======";

	//echo $colist."==";
	//print_r($colistheader);
	/*echo "<br>=======<br>";
	 echo $co2list."==";
	 print_r($co2listheader);*/
	//echo "<br>".$fulllist;
	//END OF FETCHING FIELD NAMES SELECTED BY USER TO MATCH





	$duplicates_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where result>".$to." GROUP BY primeid ORDER BY result desc LIMIT 10";
	//echo "<br>dqm==".$duplicates_query_main;
	//$prev_duplicates_main = DB::select(DB::raw($duplicates_query_main));
	//print_r($prev_duplicates_main);
	$res_duplicates_query_main = $conn->query($duplicates_query_main);
	if( $res_duplicates_query_main->num_rows>0)
	{
		while($row_duplicates_query_main = $res_duplicates_query_main->fetch_object())
		{
			$prev_duplicates_main[]=$row_duplicates_query_main;
		}


		$arr2_dup=[];
		$arr_dup=[];
		foreach($prev_duplicates_main as $main)
		{
			$arr_dup=[];
			$main->id="";
			$main->result="";
			$arr_dup[]=$main;
			//echo "<br><br><br>=======================<br>";
			//print_r($main);
			//$arr_dup[]=$main;
			//echo "<br>".$main->primeid."<br>";

			if($isfile2 =='' || $isfile2 == NULL){
				//ONE FILE
				$duplicates_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";
			}else{
				$duplicates_query="select yt1.matching_value,yt1.primary_id,".$fulllist."
		            from ax_ml_".$ml_id."_comparisons yt1
		            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
		            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
		            where yt1.matching_value >".$to." and yt1.primary_id=".$main->primeid."
		            ORDER BY yt1.matching_value DESC LIMIT 5";
			}

			if( $res_duplicates_query = $conn->query($duplicates_query))
			{
				$prev_duplicates=array();
				while($row_duplicates_query = $res_duplicates_query->fetch_object())
				{
					$prev_duplicates[]=$row_duplicates_query;
				}
			}
			//echo "=dq=".$duplicates_query."<br><br>";
			//$prev_duplicates = DB::select(DB::raw($duplicates_query));
			//echo "<br>===================<br>";
			//print_r($prev_duplicates);
			//$prev_duplicates=array_merge($main,$prev_duplicates);
			//echo "=====================";

			foreach($prev_duplicates as $d)
			{
				$arr_dup[]=$d;

				/*foreach($d as $f=>$t)
				 echo "<br>".$f."=========".$t;*/
			}
			//print_r($arr_dup);
			$arr2_dup=array_merge($arr2_dup,$arr_dup);

		}
		//echo "<br>arr2_dup==";
		//print_r($arr2_dup);
	}


	$tbheaders = array();
	if(!empty($prev_duplicates)){

		$tbheaders[0]='Match %';
		$tbheaders[1]='Record ID from File 1';
		$tbheaders[2]='Matching Record ID from File 2';
		$i=3;
		foreach($colistheader as $coheader)
		{
			$coheader=str_replace("col_","",$coheader);

			//$colname_map = DB::table('job_'.$id.'_name_mapping_primary')->where('colid', '=', $coheader)->pluck('colname');
			$colmap_sql="Select colname from ax_job_".$id."_name_mapping_primary where colid=".$coheader;

			//echo "<br>".$colmap_sql;
			$res_colmap = $conn->query($colmap_sql);
			if($res_colmap->num_rows>0)
			{
				while($row_colmap = $res_colmap->fetch_object())
				{
					$colname_map=$row_colmap->colname;
				}
			}

			//echo '-=-=-='.$colname_map;
			if($colname_map!="")
				$tbheaders[$i++] = $colname_map;
		}


	}
	//echo "<br>headers==";
	// print_r($tbheaders);
	//exit;
	//echo "<br><br>==================MAYBES================================<br>";
	$maybes_query_main="select r.result,r.primeid,".$colist."  from ax_job_".$id."_results r LEFT JOIN ax_job_".$id."_primary A on (A.id=r.primeid) where (r.result >".$from." AND r.result<=".$to.") GROUP BY primeid ORDER BY r.result desc LIMIT 10";
	// echo "<br>==".$maybes_query_main."<br>";
	//$prev_maybes_main = DB::select(DB::raw($maybes_query_main));
	//print_r($prev_maybes_main);
	$res_maybes_query_main = $conn->query($maybes_query_main);
	if( $res_maybes_query_main->num_rows>0)
	{
		while($row_maybes_query_main = $res_maybes_query_main->fetch_object())
		{
			$prev_maybes_main[]=$row_maybes_query_main;
		}
	 //	print_r($prev_maybes_main);
	 $arr2_may=[];
	 $arr_may=[];
	 foreach($prev_maybes_main as $main)
	 {


	 	$arr_may=[];
	 	$main->id="";
	 	$main->result="";
	 	$arr_may[]=$main;
	 	if($isfile2 =='' || $isfile2 == NULL){
	 		$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_primary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";
	 	}else{
	 		$maybes_query="select yt1.matching_value,yt1.primary_id ,".$fulllist."
            from ax_ml_".$ml_id."_comparisons yt1
            LEFT JOIN ax_job_".$id."_primary A on (A.id=yt1.primary_id)
            LEFT JOIN ax_job_".$id."_secondary B on (B.id=yt1.secondary_id)
            where (yt1.matching_value >".$from." AND yt1.matching_value<=".$to.") and yt1.primary_id=".$main->primeid."
            ORDER BY yt1.matching_value DESC LIMIT 5";
	 	}
	 	//echo "----------".$maybes_query;
	 	//$prev_maybes = DB::select(DB::raw($maybes_query));
	 	$res_maybes_query = $conn->query($maybes_query);
	 	$prev_maybes=array();
	 	if( $res_maybes_query->num_rows>0)
	 	{
	 		while($row_maybes_query = $res_maybes_query->fetch_object())
	 		{
	 			$prev_maybes[]=$row_maybes_query;
	 		}
	 	}
	 	//	echo "<br>=============<br>";
	 	//	print_r($prev_maybes);

	 	foreach($prev_maybes as $d)
	 	{
	 		$arr_may[]=$d;

	 		/*foreach($d as $f=>$t)
	 		 echo "<br>".$f."=========".$t;*/
	 	}
	 	//print_r($arr_dup);
	 	$arr2_may=array_merge($arr2_may,$arr_may);

	 }

	}
	// echo "<br>=============<br>";
	// print_r($arr2_may);
	//exit;
	// $results = DB::table('job_'.$id.'_results')->get();
	$sql_results="Select * from 'ax_ml_'.$ml_id.'_comparisons'";
	$res_results = $conn->query($sql_results);
	if( $res_results->num_rows>0)
	{
		while($row_results = $res_results->fetch_object())
		{
			$results[]=$row_results;
		}
	}

	/*echo "<br><br> total uniques===".$total_uniques;
	 echo "<br><br> total maybes===".$total_maybes;
	 echo "<br><br> total duplicates===".$total_dups;
	 */


	//$total = $total_uniques[0]->unique + $total_maybes[0]->maybe + $total_dups[0]->duplicate;
	$total = $total_uniques + $total_maybes + $total_dups;
	//echo "<br><br> total ===".$total;
	$uper = ($total_uniques == 0 ? 0 : ($total_uniques / $total) * 100);
	$mper = ($total_maybes == 0 ? 0 : ($total_maybes / $total) * 100);
	$dper = ($total_dups == 0 ? 0 : ($total_dups / $total) * 100);

	if(($uper+$mper+$dper) == 0){
		$chartempty = 'true';
	}else{
		$chartempty = 'false';
	}

	$data = array(
			'project' => $project,
			'uniques' => $total_uniques,
			'uniqueper' => $uper,
			'maybes' => $total_maybes,
			'maybeper' => $mper,
			'dups' => $total_dups,
			'dupper' => $dper,
			'preview_duplicates' => $arr2_dup,
			'tbheaders' => $tbheaders,
			'preview_maybes' => $arr2_may,
			'tbmheaders' => $tbheaders,
			'results' => $results,
			'total' => $total,
			'from' => $from,
			'to' => $to,
			'ischartempty' => $chartempty,
			'help' => $help
	);

	return $data;
	//$results = DB::table('job_21_results')->get();


}

//print_r($data);
//print_r($data['preview_maybes']);
//echo "-===-===-===".$data['maybes'];


	$data=reanalyzeAction($current_projectid,$ml_id,$conn,$_POST['from'],$_POST['to']);
	/*echo "-===-maybe===-===".$data['maybes'];
	echo "-===-unique===-===".$data['uniques'];
	echo "-===-==dups=-===".$data['dups'];
	echo "-===-==from=-===".$data['from'];
	echo "-===-==to=-===".$data['to'];*/
	
	require_once("templates/reanalyze-results.php");
	?>
	
 
	