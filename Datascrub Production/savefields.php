<?php
session_start();
require_once("config/dbconnect.php");
require_once("classes/class.datascrub.php");
use google\appengine\api\taskqueue\PushTask;
use google\appengine\api\taskqueue\PushQueue;
//$_SESSION['project_id']=277;
//$projectid=$_POST['id'];
//$projectid=$_SESSION['project_id'];

print_r($_POST['data']);
$project_id="";
$fields1=[];
$fields2=[];
$type=[];
foreach($_POST['data'] as $key=>$val)
{
	if(is_array($val))
	{
		//echo "\n".$key;
		if($key=="fields1")
		{
			$fields1=$val;
		}
		if($key=="fields2")
		{
			$fields2=$val;
		}
		if($key=="type")
		{
			$type=$val;
		}
		/*foreach($val as $k=>$v)
		{
			echo "\n".$k."=".$v;
		}*/
	}
	else
	{
		//echo "\n".$key."=".$val;
		if($key=="id")
		{
			$project_id=$val;
		}
	}
}

$projectid=$project_id;

$dropsql="DROP TABLE IF EXISTS `ax_job_".$projectid."_process`";
$conn->query($dropsql);
$sql="CREATE TABLE `ax_job_".$projectid."_process` (
  			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  			`colid` int(11) DEFAULT NULL,
  			`type` int(11) DEFAULT NULL,
  			`col2id` int(11) DEFAULT NULL,
  			PRIMARY KEY (`id`)
			)  DEFAULT CHARSET=latin1";
//echo $sql;
$conn->query($sql);









if(count($fields2)<=0)
{
$fields2=$fields1;	
}
for($i=0;$i<count($fields1);$i++)
{
	
	//echo "<br>----------".$fields1[$i]."====".$_REQUEST['fields2'][$i]."===".$_REQUEST['type'][$i];
	
	$insSQL="INsert INTO `ax_job_".$project_id."_process`(colid,type,col2id) VALUES($fields1[$i],$type[$i],$fields2[$i])";
		
	//echo $insSQL;
	$conn->query($insSQL);
}



//CreateAnalysisTables($project_id,$secondfile);
////////////////////////////////////////
//CHANGE STATUS OF THE PROJECT TO Ready for INITIAL PROCESSING (2)
$sql="update ax_projects set status='2' where id=".$project_id;
$conn->query($sql);

//GET PROCESSING VARIABLES FROM PROCESS TABLE
$columns=array();
//Check Includes and datatypes
$sql="SELECT * from ax_job_".$projectid."_process";
//echo $sql;

$result = $conn->query($sql);
//echo "hhh";
while($row = $result->fetch_assoc())
{
	//echo "===".$row['colid']."===".$row['type'];
	$columns[]=$row;
}
//print_r($columns);
	
$colist="";
$co2list="";
	
$x=0;
foreach($columns as $col){
	
	$colist.="`col_".$col['colid']."`,";
	if($col['col2id']){
		$co2list.="`col_".$col['col2id']."`,";
	}
	
}

	
//echo $colist."=======".$co2list;
	
//CHECK IF SINGLE OR DOUBLE FILE PROJECT
$secondfile=false;
$sql="select * from ax_project_files where project_id=".$projectid;
echo $sql;
$projectfileexist = $conn->query($sql);

if ($projectfileexist->num_rows > 0) {
	$sql_file2_origID ="Select file_2 from ax_project_files where project_id=".$projectid;
	$result = $conn->query($sql_file2_origID);
	
	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$file2_origID=$row['file_2'];
				if($file2_origID != '' && $file2_origID != null){
					$secondfile=true;
				}
		}
	}
}
$primary_table = 'ax_job_'.$projectid.'_primary';
//echo "---".$primary_table;
//echo "SELECT count(*) as cnt from ax_job_".$projectid."_primary";
$TotalPrimaryQuery = $conn->query("SELECT count(*) as cnt from ax_job_".$projectid."_primary");
while($TotalPrimaryResult =$TotalPrimaryQuery->fetch_assoc())
{
$TotalPrimaryRecords=$TotalPrimaryResult['cnt'];
}
//echo "============".$TotalPrimaryRecords."==".$primary_table;

$Datascrub = new datascrub($conn);
//echo $project_id."====".$secondfile;
$Datascrub->CreateAnalysisTables($projectid,$secondfile);
$tabletype="primary";



$memcache = new Memcache;
$key="prime_counter_".$projectid;
$setprimcache= $memcache->set($key, $TotalPrimaryRecords);
if($TotalPrimaryRecords>20000)
{
$queue_count=10;
}
else 
{
	$queue_count=5;	
}
$queuearray=[];
for($q=0;$q<$queue_count;$q++)
{
	$t=$q+1;
	$queuearray[$q]="normalize-queue".$t;
}
$stepprimary=$queue_count-1;

$div=intval($TotalPrimaryRecords/$stepprimary);

$start=0;
$end=0;
$k=1;
$q=0;
while($start<$TotalPrimaryRecords)
{

	$end=$start+$div;
	if($end>$TotalPrimaryRecords)
	{
		$end=$TotalPrimaryRecords;
	}
	$queue_name=$queuearray[$q];
	error_log("queuename==".$queue_name."===".$start."==".$end."==".$div);
	//$jobid = Queue::pushon($queue_name,new queueAnalyze($projectid,$secondfile,$colist,$co2list,$start,$div));
	$task = new PushTask(
			'/queueNormalize.php',
			['projectid' => $projectid, 'tabletype' => $tabletype,'secondfile' => $secondfile,'colist' => $colist,'co2list' => $co2list,'start' => $start,'div' => $div]);
		$task_name = $task->add($queue_name);
	if($start==$TotalPrimaryRecords)
	{
		break;
	}
	$start=$end;
	$k++;
	$q++;
}







/*

$task = new PushTask(
		'/queueNormalize.php',
		['projectid' => $projectid, 'tabletype' => $tabletype,'secondfile' => $secondfile,'colist' => $colist,'co2list' => $co2list]);
$task_name = $task->add("normalize-queue");

*/
if($secondfile)
{
	$tabletype="secondary";
	
	
	$TotalSecondaryQuery = $conn->query("SELECT count(*) as cnt from `ax_job_".$projectid."_secondary`");
	
	while($TotalSecondaryRows = $TotalSecondaryQuery->fetch_assoc())
	{
		$TotalSecondaryRecords = $TotalSecondaryRows['cnt'];
	}
	
	$memcache = new Memcache;
	$key="sec_counter_".$projectid;
	$setsetcache= $memcache->set($key, $TotalSecondaryRecords);
	
	if($TotalSecondaryRecords>20000)
	{
		$queue_count=10;
	}
	else
	{
		$queue_count=5;
	}
	$queuearray=[];
	for($q=0;$q<$queue_count;$q++)
	{
		$t=$q+1;
		$queuearray[$q]="normalize-queue".$t;
	}
	$stepprimary=$queue_count-1;
	
	$div=intval($TotalSecondaryRecords/$stepprimary);
	
	$start=0;
	$end=0;
	$k=1;
	$q=0;
	while($start<$TotalSecondaryRecords)
	{
	
		$end=$start+$div;
		if($end>$TotalSecondaryRecords)
		{
			$end=$TotalSecondaryRecords;
		}
		$queue_name=$queuearray[$q];
		error_log("queuename==".$queue_name."===".$start."==".$end."==".$div);
		//$jobid = Queue::pushon($queue_name,new queueAnalyze($projectid,$secondfile,$colist,$co2list,$start,$div));
		$task = new PushTask(
				'/queueNormalize.php',
				['projectid' => $projectid, 'tabletype' => $tabletype,'secondfile' => $secondfile,'colist' => $colist,'co2list' => $co2list,'start' => $start,'div' => $div]);
		$task_name = $task->add($queue_name);
		if($start==$TotalSecondaryRecords)
		{
			break;
		}
		$start=$end;
		$k++;
		$q++;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*$tabletype="secondary";
	$task = new PushTask(
			'/queueNormalize.php',
			['projectid' => $projectid, 'tabletype' => $tabletype,'secondfile' => $secondfile,'colist' => $colist,'co2list' => $co2list]);
	$task_name = $task->add("normalize-queue");
	
	*/
}

/*
$TotalPrimaryRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_primary`"));
$TotalPrimaryRecords=$TotalPrimaryRecords[0]->cnt;
$TotalPrimaryAnalysisRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_analysis_primary`"));
$TotalPrimaryAnalysisRecords=$TotalPrimaryAnalysisRecords[0]->cnt;

while($TotalPrimaryRecords!=$TotalPrimaryAnalysisRecords)
{

	$TotalPrimaryRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_primary`"));
	$TotalPrimaryRecords=$TotalPrimaryRecords[0]->cnt;
	$TotalPrimaryAnalysisRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_analysis_primary`"));
	$TotalPrimaryAnalysisRecords=$TotalPrimaryAnalysisRecords[0]->cnt;
	if($TotalPrimaryRecords==$TotalPrimaryAnalysisRecords)
	{
		break;
	}
	else{
		continue;
	}


}
	
	
	
if($secondfile)
{
	$TotalSecondaryRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_secondary`"));
	$TotalSecondaryRecords=$TotalSecondaryRecords[0]->cnt;
	$TotalSecondaryAnalysisRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_analysis_secondary`"));
	$TotalSecondaryAnalysisRecords=$TotalSecondaryAnalysisRecords[0]->cnt;
		
	while($TotalSecondaryRecords!=$TotalSecondaryAnalysisRecords)
	{
			
		$TotalSecondaryRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_secondary`"));
		$TotalSecondaryRecords=$TotalSecondaryRecords[0]->cnt;
		$TotalSecondaryAnalysisRecords = DB::select( DB::raw("SELECT count(*) as cnt from `job_".$projectid."_analysis_secondary`"));
		$TotalSecondaryAnalysisRecords=$TotalSecondaryAnalysisRecords[0]->cnt;
		if($TotalSecondaryRecords==$TotalSecondaryAnalysisRecords)
		{
			break;
		}
		else{
			continue;
		}
			
			
	}
}
*/

//$Datascrub->InsertIntoAnalysisTables($project_id,$secondfile,$colist,$co2list);
?>
