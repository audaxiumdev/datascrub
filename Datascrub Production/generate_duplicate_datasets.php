<?php
session_start();
require_once("config/dbconnect.php");

require_once("templates/header.html");
require_once("templates/nav.html");
?>
<div id="content" class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default top50">
				<div class="panel-heading">Enter Project ID to Analyze</div>
					<div class="panel-body">
		
		
							<form class="form-horizontal" role="form" method="POST" action="/generate_duplicate_datasets">
							<!--  <input type="hidden" name="_token" value="UKOPFx1opOUJweNe0t9jW4MpNm3KdJcn25GTzRy9">-->
	
									<div class="form-group">
										<div class="col-md-12">
											<div class="input-group add-on">
	
													<input tabindex="1" type="text" placeholder="Project ID" class="form-control" name="dupprojectid" >
											</div>
										</div>
									</div>
	
	
	
	
	
								<div class="form-group">
									<div class="col-md-6">
										<button type="submit" tabindex="2" class="btn btn-primary">Submit</button>
									</div>
	
								</div>
							</form>
						</div>

					</div>
			</div>
		</div>
</div>


<div id="content" class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			
				<div class="block  table-responsive">
				Rules:<br><ul>
				<li>  5 duplicate rows will be inserted all fields exactly same except id(primary_id).</li> 
				<li>  5 more duplicate rows will be inserted that will satisfy the following:<ul>
					<li>If field is Numeric and number of chararacters<=6 Then,char23+11 </li>
					<li>If field is Numeric and number of chararacters>6 Then,charlast3+101 </li>
					<li>If field is <b>NOT</b> Numeric and number of chararacters>=5 Then,char4+1( if char 4 is a ,it will be replaced by b) </li>
					</ul></li>
				</ul>	
				</div>
			</div>
		</div>
	</div>


<?php 



if(isset($_POST['dupprojectid']) && $_POST['dupprojectid']!="")
{

	//echo $_POST['analyzeprojectid'];

	$sql="select project_name from ax_projects where id=".$_POST['dupprojectid'];
	//echo $sql;
	if($res = $conn->query($sql))
	{
		while($row = $res->fetch_assoc())
		{
			$projectname=$row['project_name'];
			echo $projectname;
		}
	}

	$ids=array();
	$sql="select id  from ax_job_".$_POST['dupprojectid']."_primary ";
	//echo $sql;
	if($res = $conn->query($sql))
	{
		while($row = $res->fetch_assoc())
		{
			$ids[]=$row['id'];
			
		}
	}

$arr_dup=array_rand($ids,10);
	
	//print_r($arr_dup);
	$str = implode(",", $arr_dup);
	
	//echo $str;
	
	$fields=array();
	$sql_fields="show columns from ax_job_".$_POST['dupprojectid']."_primary";
	if($res_fields = $conn->query($sql_fields))
	{
		while($row_fields = $res_fields->fetch_assoc())
		{
			//echo "<br><---->".$row_comp_columns['Field'];
			if($row_fields['Field']!="id" )
				$fields[]=$row_fields['Field'];
			
		}
	}
	
	
	//print_r($fields);
	$strfields = implode(",", $fields);
	//echo $strfields;
	
	for($i=0;$i<5;$i++)
	{
		$sql_insert="Insert Into ax_job_".$_POST['dupprojectid']."_primary(".$strfields.") select ".$strfields."  from  ax_job_".$_POST['dupprojectid']."_primary where id=".$arr_dup[$i];
		//echo $sql_insert;
		if($conn->query($sql_insert)===TRUE)
		{
			//echo "<br>Duplicate record ".($i+1)." has been inserted";
		}
		else {
			echo $conn->error;
		}
	}
	
	
	for($i=5;$i<10;$i++)
	{
		$sql_insert="Insert Into ax_job_".$_POST['dupprojectid']."_primary(".$strfields.") select ".$strfields."  from  ax_job_".$_POST['dupprojectid']."_primary where id=".$arr_dup[$i];
		//echo "<br><br>".$sql_insert;
		if($conn->query($sql_insert)===TRUE)
			{
			$iid=$conn->insert_id;
			//echo "<br>Duplicate record  has been inserted as id ".$iid;
			}
			else {
			echo $conn->error;
			}
			
		//$iid=1061;
		$sql_select="select * from ax_job_".$_POST['dupprojectid']."_primary where id=".$iid;
		//echo "<br>".$sql_select;
		if($res_select = $conn->query($sql_select))
		{
			while($row_select = $res_select->fetch_assoc())
			{
				echo "<br>";
				//print_r($row_select);
				echo "<br>";
				for($k=0;$k<count($fields);$k++)
				{
					$final_string="";
					//echo "<br>".$row_select[$fields[$k]];
					//echo "<br>".is_numeric($row_select[$fields[$k]]);
					if(is_numeric($row_select[$fields[$k]]) && strlen($row_select[$fields[$k]])<=6)
							{
								//echo "<br>Original numeric string=".$row_select[$fields[$k]];
								$char23=substr($row_select[$fields[$k]],1,2);
								$char23=$char23+11;
								$final_string=substr($row_select[$fields[$k]],0,1).$char23.substr($row_select[$fields[$k]],3,strlen($row_select[$fields[$k]]-1));
								//echo "<br>Final numeric str=".$final_string;
								
							}
					elseif(is_numeric($row_select[$fields[$k]]) && strlen($row_select[$fields[$k]])>6)
							{
								//echo "<br>Original numeric string=".$row_select[$fields[$k]];
								$charlast3=substr($row_select[$fields[$k]],-3);
								$charlast3=$charlast3+101;
								$final_string=substr($row_select[$fields[$k]],0,strlen($row_select[$fields[$k]])-3).$charlast3;
								//echo "<br>Final numeric str=".$final_string;
							}
							elseif(!is_numeric($row_select[$fields[$k]]) && strlen($row_select[$fields[$k]])>=5)
							{
								//echo "<br>Original string string=".$row_select[$fields[$k]];
								$char4=substr($row_select[$fields[$k]],3,1);
								//echo "<br>==".$char4;
								$char4_replace=($char4 == 'z') ? 'a' : chr(ord($char4)+1);
								//echo "<br>==".$char4_replace;
								if(ctype_alpha($char4_replace) )
								{
									$final_string=str_replace($char4,$char4_replace,$row_select[$fields[$k]]);
								}
								//echo "<br>Final string str=".$final_string;
							}
							
							
							if(strlen($final_string)>0 && strcmp($final_string,$row_select[$fields[$k]])>0)
							{
								$update_query="update ax_job_".$_POST['dupprojectid']."_primary set ".$fields[$k]."='".$final_string."' where id=".$iid;
								//echo "<br><br>".$update_query;
								$conn->query($update_query);
							}
							
				
				}
					
			}
		}
		
		
		
	}
	
}














require_once("templates/loginfooter.html");