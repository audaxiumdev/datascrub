<?php
session_start();
//require_once("config/conf.php");
require_once("config/dbconnect.php");
require_once("functions.php");
checkSession();



/*
echo "====post==";
print_r($_POST);
echo "====request==";
print_r($_REQUEST);
echo "====get==";
print_r($_GET);

*/



$current_projectid=get_id();

//echo "----------------".$current_projectid;
if($current_projectid=="error")
{
	echo "<h1>Wrong Project ID.The project Id cannot contain characters.</h1>";
	exit;
}
if($current_projectid=="" || $current_projectid=="0")
{
	if(isset($_SESSION['project_id']))
	{
		$current_projectid==$_SESSION['project_id'];
		$current_projectname==$_SESSION['project_name'];
	}
	else {
		header("Location:projects.php");
	}
}
//checkProjectCompleted($current_projectid,$conn);
//echo "===".$_SESSION['userid'];
if(!isset($_SESSION['userid']) or $_SESSION['userid']=="")
{
	echo "<h1>Your session has been expired . Please Login again</h1>";
	exit;	
}
else 
{
$current_projectname=getProjectName($current_projectid, $_SESSION['userid'],$conn);

}
//echo "<br>=======".$current_projectname;
if($current_projectname=="perm_deny")
{
	echo "<h1>Wrong Project ID or Permission Denied</h1>";
	exit;
}
elseif($current_projectname=="wrong_user")
{
	echo "<h1>User with Id does not exists</h1>";
	exit;
}





$sql_ml_id="select id from ax_ml where project_id=".$current_projectid;
if( $ml_id_res = $conn->query($sql_ml_id))
{
	while($ml_id_row = $ml_id_res->fetch_assoc())
	{
		$ml_id = $ml_id_row['id'];

	}
}
$plot=array();
$chartdata="['Matching Values','Count'],";
//$sql_chart_data="select round(matching_value,2) as matching_value,count(id) as cnt from ax_datascrub.ax_ml_".$ml_id."_comparisons group by round(matching_value,2)";
$sql_chart_data="select round(matching_value,3) as matching_value from ax_datascrub.ax_ml_".$ml_id."_comparisons limit 10000";

$res_chart_data=$conn->query($sql_chart_data);
$i=0;
if( $res_chart_data->num_rows>0)
{
	$total_rows=$res_chart_data->num_rows;
	while($row_chart_data = $res_chart_data->fetch_assoc())
	{
		$plot[]=$row_chart_data['matching_value'];
		$chartdata .= "[".$j.",".$row_chart_data['matching_value']."]";
		if($i<($total_rows-1))
		{
			$chartdata.=",";
		}
		$i++;
}
}
/*$chartdata="['Matching Values','Count'],";
$plot = array(10,9,8,8,9,7,6,5,4,6,7,8,11,10,12,14,16,20,30,29,28,29,27,25,20,18,15,10,8,5,4,1);
$plot2 = array(5,8,10,25,6,3,44,51,55,56,57,58,34,5,59,2);
$plot3 = array(9,7,2,8,5,6,3,4, 2); //9,8,6,4
$plot1 = array(15, 13, 12, 18, 19, 20, 7, 6, 5, 4, 3, 2, 1);//15,20

for($j=0;$j<count($plot);$j++)
{
	$chartdata.="[".($j).",".$plot[$j]."]";	
	if($j<(count($plot)-1))
	{
		$chartdata.=",";
	}
}*/
echo $chartdata;

?>
<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          <?=$chartdata;?>
        ]);
/*
 *   ['Year', 'Sales', 'Expenses'],
 ['2004',  1000,      400],
 ['2005',  1170,      460],
 ['2006',  660,       1120],
 ['2007',  1030,      540]
 */
        var options = {
          title: 'Data Matches',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>
</html>

<?php 
include("localminmax.php");
?>