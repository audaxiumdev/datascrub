<?php
session_start();
require_once("config/dbconnect.php");
//echo "inside export_results";



$data=array();
$data=$_GET;
//print_r($data);
//echo "<br><br>";

exportAction($data,$conn);
function exportAction($data,$conn){

	
	$from = $data['from'];
	$to = $data['to'];
	$jobid = $data['id'];
	$query = $data['query'];
	if($to==100)
	{
		$to=99.99;
	}
	if($from==0)
	{
		$from=1;
	}
	
	$to=$to*0.01;
	$from=$from*0.01;
	
	$colnames=array();
	$sql_mapping_colnames="SElect * from ax_job_".$jobid."_name_mapping_primary";
	//echo $sql_mapping_colnames;
	$res_mapping_colnames = $conn->query($sql_mapping_colnames);
	if( $res_mapping_colnames->num_rows>0)
	{
		while($row_mapping_colnames = $res_mapping_colnames->fetch_object())
		{
			$colnames[]=$row_mapping_colnames;
		}
	}
	
	
	
	
	
	//print_r($colnames);
	
	
	
	if($query == 'uniques'){
		$sql="SELECT A.*,B.primeid,B.result FROM ax_job_".$jobid."_primary A
                    LEFT JOIN ax_job_".$jobid."_results B on (A.id=B.primeid)
                    WHERE B.result<".$from." or B.result is NULL";
		//GET ALL THE COLUMN NAMES FROM  PRIMARY
		/*$res_results = $conn->query($sql);
		if( $res_results->num_rows>0)
		{
			while($row_results = $res_results->fetch_object())
			{
				$results[]=$row_results;
			}
		}*/
		
		
		//$colnames = DB::table('job_'.$jobid.'_name_mapping_primary')->get();
		$sql="SELECT B.result as `Match Result`";

		foreach($colnames as $colname){
			$sql.=",A.`col_".$colname->colid."` AS `".$colname->colname."_p`";
		}
		$sql.=" FROM ax_job_".$jobid."_primary A
                    LEFT JOIN ax_job_".$jobid."_results B on (A.id=B.primeid)
                    WHERE B.result<".$from." or B.result is NULL";
	}else{
		//GET ALL THE COLUMN NAMES FROM  PRIMARY
		//$colnames = DB::table('job_'.$jobid.'_name_mapping_primary')->get();
		$sql="SELECT yt1.result as `Match Result`,yt1.primeid as `Primeid`,yt1.secondid as `Secondid`";
		foreach($colnames as $colname){
			$sql.=",A.`col_".$colname->colid."` AS `".$colname->colname."_p`";
		}
		//IS IT ONE OR TWO FILE
		//$isfile2 = DB::table('project_files')->where('project_id', '=', $jobid)->pluck('file_2');
		
		
		
		
	$sql_check_file2="select file_2 from ax_project_files where project_id=".$jobid;
	//echo "<br>".$sql_check_file2;
	if( $res_check_file2 = $conn->query($sql_check_file2))
	{
		while($row_check_file2 = $res_check_file2->fetch_assoc())
		{
			$isfile2 = $row_check_file2['file_2'];

		}
	}
		
		
		
		
		//echo "<br><br>===isfile2===".$isfile2;
		
		
		
		
		
		
		
		
		
		
		
		if($isfile2 =='' || $isfile2 == NULL){
			//COMPARE FILE --- GET ALL THE COLUMN NAMES FROM  PRIMARY
			//$colnames = DB::table('job_'.$jobid.'_name_mapping_primary')->get();
			
			
			
			
			foreach($colnames as $colname){
				$sql.=",B.`col_".$colname->colid."` AS `".$colname->colname."_s`";
			}
			$sql.= " from ax_job_".$jobid."_results yt1
                    LEFT JOIN ax_job_".$jobid."_primary A on (A.id=yt1.primeid)
                    LEFT JOIN ax_job_".$jobid."_primary B on (B.id=yt1.secondid)";
		}else{
			//COMPARE FILE --- GET ALL THE COLUMN NAMES FROM  SECONDARY
			//$colnames = DB::table('job_'.$jobid.'_name_mapping_secondary')->get();
			$colnames=array();
			$sql_mapping_colnames="SElect * from ax_job_".$jobid."_name_mapping_secondary";
			//echo $sql_mapping_colnames;
			$res_mapping_colnames = $conn->query($sql_mapping_colnames);
			if( $res_mapping_colnames->num_rows>0)
			{
				while($row_mapping_colnames = $res_mapping_colnames->fetch_object())
				{
					$colnames[]=$row_mapping_colnames;
				}
			}
			
			
			
			
			
			
			
			
			foreach($colnames as $colname){
				$sql.=",B.`col_".$colname->colid."` AS `".$colname->colname."_s`";
			}
			$sql.= " from ax_job_".$jobid."_results yt1
                    LEFT JOIN ax_job_".$jobid."_primary A on (A.id=yt1.primeid)
                    LEFT JOIN ax_job_".$jobid."_secondary B on (B.id=yt1.secondid)";
		}
		if($query == 'matches'){
			//DUPLICATES
			$sql.=" where yt1.result>".$to;
		}else{
			//MAYBES
			$sql.=" where yt1.result>".$from." AND yt1.result<=".$to;
		}
		// echo $sql;
	}
	//echo $sql;
	//exit;
	$results=array();
	$results_sql = $conn->query($sql);
	while($row=$results_sql->fetch_object())
	{
		$results[]=$row;
	}
	
	if( !$results_sql ){
		echo "Error=".$conn->error;
	
		//return 'error';
		//return Redirect::back()->withErrors(['msg', 'error']);
	}
	else if($results_sql->num_rows<=0)
	{
		echo "NO Matching results are found";
	}else{
	
		//$proj = Project::find($jobid);
		$proj=array();
		$sql_proj="select * from ax_projects where id=".$jobid;
		$res_proj=$conn->query($sql_proj);
		while($row_proj=$res_proj->fetch_object())
		{
			$proj=$row_proj;
		}
		
		//print_r($proj);
	//exit;	
		$projname = str_ireplace(' ', '_', $proj->project_name);
		//echo "======".$projname;
		//echo $query;
		//exit;
		$filename = "ml_".$projname.'_'.$query.'.csv';
		
		$handle = fopen('php://output', 'w');
		if ($handle && $results)
		{
			/*header('Content-Type: text/csv');
			 header('Content-Disposition: attachment; filename='.$filename);
			 header('Pragma: no-cache');
			 header('Expires: 0');*/
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Cache-Control: private', false);
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment;filename=' . $filename);
			
			
			
			foreach($results[0] as $tbheader => $data){
				$fileheaders[] = $tbheader;
			}
			
			//print_r($fileheaders);
			//exit;
			fputcsv($handle, $fileheaders);
			//exit;
			
		foreach($results as $row) {
				$list = (array)$row;
				fputcsv($handle, $list);
			}
			
			fclose($handle);
			die();
		
	}	
	
	/*	$headers = array(
				'Content-Type'        => 'text/csv',
				'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
				'Content-Disposition' => 'attachment; filename='.$filename,
				'Expires'             => '0',
				'Pragma'              => 'public',
		);

		
		
		//$response = new StreamedResponse(function() use($results, $jobid){
			// Open output stream
			$handle = fopen('php://output', 'w');
			// Add CSV headers
			foreach($results[0] as $tbheader => $data){
				$fileheaders[] = $tbheader;
			}
			fputcsv($handle, $fileheaders);

			foreach($results as $row) {
				$list = (array)$row;
				fputcsv($handle, $list);
			}

			// Close the output stream
			fclose($handle);
			die();
		//}, 200, $headers);

*/			//return $response->send();
	}

}