<div class="container">
        <div class="row">
            

    

                   
                        <div class="col-md-6">
                            <form class="row project-form" method="post" role="form">
                                <div id="errors"></div>
                               

                                <div class="col-md-6 padbtm10">
                                    <label for="usr-fn">Name the project</label>
                                    <input tabindex="1" type="text" class="form-control" name="project_name" value=""  required/>
                                </div>

                                                                <div class="col-md-6 padbtm10">
                                    <label for="usr-fn">Select an existing client/company</label>
                                    <select tabindex="2" class="form-control modal-select client" id="client" name="user_id">
                                        <option> - Select -</option>
                                                                             
                                                                             <?php 
                                                                             while($row_users = $result_users->fetch_assoc()) {
                                                                             echo '<option value="'.$row_users['id'].'">'.$row_users['last_name']." ".$row_users['first_name'].' / Audaxium</option>';
                                                                             }
                                                                             
                                                                             ?>
                                                                             
                                                                                    
                                                                                    
                                                                            </select>
                                </div>
                                
                                
 <button type="submit" data-action="update" class="btn btn-primary ecm-save-project" data-id="">Save Project</button>
                            </form>
                        </div>
                    </div>
            
              <div class="col-md-6 total">
    <!--  <p><b>Total Items:</b> <?php  //$projects->total() ?></p>-->
</div>

<div class="col-md-6">
<div class="col-md-12 alert alert-info fade" id="projectDeleteAlert" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Done!</strong> Project successfully deleted.
        </div>
   <div class="row">
        
        
        
    </div>
</div>
<div class="col-md-12">
    <div class="block  table-responsive">
        <table cellspacing="5" class="table table-striped table-bordered table-list">
            <tbody>
            <thead>
            <tr>
                <th width="30%">Project Name</th>
                <th width="20%">Number of Records</th>
                <th width="20%">Status</th>
                <th width="20%">User</th>
                <th width="10%" class="sorter-false filter-false">Action</th>
            </tr>
            </thead>
            <?php
            $projects=array();
            while($row_projects=$result_projects->fetch_assoc())
            {
            	$projects[]=$row_projects;
            }
           // print_r($projects);
           foreach($projects as $project){
           	
           	//echo $project['id']."==";
            	$file1rows = '0';
            	$file2rows = '0';
            	$sql_dataexists="Select * from ax_project_files where project_id=".$project['id'];
            	$result_dataexists=$conn->query($sql_dataexists);
            	if ($result_dataexists->num_rows > 0) {
            		
            		$sql_data="Select * from ax_project_files where project_id=".$project['id']." LIMIT 1";
            		$result_data=$conn->query($sql_data);
            		$file1ID='';
            		$file2ID='';
            		$fileID2='';
            		while($row_data=$result_data->fetch_assoc())
            		{
            			$file1ID = $row_data['file_1'];
            			$fileID2 = $row_data['file_2'];
            		}
            			if($file1ID != '' || $file1ID != NULL){
            				//$file1_data = DB::table('files')->where('id', '=', $file1ID)->first();
            				 $sql_file1_data="select * from ax_files where id=".$file1ID." LIMIT 1";
            				 $result_file1_data=$conn->query($sql_file1_data);
            				 
            				 $file1datarows=0;
            				
            				 
            				 while($file1_data=$result_file1_data->fetch_assoc())
            				 {
            				 	$file1datarows=$file1_data['rows'];
            				 	
            				 }
            				 $file1rows=$file1datarows;
            				 
            				 	if($fileID2 == ''){
            				 		$file2rows = $file1datarows;
            				 	}else{
            				 	$file2ID = $fileID2;
            				 	
            				 	$sql_file2_data="select * from ax_files where id=".$file2ID." LIMIT 1";
            				 	$result_file2_data=$conn->query($sql_file2_data);
            				 	 
		            				 	while($file2_data=$result_file2_data->fetch_assoc())
		            				 	{
		            				 	
		            				 		$file2rows = $file2_data['rows'];
		            				 	
		            				 	}
            				 	
            						}
            			}		 	
            	}	 	//echo "====file1rows=".$file1rows."==file2rows==".$file2rows; 
            				
            				 	$status = $project['status'];
            				//echo "status=".$status;
            				 $statuslabel = array('Not set', 'Data Loaded', 'Ready for Processing', 'Complete');
            				 $step = array('/upload11/'.$project['id'], '/mapfields/'.$project['id'], '/mapfields/'.$project['id'], '/export.php/id='.$project['id']);
            				 //$status == null
            				 if($status==99)
            				 {
            				 	$status=2;
            				 }
            				 if($status == 1 ){
            				 	//$stat = 'Not set';
            				 	$editlink = $step[0];
            				 }else if($status == 2){
            				 	if (isset($primeIDs[$project['id']])){
            				 		//$position = $primeIDs[$project['id']];
            				 		//$stat = 'Running (Record #'.$position.')';
            				 	}else{
            				 		//$position = "NOFIND";
            				 		//$stat = 'Running';
            				 	}
            				 	//var_dump($primeIDs);
            				 	//var_dump($maxprimeid);
            				 
            				 	$editlink = $step[0];
            				 }else{
            				 	//$stat = $statuslabel[$status];
            				 	//$editlink = $step[$status];
            				 	$editlink = $step[0];
            				 }
            				 
            				 
            				 $stat=$status_array[$status];
            				 $projID = $project['id'];
            				
            				$sql_client="select * from ax_users where id in(select user_id from ax_user_projects where project_id=".$projID.") LIMIT 1";
            				$result_client=$conn->query($sql_client);
            				$client=array();
            				while($row_client=$result_client->fetch_assoc())
            				{
            					$client=$row_client;
            				}
            
                ?>
                 <tr id="<?php echo $project['id'];?>">
                    <td><?php echo $project['project_name'];?></td>
                    <td><?php echo $file1rows?> / <?php  echo $file2rows?></td>
                    <td><?php echo $stat;?></td>
                    <td><?php echo $client['last_name']?>, <?php  echo $client['first_name'];?></td>
                     <td align="center">
                        <div class="block func3">
                            <a title="Open" data-id="<?php echo $project['id'];?>" data-options="" class="ecm-open-project" href="<?php  echo $editlink;?>">
                                <i class="fa fa-blue fa-external-link"></i>
                            </a>
                            
                            <!--  <a data-csrftoken="" data-toggle="modal" data-target="#confirmationModal" data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project?"}' title="Delete" data-id="<?php echo $project['id'];?>" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>-->
                           <!-- <a href="#" data-href="/project/delete.php?id=<?php echo $project['id'];?>" data-toggle="modal" data-target="#confirm-delete" data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project?"}' title="Delete" data-id="<?php echo $project['id'];?>" class="ecm-deleting-project"> <i class="fa fa-times"></i></a>
                              <a href="#" data-href="/project/delete.php?id=<?php //echo $project['id'];?>" data-toggle="modal" data-target="#confirm-delete">Delete project #<?php //echo $project['id'];?></a>
                        
                           
                           <a href="#" data-href="/project/delete.php?id=<?php echo $project['id'];?>" data-toggle="modal" data-target="#confirming-delete" data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project?"}' title="Delete" data-id="<?php echo $project['id'];?>"> <i class="fa fa-times"></i></a>  
                        
                        
                        <a href="#" class="confirm-delete" data-id="<?php echo $project['id'];?>"> <i class="fa fa-times"></i></a>-->
<a href="#" data-id="<?php echo $project['id'];?>" class="confirming-the-delete" data-href="/project/delete.php?id=<?php echo $project['id'];?>" data-toggle="modal" data-target="#confirm-delete" id="link_<?php echo $project['id'];?>" > <i class="fa fa-times"></i></a>
</div>
                    </td>
                </tr>
            <?php   } ?>
              </tbody>
        </table>
    </div>
</div>
<div class="col-md-6 total">
    <!-- <p><b>Total Items:</b> </p> -->
</div>
<div class="col-md-6">
   
</div>
    

                
                           
              
              
               
</div>

 
<div id="myModal" class="modal hide">
    <div class="modal-header">
        <a href="#" data-dismiss="modal" aria-hidden="true" class="close">�</a>
         <h3>Delete</h3>
    </div>
    <div class="modal-body">
        <p>You are about to delete.</p>
        <p>Do you want to proceed?</p>
    </div>
    <div class="modal-footer">
      <a href="#" id="btnYes" class="btn danger">Yes</a>
      <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
    </div>
</div>



<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                            <a href="#" class="btn btn-danger btn-ok" data-dismiss="modal"><i class="fa fa-save"></i>&nbsp;Yes</a>
                 <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                           
                            </div>
            </div>
            </div>
           <!--  
           
           <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-danger btn-ok">Delete</a>
                <a href="#" class="btn btn-danger btn-ok" ><i class="fa fa-save"></i>&nbsp;Yes</a>
                 <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                           
                           
            </div>-->
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm in" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="btn btn-primary btn-sm btn-ok uibtn-delete" data-options="" data-id=""><i class="fa fa-save"></i>&nbsp;Yes</a>
                                <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
 




<script>
/*
$('#myModal').on('show', function() {
    var id = $(this).data('id'),
        removeBtn = $(this).find('.danger');
})

$('.confirm-delete').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    $('#myModal').data('id', id).modal('show');
});

$('#btnYes').click(function() {
    // handle deletion here
  	var id = $('#myModal').data('id');
  	$('[data-id='+id+']').remove();
  	$('#myModal').modal('hide');
});



*/


$('.confirming-the-delete').on('click', function(e) {
   // e.preventDefault();

    var id = $(this).data('id');
    //$('#confirm-delete').data('id', id).modal('show');
    $('#confirm-delete').data('id', id);
});





        $('#confirm-delete').on('click', '.btn-ok', function(e) {
            //alert('clicked delete');
           // var $modalDiv = $(e.delegateTarget);
           // var id = $(this).data('id');
           var elemobj= $('.confirming-the-delete');
            var id = $('#confirm-delete').data('id');
            //alert("id="+id);
			var data={"proj-id":id};
			//$('#confirm-delete').modal('hide');
            $.ajax({
         	   
                url: '/project/delete.php',
                type: 'POST',
                data: {data:data},
                beforeSend: function() {
                	//alert('sdsdsdsd');
                    //$.showLoader();
                },
                error: function(xhr){
                    alert("An error occured: " + xhr.status + " " + xhr.statusText);
                },
                success: function (elemobj,data,id) {
                   // alert('success'+data);
                	$('#projectDeleteAlert').addClass('in');
                    $('#projectDeleteAlert').show();

					/*$('#confirm-delete').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();*/
                    /*elemobj.parent().parent().parent().parent().fadeOut(function () {
                        $(this).remove();
                    });*/
                    $('#confirm-delete').fadeOut(function () {
                    	//$('.confirming-the-delete').parent().parent().parent().remove();
                    	//$('#row_'+id).remove();
                     //$('.confirming-the-delete').closest('tr').remove();
                    	//$('[row_id='+id+']').remove();
                    	//$('.confirming-the-delete').parent().parent().remove();
                    	//alert(id);
                    	  
                    	   var id = $('#confirm-delete').data('id');
                    	  // alert(id);
                    	 $('#' + id + '').closest('tr').remove();
                  	 // var id=$('.confirming-the-delete').closest('.tr').attr('id');
                  	  
                    });
                    
                 // $.unblockUI();
                }
            });










            
            // $.ajax({url: '/api/record/' + id, type: 'DELETE'})
            // $.post('/api/record/' + id).then()
           /* $modalDiv.addClass('loading');
            setTimeout(function() {
                $modalDiv.modal('hide').removeClass('loading');
            }, 1000)*/
        });
        $('#confirm-delete').on('show.bs.modal', function(e) {
           // alert("showing modal");
            var data = $(e.relatedTarget).data();
            $('.title', this).text(data.recordTitle);
            $('.btn-ok', this).data('recordId', data.recordId);
        });
       /* $.confirm = function (message, title, callback) {
            alert('ddd=='+title);
        }*/

      /*    $.confirm = function (message, title, callback) {
            alert('ddd=='+title);
        	confirmModal = $('#confirmationModal');
            confirmModal.find('.modal-title').text(title);
            confirmModal.find('p').text(message);
            //confirmModal.find('.btn-ok').data-id(id);
            
          $('.uibtn-delete').entwine({
                onclick: function (){ 
alert('sdsdsdsd');
                    callback(); 
                    return false; }
            });

            return confirmModal;
        }*/

       /* $.confirmClose = function (){
        	
            $('#confirmationModal').modal('hide');
        }

        $('.uibtn-delete').entwine({
            onclick: function (){ 
            alert("SDsdasdadada");	
            }
        });*/

        
       /* $('.uibtn-delete').entwine({
            onclick: function (){ 
            	//var id = $(this).find('.data-id.');
                // alert(id);      
            	confirmModal = $('#confirmationModal');
            	confirmModal.find('.btn-ok').data-id(id);

                 }
        });*/
       /* $('#confirmationModal').on('click', '.btn-ok', function(e) {
            alert('sdsdsd');
            /*var $modalDiv = $(e.delegateTarget);
            var id = $(this).data('id');
            alert("id=="+id);*/
           /*         
            
        });*/

      /*  function confirm(message, title, id) {
            alert("inside confirm");
        	confirmModal = $('#confirmationModal');
            confirmModal.find('.modal-title').text(title);
            confirmModal.find('p').text(message);
            confirmModal.find('.btn-ok').data-id(id);

            
            $('.uibtn-delete').entwine({alert("Sdsdsds
                onclick: function (){ 
					alert("clicked yes");
                    callback();
                     return false; }
            });

            return confirmModal;
        }

        function confirmClose(){
        	
            $('#confirmationModal').modal('hide');
        }




        
       /* $('.uibtn-delete').entwine({
            
            onclick: function (e){ 
                alert("clicked ok");
               // var data = $(e.relatedTarget).data();
               // alert(data.id);



                
             /*   var $modalDiv = $(e.delegateTarget);
                var id = $(this).data('id');
                alert(id);
                var myClass = $(this).parent().attr("class");
                alert(myClass);
            	var elemobj = this;
            	var data={"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')};
                
alert(data);*/
            
            	 /*  $.ajax({
                	   
                    url: '/project_delete.php',
                    type: 'POST',
                    data: {data:data},
                    beforeSend: function() {
                    	alert('sdsdsdsd');
                       // $.showLoader();
                    },
                    error: function(xhr){
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function (elemobj,data) {
                        alert('success'+data);
                    	$('#projectDeleteAlert').addClass('in');
                        $('#projectDeleteAlert').show();
                        element.parent().parent().parent().parent().fadeOut(function () {
                            $(this).remove();
                        });
                        $.unblockUI();
                    }
                });*/
           /* }
        });*/
       /* function checkfunc()
        {
            alert("checkin function");
        }

        
        $('.ecm-delete-project').entwine({
            onclick: function() {
                alert('ecm-delete-projec');
            	var elemobj = this;


            	var id = elemobj.data('id');
                alert(id);
                function callback()
                {
alert("callback");
                	//elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});



                	
                	confirmClose();

                }
                confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, id,callback);
                return false;
            },

                delete: function (element, data) {
                    alert('inside delete function');
                	   $.ajax({
                        url: '/project/delete.php',
                        type: 'post',
                        data: data,
                        beforeSend: function() {
                            $.showLoader();
                        },
                        success: function (data) {
                            alert('success'+data);
                        	$('#projectDeleteAlert').addClass('in');
                            $('#projectDeleteAlert').show();
                            element.parent().parent().parent().fadeOut(function () {
                                $(this).remove();
                            });
                            $.unblockUI();
                        }
                    });
                

*/



                
                //checkfunc();
                //return false;
              
            	/*var mycallback = function () {
                    alert("inside callback");
                    //elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});
                    //$.confirmClose();
                }
            	 $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, mycallback);
                 return false;*/
            /*}
        });
      /*  $('.ecm-delete-project').entwine({
            onclick: function() {
            	alert("inside delete project");
            	var elemobj = this;
                var callback = function () {
                    alert("inside callback");
                    elemobj.delete(elemobj, {"_token":elemobj.data('csrftoken'),"id": elemobj.data('id')});
                    $.confirmClose();
                }
                $.confirm(elemobj.data('confirmation').message, elemobj.data('confirmation').title, callback);
                return false;
            },

            delete: function (element, data) {
                alert('inside delete function');
            	   $.ajax({
                    url: '/project/delete.php',
                    type: 'post',
                    data: data,
                    beforeSend: function() {
                        $.showLoader();
                    },
                    success: function (data) {
                        alert('success'+data);
                    	$('#projectDeleteAlert').addClass('in');
                        $('#projectDeleteAlert').show();
                        element.parent().parent().parent().fadeOut(function () {
                            $(this).remove();
                        });
                        $.unblockUI();
                    }
                });
            }
        });*/
    </script>






<div class="modal fade bs-example-modal-sm in" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="btn btn-primary btn-sm btn-ok uibtn-delete" data-options="" data-id=""><i class="fa fa-save"></i>&nbsp;Yes</a>
                                <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
 