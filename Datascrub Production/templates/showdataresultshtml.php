<div id="content" class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default top50">
				<div class="panel-heading">Enter Project ID to Analyze</div>
				<div class="panel-body">
					
					
					<form class="form-horizontal" role="form" method="POST" action="/show_data_results">
						<!--  <input type="hidden" name="_token" value="UKOPFx1opOUJweNe0t9jW4MpNm3KdJcn25GTzRy9">-->

						<div class="form-group">
							<div class="col-md-12">
                                <div class="input-group add-on">
                                    
								    <input tabindex="1" type="text" placeholder="Project ID" class="form-control" name="analyzeprojectid" ><br><br>
								    <input tabindex="1" type="text" placeholder="id1" class="form-control" name="id1" ><br><br>
								    <input tabindex="1" type="text" placeholder="id2" class="form-control" name="id2" ><br><br>
                                </div>
							</div>
						</div>

						

						

						<div class="form-group">
							<div class="col-md-6">
								<button type="submit" tabindex="2" class="btn btn-primary">Submit</button>
                            </div>
                            
						</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

