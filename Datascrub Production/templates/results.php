<div class="container">
        <div class="row">
            <div class="content_wrapper">
                <!-- Content -->
                <script src="<?=WEB_URL?>/js/plugins/Chart.js"></script>
<script src="<?=WEB_URL?>/js/plugins/highcharts/highcharts.js"></script>
<div class="container" id="content">
    <div class="row">
        <div class="col-md-12 border-btm">
            <h3><i class="fa fa-file"></i> <?=$data['project'];?></h3>
            <div id="crumbs" class="col-md-9">
                <ul>
                    <li><a href="/upload11/<?=$current_projectid?>">File Upload</a></li>
                    <li><a class="/mapfields/<?=$current_projectid;?>">Field Select</a></li>
                    <li><a href="/export/<?=$current_projectid?>">Initialize Learning</a></li>
                    <li><a class="not">Export Results</a></li>
                    
                    
                </ul>
            </div>
            <div id="crumbs" class="col-md-3">
              <button id="<?=$current_projectid;?>" class="btn btn-primary" style="margin-left:70px;" role="button" onClick="window.open('/barchart/<?=$current_projectid;?>')";>View Chart</button>
            </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-12 alert alert-danger fade" id="exportfileAlert" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> Can't export file at the moment.
        </div>
    </div>
    <div id="analysis_data1" class="row">
        <div class="col-md-9">
            <h2>Step 4: Analyze &amp; Export Results</h2>
        </div>
        <div class="col-md-3 step-btn">
            <button id="<?=$current_projectid;?>" data-csrftoken="hadsyiX9mYzk1cks1mLANeEdOGLAc7rr4xidP0e5" class="submitbtn ecm-export-results btn btn-primary" role="button">Export Results</button>
            <span data-help="&lt;p&gt;Here you are presented with a chart indicating the number of Unique Rows, Maybes and Matches.&lt;/p&gt;

&lt;p&gt;Adjust the slider to modify the number of records in each set.&lt;/p&gt;

&lt;p&gt;The lower number (Default 0) is; The &quot;probability&quot; number where below this, you are confident the record is unique. This number can vary widely depending on your data.  Start with about 60.
The higher, left side of the slider, is; The &quot;probability&quot; number where, above this, you are very very confident that the records match.  Often this number makes sense above 80, but feel free to move it around.&lt;/p&gt;

&lt;p&gt;Below you&#039;ll see example records in the maybe and match categories after you adjust the slider. We will be improving this interface later to show the matched records&lt;/p&gt;

&lt;p&gt;The &quot;Probability&quot; is not a true %.  It is just a number our algorithm spits out.  Max is 100.&lt;/p&gt;

&lt;p&gt;Once you&#039;re done you may download a CSV of the Uniques, Maybes, and Matches seperately.  If you want to download all in 1 file, for now, set the values to get all records as Matches.&lt;/p&gt;

&lt;ul&gt;On export your CSV will contain:
&lt;li&gt;-	Column A will be the Match result number 0-100&lt;/li&gt;
&lt;li&gt;-	The Original File Columns designated by ColumnName_p&lt;/li&gt;
&lt;li&gt;-	The Highest Matched Record Columns designated by ColumnName_s&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;Compare the Original with the Matches.  How good is Datascrub?  Look for false positives.  Compare and substitute data in your spreadsheet and clean up those records.&lt;/p&gt;
" class="question">?</span>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <div id="chartArea" style="min-width: 800px; height: 400px; max-width: 800px; margin-top: -20%; margin-left: -60%;"></div>

            <div class="col-md-12">
                <div class="fuzzlogic"  id="slider-logic"></div>
            </div>
        </div>

        <div class="col-md-12" id="tabs" >
            <h4>Preview Results</h4>
            <ul>
                <li><a href="#tabs-1">Matches</a></li>
                <li><a href="#tabs-2">Maybes</a></li>
            </ul>
            <div id="tabs-1" >
                                   <?php if(empty($data['preview_duplicates'])) {?>
                    <h6>No Matches Found</h6>
                <?php } else { ?>
                    <div class="block analysis-wrap table-responsive" >
                        <table  class="table table-striped table-bordered " >
                            <tbody>
                            <thead>
                            <tr>
                                <?php foreach ($data['tbheaders'] as $tbheader)
                                {
                                 echo "<th>".$tbheader."</th>";
                                }
                                ?>
                                  
                            </tr>
                            </thead>
                            <?php 
                            foreach ($data['preview_duplicates'] as $prevdup)
                            {
                               if(array_key_exists('result', $prevdup)==0)
                                {
                                echo '<tr style="background-color:#AED6F1;">';
                                }
                                else {
                                echo "<tr>";
                                }
                                
                                
                               		foreach ($prevdup as $list)
                               		{
                                     	echo "<td>".$list."</td>";
                               		}
                               
                               
                                    
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
              <?php } ?>
                           </div>
            <div id="tabs-2">
              <?php if(empty($data['preview_maybes'])) {?>
                    <h6>No Maybes Found</h6>
                <?php } else { ?>
                                    <div class="block analysis-wrap table-responsive">
                        <table cellspacing="5" class="table table-striped table-bordered ">
                            <tbody>
                            <thead>
                            <tr>
                              <?php foreach ($data['tbheaders'] as $tbheader)
                                {
                                 echo "<th>".$tbheader."</th>";
                                }
                                ?>
                             </tr>
                            </thead>
                                                        
                                  <?php 
                            foreach ($data['preview_maybes'] as $prevmyb)
                            {
                                if(array_key_exists('result', $prevmyb)==0)
                                {
                                echo '<tr style="background-color:#AED6F1;">';
                                }
                                else {
                                echo "<tr>";
                                }
                                
                                
                               		foreach ($prevmyb as $list)
                               		{
                                     	echo "<td>".$list."</td>";
                               		}
                               
                               
                                    
                                echo "</tr>";
                            }
                            ?>
                                                               
                                                        </tbody>
                        </table>
                    </div>
                     <?php } ?>
                            </div>
        </div>

        <div class="col-md-12">
            <div class="col-md-12" style="text-align:right;">
                <button id="<?=$current_projectid;?>" data-csrftoken="hadsyiX9mYzk1cks1mLANeEdOGLAc7rr4xidP0e5" class="submitbtn ecm-export-results btn btn-primary" role="button">Export Results</button>
            </div>
        </div>

        <div id="export_options">
            <ul id="export_select">
                <li data-value="all">All (<?php echo $data['uniques']+$data['maybes']+$data['dups'];?>)</li>
                <li data-value="uniques">Uniques (<?=$data['uniques'];?>)</li>
                <li data-value="maybes">Maybes (<?=$data['maybes'];?>)</li>
                <li data-value="matches">Matches (<?=$data['dups'];?>)</li>
            </ul>
        </div>

    </div>
</div>
<script>
    window.onload = function(){

        var serdata =  [{
                y: 100,
                name: "No Results",
                color: "#B9D4F0"
            }];

        <?php if($data['ischartempty'] == 'false'){ ?>
        serdata = [{
            y: <?=$data['uniqueper'];?>,
            name: "Uniques (<?=$data['uniques'];?>)",
            color: "#B9D4F0"
        }, {
            y: <?=$data['maybeper'];?>,
            name: "Maybes (<?=$data['maybes'];?>)",
            color: "#87B1DB"
        }, {
            y: <?=$data['dupper'];?>,
            name: "Matches (<?=$data['dups'];?>)",
            color: "#336699"
        }]
        <?php }?>
        

        $('#chartArea').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                backgroundColor: 'rgba(0, 0, 0, 0)',
                spacing: [0, 0, 0, 0],
                marginBottom: 0,
                marginTop: 0,
                marginLeft: 0,
                marginRight: 0
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Rows (<?=$data['total'];?>)',
                align: 'center',
                verticalAlign: 'middle',
                y: 50,
                style: {"fontSize": "14px"}
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            legend: {
                enabled: true,
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                x: -30,
                y: 80,
                navigation: {
                    activeColor: '#3E576F',
                    animation: true,
                    arrowSize: 12,
                    inactiveColor: '#CCC',
                    style: {
                        fontWeight: 'bold',
                        color: '#333',
                        fontSize: '12px'
                    }
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            series: [{
                type: 'pie',
                name: 'Rows',
                innerSize: '45%',
                data: serdata
            }]
        });

        var fuzzlogicslide = $( "#slider-logic" ).slider({
            range: true,
            min: 0,
            max: 100,
            values: [ <?=$data['from']*100;?>, <?=$data['to']*100;?> ],
            slide: function( event, ui ) {
                var offset1 = $(this).children('.ui-slider-handle').first().offset();
                var offset2 = $(this).children('.ui-slider-handle').last().offset();

                $(this).children('.ui-slider-handle').first().attr('bubbletooltip', ui.values[ 0 ]+'%');
                $(this).children('.ui-slider-handle').last().attr('bubbletooltip', ui.values[ 1 ]+'%');

            },
            stop: function( event, ui ) {
                $.showLoader();
               // var data = {"_token": $('.ecm-export-results').data('csrftoken'),"id": $('.ecm-export-results').attr('id'), "from": ui.values[ 0 ],  "to": ui.values[ 1 ]};
 var data = {"id": <?=$current_projectid;?>, "from": ui.values[ 0 ],  "to": ui.values[ 1 ]};

               //alert("sending load request=="+ui.values[0]+"----"+ui.values[1]);
               //alert("data="+<?=$current_pojectid;?>);$('#analysis_data').load('/results/<?=$current_projectid;?>', data, function(){
                $('#analysis_data1').load('/reanalyze-results/<?=$current_projectid;?>', data, function(){
                    $.unblockUI();
                    $( "#tabs" ).tabs();
                    $(".table-list").tablesorter({
                        widgets: ["zebra", "filter"]
                    });
                    $('.modal-select').chosen({width: "100% !important"});

                });

            }
        });
        $('#slider-logic').children('.ui-slider-handle').first().attr('bubbletooltip', $( "#slider-logic" ).slider( "values", 0 ) + '%');
        $('#slider-logic').children('.ui-slider-handle').last().attr('bubbletooltip', $( "#slider-logic" ).slider( "values", 1 ) + '%');

    };
</script>
            </div>
        </div>

        <div class="modal fade bs-example-modal-sm in" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="btn btn-primary btn-sm uibtn-delete" data-options=""><i class="fa fa-save"></i>&nbsp;Yes</a>
                                <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" role="dialog" aria-labelledby="ResultsModalLabel" aria-hidden="true" id="ResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>This project has already been run. Do you wish to view the results or re-run the analysis?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="proceed_results" href="#" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-file-text-o"></i>&nbsp;View Current Results</a>
                        <a id="rerun_analysis" href="#" class="btn btn-default btn-sm" data-projid=""><i class="fa fa-repeat"></i>&nbsp;Re-Run Analysis</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" role="dialog" aria-labelledby="RerunResultsModalLabel" aria-hidden="true" id="RerunResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>Would you like to use the same fields as previously selected or select new fields for comparison?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="same_fields" href="#" data-csrftoken="hadsyiX9mYzk1cks1mLANeEdOGLAc7rr4xidP0e5" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-check-square-o"></i>&nbsp;Use Same Fields</a>
                        <a id="new_fields" href="#" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i>&nbsp;Select New Fields</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
    <!-- HIDDEN / POP-UP DIV -->
    <div class="modal fade" role="dialog" aria-labelledby="HelpModalLabel" aria-hidden="true" id="HelpModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel"><span class="question">?</span> Help</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div id="popup" class="col-md-12 col-sm-12 padbtm15"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p style="float:right;font-size:10px;">Please contact us for more assistance.</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="push"></div>
</div>

<div id="upload_notif" class="notif">
    <div id="notif_content" style="display: none;">
        <div id="box_title_top"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-down"></i></div>
        <div id="project_data_content">
            <div id="project_name_link"><span id="project_name"></span><span style="float:right; margin-right:10px;" id="proj_link"></span></div>
            <ul id="processing_files"></ul>
        </div>
    </div>
    <div id="box_title_btm"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-up"></i></div>

