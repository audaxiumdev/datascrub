 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Datascrub.io</title>
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/bootstrap.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/dropzone.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/font-awesome/css/font-awesome.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/theme.default.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/chosen/chosen.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/smoothness/jquery-ui.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/smoothness/theme.css">
    <link media="all" type="text/css" rel="stylesheet" href="http://datascrub/css/theme.css">

    <script src="http://datascrub/js/jquery-1.10.2.js"></script>
    <script src="http://datascrub/js/dropzone.js"></script>
    <script src="http://datascrub/js/jquery.entwine-dist.js"></script>
    <script src="http://datascrub/js/bootstrap.min.js"></script>
    <script src="http://datascrub/js/jquery.tablesorter.js"></script>
    <script src="http://datascrub/js/jquery.tablesorter.widgets.js"></script>
    <script src="http://datascrub/js/jquery-ui.js"></script>
    <script src="http://datascrub/js/ui-config.js"></script>
    <script src="http://datascrub/js/jquery.form.min.js"></script>
    <script src="http://datascrub/js/jquery.validate.js"></script>
    <script src="http://datascrub/js/jquery-validate-additional-methods.js"></script>
    <script src="http://datascrub/js/blockUI.js"></script>
    <script src="http://datascrub/js/chosen.jquery.js"></script>
    <script src="http://datascrub/js/functions.js"></script>
    <script src="http://datascrub/js/default.js"></script>
    <script src="http://datascrub/js/projectlist.js"></script>
    
</head>
<body>
<div id="container">
    <nav class="navbar navbar-default">
    <div class="container container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/images/datascrublogo200.png" alt="Datascrub"/></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                
                    <li><a href="http://datascrub/projects">Projects</a></li>
                                    <li><a href="http://datascrub/companies">Companies</a></li>
                    <li><a href="http://datascrub/users">Users</a></li>
                
                    <li><a href="http://datascrub/auth/logout">Logout</a></li>
                            </ul>
            <!--<div class="pull-right" style="text-align: right;">
                <h6>WELCOME <i class="fa fa-user"></i> Supriya Uppal</h6>
                <h6>You are logged in as Admin user. <a href="http://datascrub/auth/logout">Logout</a></h6>
            </div>-->
        </div>
    </div>
</nav>
    <div class="container">
        <div class="row">
            <div class="content_wrapper">
                <!-- Content -->
                <div class="container" id="content">

    
    <div class="row">
        <div class="col-md-6">
            <h3>My Projects</h3>
        </div>

        <div class="col-md-6">
            <div class="col-md-12" style="text-align:right; padding-right: 0;">
                <a title="Create project" data-options="" class="btn btn-primary ecm-add-project" href="#createProjectModal" role="button">Add project</a>
                <span data-help="&lt;h3&gt;Add Project&lt;/h3&gt;&lt;p&gt;A Project is a specific analysis you would like to do.  You may want to analyse a single file, or one file against another. Give the project a detailed name so you and your team will understand what you are doing.&lt;/p&gt;&lt;p&gt;You may have many many projects during Beta, so please do not hesitate to try multiple datasets and approaches.&lt;/p&gt;&lt;br/&gt;&lt;h3&gt;Deleting&lt;/h3&gt;&lt;p&gt;You may FULLY delete a project by hitting the X beside the project.  Your data will be fully removed from the system.  Forever.&lt;/p&gt;&lt;br/&gt;&lt;h3&gt;Opening&lt;/h3&gt;&lt;p&gt;Open the project by clicking on the (box with arrow) symbol.  This will take you to a new page.&lt;/p&gt;&lt;br/&gt;&lt;h3&gt;The Queue&lt;/h3&gt;&lt;p&gt;If you have many projects and are performing 1 or more analyses, you may see that your project is Ready for Processing, Queued, Processing, or Complete.  The queue is shared with other clients so please be patient.  Very large files, of 100k or more records may take longer than an hour to run.&lt;/p&gt;" class="question">?</span>
            </div>
        </div>

        <div class="col-md-12 alert alert-info fade" id="projectCreateAlert" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Done!</strong> Project successfully created.
        </div>
        <div class="col-md-12 alert alert-info fade" id="projectDeleteAlert" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Done!</strong> Project successfully deleted.
        </div>
    </div>

    <div class="row">
        <div class="project-wrap col-md-12">
            <div class="col-md-6 total">
                <p><b>Total Items:</b> 27</p>
            </div>
            <div class="col-md-6">
            <ul class="pagination"><li class="disabled"><span>&laquo;</span></li> <li class="active"><span>1</span></li><li><a href="http://datascrub/?page=2">2</a></li> <li><a href="http://datascrub/?page=2" rel="next">&raquo;</a></li></ul>
            </div>
            <div class="col-md-12">
                <div class="block project-wrap table-responsive">
                <table cellspacing="5" class="table table-striped table-bordered table-list">
                    <tbody>
                        <thead>
                            <tr>
                                <th width="30%">Project Name</th>
                                <th width="20%">Number of Records</th>
                                <th width="20%">Status</th>
                                <th width="20%">User</th>
                                <th width="10%" class="sorter-false filter-false">Action</th>
                            </tr>
                        </thead>
                                            <tr>
                            <td>adrain project2</td>
                            <td>40875 / 40875</td>
                            <td>Running (Record #4112)</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/view/196">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="196" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>adrian proj 1</td>
                            <td>287 / 287</td>
                            <td>Running (Record #1)</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/view/195">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="195" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>hghg</td>
                            <td>30 / 30</td>
                            <td>Not set</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/view/194">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="194" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>machinelearning</td>
                            <td>30 / 30</td>
                            <td>Running </td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/view/193">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="193" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>test14</td>
                            <td>14 / 14</td>
                            <td>Running </td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/view/192">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="192" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>audaxium</td>
                            <td>23 / 14</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/184">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="184" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>testphonenumbers</td>
                            <td>220 / 220</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/182">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="182" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>testfirstname30</td>
                            <td>267 / 205</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/181">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="181" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>Test First NAME</td>
                            <td>205 / 205</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/180">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="180" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>sqsqsqs</td>
                            <td>23 / 23</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/179">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="179" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>project3</td>
                            <td>101 / 2730</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/178">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="178" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>Project2</td>
                            <td>101 / 101</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/177">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="177" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>Contacts with company data</td>
                            <td>218 / 218</td>
                            <td>Complete</td>
                            <td>Uppal, Supriya</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/173">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="173" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>hhj</td>
                            <td>14 / 12</td>
                            <td>Complete</td>
                            <td>MacEwen, Rob</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/172">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="172" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                            <tr>
                            <td>gy</td>
                            <td>14 / 14</td>
                            <td>Complete</td>
                            <td>MacEwen, Rob</td>
                            <td align="center">
                                <div class="block func3">
                                    <a title="Open" data-id="" data-options="" class="ecm-open-project" href="project/analysis/171">
                                        <i class="fa fa-blue fa-external-link"></i>
                                    </a>
                                    <a data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" data-toggle="modal"
                                       data-target="#confirmationModal"
                                       data-confirmation='{"title":"Delete Confirmation", "message":"Are you sure you want to delete this project? It will delete the files permanently."}' title="Delete" data-id="171" class="ecm-delete-project" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                                        </tbody>
                </table>
            </div>
            </div>
            <div class="col-md-6 total">
                <p><b>Total Items:</b> 27</p>
            </div>
            <div class="col-md-6">
                <ul class="pagination"><li class="disabled"><span>&laquo;</span></li> <li class="active"><span>1</span></li><li><a href="http://datascrub/?page=2">2</a></li> <li><a href="http://datascrub/?page=2" rel="next">&raquo;</a></li></ul>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="text-align:right; padding-right: 0;">
        <a title="Create project" data-options="" class="btn btn-primary ecm-add-project" href="#createProjectModal" role="button">Add project</a>
    </div>
    </div>

<div class="modal fade" role="dialog" aria-labelledby="ProjectModalLabel" aria-hidden="true" id="ProjectModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-folder"></i> Add Project</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="row project-form" method="post" role="form">
                                <div id="errors"></div>
                                <input type="hidden" name="_token" value="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" />

                                <div class="col-md-12 padbtm10">
                                    <label for="usr-fn">Name the project</label>
                                    <input tabindex="1" type="text" class="form-control" name="project_name" value="" />
                                </div>

                                                                <div class="col-md-12 padbtm10">
                                    <label for="usr-fn">Select an existing client/company</label>
                                    <select tabindex="2" class="form-control modal-select client" id="client" name="user_id">
                                        <option> - Select -</option>
                                                                                    <option value="15">MacEwen Rob / Audaxium</option>
                                                                                    <option value="16">DesBrisay Craig / Audaxium</option>
                                                                                    <option value="17">Wonders Karen / Audaxium</option>
                                                                                    <option value="18">Woo Larry / Audaxium</option>
                                                                                    <option value="19">Son Joe / Audaxium</option>
                                                                                    <option value="21">Keiller Peter / Audaxium</option>
                                                                                    <option value="25">Lakhani Rehan / Audaxium</option>
                                                                                    <option value="28">Tomasi Alex / </option>
                                                                                    <option value="29">Atkinson David / </option>
                                                                                    <option value="30">Boyle Jenna / </option>
                                                                                    <option value="31">Meehan Sarah  / </option>
                                                                                    <option value="32">Tips Nichole / </option>
                                                                                    <option value="33">Tester Peter / </option>
                                                                                    <option value="34">Jammu Hardip / Audaxium</option>
                                                                                    <option value="35">Betts Justin / </option>
                                                                                    <option value="46">Uppal Supriya / Audaxium</option>
                                                                            </select>
                                </div>
                                
                                <!--<div class="col-md-12 padbtm10">
                                    <label for="usr-fn">or enter the name of a new client/company</label>
                                    <input tabindex="3" type="text" class="form-control client" id="client_name" name="client_name" value="" />
                                </div>-->

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" data-action="update" class="btn btn-primary ecm-save-project" data-id="">Save Project</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

            </div>
        </div>

        <div class="modal fade bs-example-modal-sm in" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h4 class="modal-title">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 padbtm15">
                                <p>Are you sure you want to delete?</p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <a href="#" class="btn btn-primary btn-sm uibtn-delete" data-options=""><i class="fa fa-save"></i>&nbsp;Yes</a>
                                <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;No</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" role="dialog" aria-labelledby="ResultsModalLabel" aria-hidden="true" id="ResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>This project has already been run. Do you wish to view the results or re-run the analysis?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="proceed_results" href="#" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-file-text-o"></i>&nbsp;View Current Results</a>
                        <a id="rerun_analysis" href="#" class="btn btn-default btn-sm" data-projid=""><i class="fa fa-repeat"></i>&nbsp;Re-Run Analysis</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" role="dialog" aria-labelledby="RerunResultsModalLabel" aria-hidden="true" id="RerunResultsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"><i class="fa fa-file"></i> Confirm View Results</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 padbtm15">
                                    <p>Would you like to use the same fields as previously selected or select new fields for comparison?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a id="same_fields" href="#" data-csrftoken="LONBjLxC2ioVkH18FtIL4OCxlvGUkWiPI7YI7wmu" class="btn btn-primary btn-sm" data-options=""><i class="fa fa-check-square-o"></i>&nbsp;Use Same Fields</a>
                        <a id="new_fields" href="#" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i>&nbsp;Select New Fields</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
    <!-- HIDDEN / POP-UP DIV -->
    <div class="modal fade" role="dialog" aria-labelledby="HelpModalLabel" aria-hidden="true" id="HelpModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel"><span class="question">?</span> Help</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div id="popup" class="col-md-12 col-sm-12 padbtm15"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <p style="float:right;font-size:10px;">Please contact us for more assistance.</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="push"></div>
</div>

<div id="upload_notif" class="notif">
    <div id="notif_content" style="display: none;">
        <div id="box_title_top"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-down"></i></div>
        <div id="project_data_content">
            <div id="project_name_link"><span id="project_name"></span><span style="float:right; margin-right:10px;" id="proj_link"></span></div>
            <ul id="processing_files"></ul>
        </div>
    </div>
    <div id="box_title_btm"><span class="notif_title_text">Processing Files</span> <span class="total_processing_files inactive"><span class="total_num"></span></span><i class="fa fa-toggle-up"></i></div>
<!--  </div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="centered">Copyright &copy; 2015 - All Rights Reserved. <a href="http://datascrub.io/">DataScrub.io</a></p>
            </div>
        </div>
   </div>
</footer>
</body>
</html>-->
