<div id="analysis_data1" class="row"> 
<div class="col-md-9">
    <h2>Step 4: Analyze &amp; Export Results</h2>
</div>
</div>div>
<?php 
exit;
?>



<div id="analysis_data1" class="row"> 
<div class="col-md-9">
    <h2>Step 4: Analyze &amp; Export Results</h2>
</div>
<div class="col-md-3 step-btn">
    <button id="<?=$current_projectid;?>" data-csrftoken="" class="submitbtn ecm-export-results btn btn-primary" role="button">Export Results</button>
    <span data-help="" class="question">?</span>
</div>
<div class="col-md-4 col-md-offset-4">
    <div id="reanalyze_chartArea" style="min-width: 800px; height: 400px; max-width: 800px; margin-top: -20%; margin-left: -60%;"></div>
    <div class="col-md-12">
        <div class="fuzzlogic" id="reanalyze_slider_logic"></div>
    </div>
</div>

<div class="col-md-12" id="tabs">
    <h4>Preview Results</h4>
    <ul>
        <li><a href="#tabs-1">Matches</a></li>
        <li><a href="#tabs-2">Maybes</a></li>
    </ul>
    <div id="tabs-1">
       <?php if(empty($data['preview_duplicates'])) {?>
                    <h6>No Matches Found</h6>
        <?php } else { ?>
            <div class="block analysis-wrap table-responsive">
                <table cellspacing="5" class="table table-striped table-bordered ">
                    <tbody>
                    <thead>
                    <tr>
                         <?php foreach ($data['tbheaders'] as $tbheader)
                                {
                                 echo "<th>".$tbheader."</th>";
                                }
                                ?>
                    </tr>
                    </thead>
                    <?php 
                            foreach ($data['preview_duplicates'] as $prevdup)
                            {
                               if(array_key_exists('result', $prevdup)==0)
                                {
                                echo '<tr style="background-color:#AED6F1;">';
                                }
                                else {
                                echo "<tr>";
                                }
                                
                                
                               		foreach ($prevdup as $list)
                               		{
                                     	echo "<td>".$list."</td>";
                               		}
                               
                               
                                    
                                echo "</tr>";
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    <div id="tabs-2">
        <?php if(empty($data['preview_maybes'])) {?>
                    <h6>No Maybes Found</h6>
                <?php } else { ?>
            <div class="block analysis-wrap table-responsive">
                <table cellspacing="5" class="table table-striped table-bordered ">
                    <tbody>
                    <thead>
                    <tr>
                         <?php foreach ($data['tbheaders'] as $tbheader)
                                {
                                 echo "<th>".$tbheader."</th>";
                                }
                                ?>
                    </tr>
                    </thead>
                   <?php 
                            foreach ($data['preview_maybes'] as $prevmyb)
                            {
                                if(array_key_exists('result', $prevmyb)==0)
                                {
                                echo '<tr style="background-color:#AED6F1;">';
                                }
                                else {
                                echo "<tr>";
                                }
                                
                                
                               		foreach ($prevmyb as $list)
                               		{
                                     	echo "<td>".$list."</td>";
                               		}
                               
                               
                                    
                                echo "</tr>";
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>

<div class="col-md-12">
    <div class="col-md-12" style="text-align:right;">
        <button id="{{ $project->id }}" data-csrftoken="<?php echo csrf_token(); ?>" class="submitbtn ecm-export-results btn btn-primary" role="button">Export Results</button>
    </div>
</div>

<div id="export_options">
    <ul id="export_select">
        <li data-value="all">All (<?php  $data['uniques']+$data['maybes']+$data['dups'];?>)</li>
        <li data-value="uniques">Uniques (<?=$data['uniques'];?>)</li>
        <li data-value="maybes">Maybes (<?=$data['maybes'];?>)</li>
        <li data-value="matches">Matches (<?=$data['dups'];?>)</li>
    </ul>
</div>
</div>
<style>
    .tablesorter-default {
        background-color: #fff;
        border-spacing: 0;
        color: #333;
        font: 12px/18px Arial,sans-serif;
        margin: 10px 0 15px;
        text-align: left;
        width: 100%;
    }
</style>

<script>
    jQuery(document).ready(function($){
        //var ctx = document.getElementById("chartArea").getContext("2d");
        //window.myPie = new Chart(ctx).Pie(pieData);
        //console.log('reanalyze');

        $( "#reanalyze_slider_logic" ).slider({
            range: true,
            min: 0,
            max: 100,
            values: [ <?=$data['from']*100;?>, <?=$data['to']*100;?> ],
            slide: function( event, ui ) {
                var offset1 = $(this).children('.ui-slider-handle').first().offset();
                var offset2 = $(this).children('.ui-slider-handle').last().offset();

                $(this).children('.ui-slider-handle').first().attr('bubbletooltip', ui.values[ 0 ]+'%');
                $(this).children('.ui-slider-handle').last().attr('bubbletooltip', ui.values[ 1 ]+'%');

            },
            stop: function( event, ui ) {
                $.showLoader();
                var data = {"id": <?=$current_projectid;?>, "from": ui.values[ 0 ],  "to": ui.values[ 1 ]};
				alert ("reanalyzeee"+ui.values[ 0 ]+"--"+ui.values[ 1 ]);
                $('#analysis_data1').load('/reanalyze-results/<?=$current_projectid;?>', data, function(){
                    $.unblockUI();
                    $( "#tabs" ).tabs();
                    $(".table-list").tablesorter({
                        widgets: ["zebra", "filter"]
                    });
                });
            }
        });

        $('#reanalyze_slider_logic').children('.ui-slider-handle').first().attr('bubbletooltip', $( "#reanalyze_slider_logic" ).slider( "values", 0 ) + '%');
        $('#reanalyze_slider_logic').children('.ui-slider-handle').last().attr('bubbletooltip', $( "#reanalyze_slider_logic" ).slider( "values", 1 ) + '%');
        //$( "#slider-logic" ).slider( "values", 0,);
        //$( "#slider-logic" ).slider( "values", 1,);

        var serdata =  [{
            y: 100,
            name: "No Results",
            color: "#B9D4F0"
        }];

        <?php if($ischartempty == 'false'){ ?>
        serdata = [{
            y: <?=$data['uniqueper'];?>,
            name: "Uniques (<?=$data['uniques'];?>)",
            color: "#B9D4F0"
        }, {
            y: <?=$data['maybeper'];?>,
            name: "Maybes (<?=$data['maybes'];?>)",
            color: "#87B1DB"
        }, {
            y: <?=$data['dupper'];?>,
            name: "Matches (<?=$data['dups'];?>)",
            color: "#336699"
        }]
        <?php }?>


        $('#reanalyze_chartArea').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                backgroundColor: 'rgba(0, 0, 0, 0)',
                spacing: [0, 0, 0, 0],
                marginBottom: 0,
                marginTop: 0,
                marginLeft: 0,
                marginRight: 0
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Rows (<?=$data['uniques']+$data['maybes']+$data['dups'];?>)',
                align: 'center',
                verticalAlign: 'middle',
                y: 50,
                style: {"fontSize": "14px"}
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    allowPointSelect: true,
					  animation: false,
                    cursor: 'pointer',
                    showInLegend: true,
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            legend: {
                enabled: true,
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                x: -30,
                y: 80,
                navigation: {
                    activeColor: '#3E576F',
                    animation: true,
                    arrowSize: 12,
                    inactiveColor: '#CCC',
                    style: {
                        fontWeight: 'bold',
                        color: '#333',
                        fontSize: '12px'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Rows',
                innerSize: '45%',
                data: serdata
            }]
        });
    });
</script>